var UINotific8 = function () {
	
    return {
        //main function to initiate the module		
        init: function () {
			UINotific8.payslips();
            UINotific8.ticketing();
            UINotific8.localClientWizard();
        },
		
		payslips: function(){
			$.ajax({
				type: "GET",
				url: document.location.origin+ '/mypayroll/processing/checkForPayslipUpdates.php',
				dataType: "json",
				data: {},
				success: function(data)
					{
						if(data[1] == "1"){
							var settings = {
								theme: 'smoke',
								sticky: false,
								horizontalEdge: 'top',
								verticalEdge: 'right',	
								}
								$.notific8('zindex', 9996);
								settings.heading = "Payslips received";
								settings.life = "3000";
								if(data[1] != data[8]){
									$.notific8("Your payslips have been received and are being processed", settings);
								}
							}						
						else if(data[1] == "2") {
							var settings = {
								theme: 'tangerine',
								sticky: false,
								horizontalEdge: 'top',
								verticalEdge: 'right'
								}
								$.notific8('zindex', 9996);
								settings.heading = "Payslip processing";
								settings.life = "3000";
								if(data[1] != data[8]){
									$.notific8("Your payslips will be available shortly", settings);
								}
							}											
						else if(data[1] == "3") {
							var settings = {
								theme: 'teal',
								sticky: false,
								horizontalEdge: 'top',
								verticalEdge: 'right'
								}
								$.notific8('zindex', 9996);
							settings.heading = "Payslip processing";
							settings.life = "3000";
							if(data[1] != data[8]){
								$.notific8("Your payslips are available for viewing now", settings);
								}
							}											
						else if(data[1] == "4") 
							{
							var settings = {
								theme: 'ruby',
								sticky: false,
								horizontalEdge: 'top',
								verticalEdge: 'right'
								}
								$.notific8('zindex', 9996);
								settings.heading = "Payslips error";
								settings.life = "6000";
								if(data[1]!= data[8])
								{

									$.notific8("We have encountered some problems with processing payslips. Our system found x payslips but you entered x ", settings);
								}
							}
						setTimeout(UINotific8.payslips, 5000);	
					},
					error: function(xhr){
						//alert(xhr);
					}
				});
			},

        ticketing: function(){
            $('body').on('click', '#selfServicePortal', function(){
                var settings = {
                    theme: 'teal',
                    sticky: false,
                    horizontalEdge: 'top',
                    verticalEdge: 'right'
                }
                $.notific8('zindex', 9996);
                settings.heading = 'Payslip Onboarding Ticket Raised';
                settings.life = "16000";
                $.notific8(" A link to this ticket can be found in your tickets or you can retrieve it from an emailed link which has been sent to you", settings);
            });

            $('body').on('click', '#globalReporting', function(){
                var settings = {
                    theme: 'teal',
                    sticky: false,
                    horizontalEdge: 'top',
                    verticalEdge: 'right'
                }
                $.notific8('zindex', 9996);
                settings.heading = 'Reporting Onboarding Ticket Raised';
                settings.life = "16000";
                $.notific8(" A link to this ticket can be found in your tickets or you can retrieve it from an emailed link which has been sent to you", settings);
            });
        },

        localClientWizard: function(){
            $('body').on('click', '#submitLocalClient', function(){
                var settings = {
                    theme: 'teal',
                    sticky: false,
                    horizontalEdge: 'top',
                    verticalEdge: 'right'
                }
                $.notific8('zindex', 9996);
                settings.heading = 'Success: Local Client Creation';
                settings.life = "3000";
                $.notific8("you have successfully created your new client, to amend these details go to client manager", settings);

            })
        },

        localTmfSetup: function(){
            $('body').on('click', '#submitLocalClient', function(){
                var settings = {
                    theme: 'teal',
                    sticky: false,
                    horizontalEdge: 'top',
                    verticalEdge: 'right'
                }
                $.notific8('zindex', 9996);
                settings.heading = 'Success: Local Client Creation';
                settings.life = "3000";
                $.notific8("you have successfully created your new client, to amend these details go to client manager", settings);

            })

        }

}}();