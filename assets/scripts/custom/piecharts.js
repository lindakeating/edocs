function initPieCharts(){
	var dataSet = [
		{label: "Female", data: .5, color: "#005CDE" },
		{label: "Male", data: .5, color: "#00A36A" },
	];
	var dataSet2 = [
		{label: "Female", data: .8, color: "#005CDE" },
		{label: "Male", data: .2, color: "#00A36A" },
	]
	var dataSet3 = [
		{label: "Female", data: .6, color: "#005CDE" },
		{label: "Male", data: .4, color: "#00A36A" },
	]
	var dataSet4 = [
		{label: "Female", data: .3, color: "#005CDE" },
		{label: "Male", data: .7, color: "#00A36A" },
	]
	
	var options = {
		series: {
			pie: {
				show: true,               
				label: {
					show:true,
					radius: 10,
					formatter: function (label, series) {               
						return '<div style="font-size:8pt;text-align:center;padding:5px;color:white;">' +
						label + ' : ' +
						Math.round(series.percent) +
						'%</div>';
					},
					background: {
						opacity: 0.8,
						color: '#000'
					}
				}
			}
		},

		legend: {
			show: false
		},
		grid: {
			hoverable: true
		}
	};
	$.plot("#headCountPie1", dataSet,options );
	$.plot("#headCountPie2", dataSet2,options );
	$.plot("#headCountPie3", dataSet3,options );
	$.plot("#headCountPie4", dataSet4,options );

	
}