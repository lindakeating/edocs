/**
 * Created by Linda on 20/06/14.
 */

$(function () {
    var grossToNet;
    $.ajax({
        datatype: 'JSON',
        url: "grossTonetChart.php",
        type:"POST",
        success: function(data){
          grossToNet = drawChart(data);
            }
         });

    $('body').on('click', '#refineSearch', function(){
        $.ajax({
            url: 'grossToNetChart.php',
           // datatype: 'JSON',
            type: 'POST',
            data: {
                action: 'updateChart',
                region: selectedRegion,
                country: selectedCountry,
                costCenter: selectedCostCenter,
                group: selectedGroup
            },
            success: function(data){
                var d = JSON.parse(data);
                while(grossToNet.series.length > 0)
                    grossToNet.series[0].remove(true);
               // grossToNet.series[0].remove(true);
                for(var i=0; i< d['series'].length; i++){
                    grossToNet.addSeries(d['series'][i]);
                }
                grossToNet.redraw();
            },
            error:function(e) {
                console.log(e);
            }
        });
    });
});

    function drawChart(d){

       var d = JSON.parse(d);

        for (var i= 0; i< d.series.length; i++ ){
            if(d.series[i].marker != null){
                d.series[i].marker.lineColor= d.series[i].marker.lineColor.replace(/"/g, "");
            }
        }
        var $container = $('#grossToNetChart');
        var options = {
            chart: {
                renderTo: $container[0],
                height: 300
            },
            colors: ['Teal', 'Red','LightSteelBlue', 'Orange' ],
            title: {
                text: 'Gross Analysis'
            },
            xAxis: {
                categories: ['Commission', 'Overtime', 'Basic Salary']
            },
            labels: {
                items: [{
                    html: 'Gross Analaysis',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                    }
                }]
            },
            series: d['series']
        }
        var grossToNet = new Highcharts.Chart(options);
        return grossToNet;
 }
