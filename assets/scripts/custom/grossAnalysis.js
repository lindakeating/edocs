	  var table;
	  var childTable;
	  var regionSelect;
	  var countrySelect;
	  var selectedRegion = [];
	  var selectedCountry = [];
	  var selectedCostCenter = [];
	  var selectedGroup = [];
      var startRange = '201405';
      var endRange = '201405';
      var col_id;

	  $.ajax({
		"dataType": 'JSON',
		"url": 'gross_to_net.php',
		"type": 'POST',
		'data': {action: 'getParentTable',
                 start: '201405',
                 end: '201405'},
		"success": function(data){
			tableName = "#grossToNet";
			drawTable(data);
			addEventListenerToCell(table, tableName);
			writeSelectHeadings(table);
			drawChart(data);
		}
	});
	
	function drawChart(d){
	 var datasets = [];
	 $.each(d.aaData, function(i, val){	 
		arr = [];
		arr.push([1, val['Net Salary']]);
		arr.push([2, val['Total Gross Salary']]);
		arr.push([3, val['Total ER Cost']]);
		object = new Object();
		object.label = val['Country'];
		object.label = val['Country'];
		object.data = arr;
		datasets.push(object);
		
		var options = {
			series: { 
				lines: { show: true },     
				points: {
					radius: 3,
					show: true
				}
			},
			paging: false,
			yaxis: { tickLength:0 }, 
			xaxis: { tickLength:0 },
			xaxis: {
			ticks: [[1,'Net Salary'],[2,'Total Gross Salary'],[3,'Total ER Cost']]
			},
			legend : {
				container: $("#legend"),
				show: true,
				position: "ne",
				backgroundColor: "green",
				backgroundOpacity: 0.5		
			}
		};
	 });
	}
	
	function drawSparkLineChart(d){
		sparkValues = [];
		$.each(d.aaData, function(i, val){
			values = [];
			values.push(val['Total Gross Salary']);
			values.push(val['Total Taxable Salary']);
			values.push(val['Total ER Cost']);
			sparkValues.push(values);			
		});
		$('#chart').sparkline(sparkValues, {type: 'bar',height: '8.5em', width: '1000px',  enableTagOptions: true , barWidth: '20px'});
	}
	
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function drawTable(data){
        console.log(data.aoColumns);
		table = $('#grossToNet').dataTable({

			"aaData" :data.aaData,
			"aoColumns": data.aoColumns,
			"sDom": '<"block-inline pull-left"C>lfrtip<"toolbar">',
			"oColVis": {
				"bRestore": true,
				"align": "left"
			},
			"destroy": true,
			"paging": false,
			"jQueryUI": true,
			"destroy": true,
			"scrollX": "100%",
			"bFilter": false,
			"bLengthChange": false,
			"oTableTools": {
				"sRowSelect": "single"
			},
			"language": {
				"decimal": ",",
				"thousands": ","
			},
			"columnDefs": [
				{
					"targets": [2,3,4,5,7,8,9,10,11,13,14,15,16,17,20,21,22,23,24, 25, 27 ,28],
					"visible": false
				},
				{
					"render": function ( data, type, row ) {
						if(data != null){
							return numberWithCommas(data);
						}
						else{
							return data;
						}
                },
                "targets": [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
				},
                {
                    "targets" : [],
                    "createdCell": function(td, cellData, rowData, row, col){
                        if(col){
                            $(td).append('<i class="fa fa-chevron-down pull-right"></i>');
                        }
                    }
                }
			],
			"footerCallback": function(tFoot, aaData, iStart, iEnd, aiDisplay){

				var api = this.api();
				$($(tFoot).children()).remove();
				
				var aRow = aaData[0];
				var pCnt = 0;
				var properties = [];
				for(var p in aRow) {
					if (aRow.hasOwnProperty(p) ){
						pCnt += 1;
						properties.push(p);
					}
				}
				var totals = [];
				for (var j=0; j < pCnt; j += 1) {
					totals[j] = 0;
				}
				for(var i= 0; i<aaData.length; i++){
				    // for each column
					for (var j=0; j < pCnt; j += 1) {					
						var propName = properties[j];
						var t;
						if (aaData[i][propName] && ( !isNaN(t = parseFloat(aaData[i][propName])))) {
							totals[j] += t;
						}
					}
				}	
				var myTable = $('#grossToNet');
				for (var j=0; j < pCnt; j += 1) {
				var a = document.createElement('th');
				
					if(api.column(j).visible()){
						$(a).html(j < 5 ? '' : numberWithCommas("" + totals[j]));
						$(tFoot).append(a);	
					}
				}
		},					
			"initComplete":function(){					
			},
			"bInfo" : false
		});

	}
	
	function drawChildTable(data){

		if(typeof childTable === 'undefined'  ){
            if(data.aaData.length > 0){
               var showColumns = ['Period', col_id, 'Company Name', 'Department','Employee Name', 'Net Salary', 'Total Gross Salary', 'Total ER Cost'];
                var showColumnsIndices = [];
                for(var i= 0; i<data.aoColumns.length; i++){
                    if($.inArray(data.aoColumns[i]['mDataProp'], showColumns) == -1){
                        showColumnsIndices.push(i);
                    }
                }
               var $container = $('#childTable');
               childTable = $('#childTable').dataTable({

                    "aaData" :data.aaData,
                    "aoColumns": data.aoColumns,
                    "sDom": '<"block-inline pull-left"C>lfrtip<"toolbar">',
                    "oColVis": {
                        "bRestore": true,
                        "align": "left"
                    },
                    "destroy": true,
                    "scrollX": "100%",
                    "bFilter": false,
                    "bLengthChange": false,
                    "oTableTools": {
                        "sRowSelect": "single"
                    },
                    "language": {
                        "decimal": ",",
                        "thousands": ","
                    },
                    "columnDefs": [
                        {
                            "targets": showColumnsIndices,
                            "visible": false
                        },
                    ],
                    "footerCallback": function(tFoot, aaData, iStart, iEnd, aiDisplay){

                        var api = this.api();
                        $($(tFoot).children()).remove();

                        var aRow = aaData[0];
                        var pCnt = 0;
                        var properties = [];
                        for(var p in aRow) {
                            if (aRow.hasOwnProperty(p) ){
                                pCnt += 1;
                                properties.push(p);
                            }
                        }
                        var totals = [];
                        for (var j=0; j < pCnt; j += 1) {
                            totals[j] = 0;
                        }
                        for(var i= 0; i<aaData.length; i++){
                            // for each column
                            for (var j=0; j < pCnt; j += 1) {
                                var propName = properties[j];
                                var t;
                                if (aaData[i][propName] && ( !isNaN(t = parseFloat(aaData[i][propName])))) {
                                    totals[j] += t;
                                  //  console.log("total[" + i + ", " + j + "]=" + totals[j] + " " + propName);
                                }
                            }
                        }
                        var myTable = $('#grossToNet');
                        for (var j=0; j < pCnt; j += 1) {
                            var a = document.createElement('th');

                            if(api.column(j).visible()){
                                $(a).html(j < 8 ? '' : numberWithCommas("" + totals[j]));
                                $(tFoot).append(a);
                            }
                        }
                    },
                    "initComplete":function(){
                    },
                    "bInfo" : false
                })
            }
            else{
                alert('Please widen the search no results found');
            }
		}
		else {
			oSettings = childTable.fnSettings();
			childTable.fnClearTable(this);
            showChildColumns = ['Period', col_id, 'Company Name','Employee Name','Net Salary', 'Total Gross Salary', 'Total ER Cost'];

			for (var i=0; i<data.aaData.length; i++){
					childTable.oApi._fnAddData(oSettings, data.aaData[i]);
				}
            for (var i=0; i<oSettings.aoColumns.length; i++){
                if($.inArray(oSettings.aoColumns[i]['mDataProp'], showChildColumns) == -1){
                    var bVis = false;
                    var iCol = i;
                    childTable.fnSetColumnVis(iCol, bVis, true);
                }
                else{
                    var bVis = true;
                    var iCol = i;
                    childTable.fnSetColumnVis(iCol, bVis, true);
                }

                oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
			    childTable.fnDraw();

		}
	}
    }


	function writeSelectHeadings(t){
		var api = t.api();
		var oSettings = t.fnSettings();
		var col_id = oSettings.aoColumns;
        var selectBoxes = $('<div id="selectBoxes" class="block-inline pull-left"></div>').appendTo($('#grossToNet_wrapper .ColVis'));

		var select = $('<select id="regionSelect" class="multiselect" data-role="multiselect" multiple="multiple" title="Select Region" data-name="Select Region"></select>')
			.appendTo($('#selectBoxes'));

			select.append('<option value="AMERICAS">AMERICAS</option>'+
						 '<option value="EMEA">EMEA</option>'+
						 '<option value="BENELUX">BENELUX</option>'+
						 '<option value="APAC">APAC</option>');	

		var selectCountry = $('<select id="countrySelect" class="multiselect" data-role="multiselect" multiple="multiple" title="Select Country" data-name="Select Country"></select>')
			.appendTo($('#selectBoxes'));
			selectCountry.append(' <option value="UK">UK</option>'+
			                      '<option value="CN">CN</option>'+
			                      '<option value="IE">IE</option>'+
			                      '<option value="NL">NL</option>'+
			                      '<option value="SG">SG</option>'+
								  '<option value="US">US</option>');
								  
		/* var selectCostCenter = $('<select id="costCenterSelect" class="multiselect" data-role="multiselect" multiple="multiple" title="Select Cost Center" data-name="Select Cost Center" ></select>')
			.appendTo($('#selectBoxes'));
			selectCostCenter.append(    '<option value="UKMKXXXXXX">UKMKXXXXXX</option>'+
										'<option value="UKMAXXXXXX">UKMAXXXXXX</option>'+
										'<option value="UKTEXXXXXX">UKTEXXXXXX</option>'+
										'<option value="UKBSXXXXXX">UKBSXXXXXX</option>'+
										'<option value="30180835">30180835</option>'+
										'<option value="30180853">30180853</option>'+
										'<option value="30180606">30180606</option>'+
										'<option value="30181036">30181036</option>'+
										'<option value="30180038">30180038</option>'+
										'<option value="30185326">30185326</option>'+
										'<option value="30181361">30181361</option>'+
										'<option value="30180908">30180908</option>'+
										'<option value="30180978">30180978</option>'+
										'<option value="2956">2956</option>'+
										'<option value="20160326 - 20160326">20160326 - 20160326</option>'+
										'<option value="20160923 - 20160923">20160923 - 20160923</option>'+
										'<option value="20160322 - 20160322">20160322 - 20160322</option>'+
										'<option value="20160322 - 20160323">20160322 - 20160323</option>'+
										'<option value="30130885">30130885</option>'+
										'<option value="30130883">30130883</option>'+
										'<option value="30130886">30130886</option>'+
										'<option value="30130494">30130494</option>'+
										'<option value="30130798">30130798</option>'+
										'<option value="30130036">30130036</option>'+
										'<option value="30130713">30130713</option>'+
										'<option value="30130712">30130712</option>'+
										'<option value="30130710">30130710</option>'); */
										
		/* var selectGroup = $('<select id="group" class="multiselect" data-role="multiselect" multiple data-name="Select Group"><option vlaue="">Select Group</option></select>')
			.appendTo($('#selectBoxes'));
			selectGroup.append('<option value="Hinder Group">Hinder Group</option>'+
							   '<option value="Frozen Group">Frozen Group</option>'+
							   '<option value="Maxwell Group">Maxwell Group</option>'); */
							   
		initMultiSelect();

        var reportRange = $('<div id="reportRange" class="btn default"><i class="fa fa-calendar"></i>&nbsp; <span> </span><b class="fa fa-angle-down"></b></div>')
            .appendTo($('#selectBoxes'));
        initDatePicker();

		var searchButton = $('<div class="btn-group"><button type="button" id="refineSearch" class="multiselect btn btn-default">Submit Search</button></div>')
			.appendTo($('#selectBoxes'));



	};

     var resetButton = $('<div class="btn-group pull-right"><button type="button" id="reset" class="multiselect btn btn-default ">Reset Search</button></div>')
          .appendTo($('#drilldown'));

     $('body').on('click', '#reset', function(){
         $('#selectBoxes').empty();
         writeSelectHeadings(table);
         initMultiSelect();
         selectedRegion = [];
         selectedCountry = [];
         selectedCostCenter = [];
         selectedGroup = [];
     })

	function updateMultiSelect(selectBox, selected){
		$.ajax({
			url: 'multiselect.php',
			"type": 'POST',
			"data": {selected: selected,
					 column: selectBox},
			'success': function(data){	
				json = JSON.parse(data);
				
				if(json.region != null){
				regionSelect.multiselect('dataprovider', json.region);
				regionSelect.multiselect('rebuild');
				regionSelect.multiselect('select', selectedRegion);
				}
				
				if(json.country != null){
				countrySelect.multiselect('dataprovider', json.country);
				countrySelect.multiselect('rebuild');
				countrySelect.multiselect('select', selectedCountry );
				}
				
				if(json.costCenter != null){
				costCenterSelect.multiselect('dataprovider', json.costCenter);
				costCenterSelect.multiselect('rebuild');
				costCenterSelect.multiselect('select', selectedCostCenter );
				}
				
				if(json.group != null){
				groupSelect.multiselect('dataprovider', json.group);
				groupSelect.multiselect('rebuild');
				groupSelect.multiselect('select', selectedGroup );
				}
			},
			'error': function(d){
				console.log(d);
			}
		});
	}	
	
	function initMultiSelect(){
		countrySelect = $('#countrySelect').multiselect({
			includeSelectAllOption: true,
			buttonText: function(options, select) {
				if (options.length == 0) {
					return 'Select Country <b class="caret"></b>';
				}
				else if (options.length > 3) {
					return options.length + ' selected <b class="caret"></b>';
				}
				else {
					var selected = '';
					options.each(function() {
						selected += $(this).text() + ', ';
					});
					return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
				}
			},
			onDropdownHide: function (element){
				if($('#countrySelect option:selected').length != 0){
					var selected = [];
					selectedCountry = [];
					var o = $('#countrySelect option:selected');
					$(o).each(function(index, country){
						selectedCountry.push($(this).val());
						selected.push([$(this).val()]);
					});
					updateMultiSelect("Country", selected);
					
				}
			}
		});
	
		regionSelect = $('#regionSelect').multiselect({	
			includeSelectAllOption: true,
			buttonText: function(options, select) {
				if (options.length == 0) {
					return 'Select Region <b class="caret"></b>';
				}
				else if (options.length > 3) {
					return options.length + ' selected <b class="caret"></b>';
				}
				else {
					var selected = '';
					options.each(function() {
						selected += $(this).text() + ', ';
					});
					return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
				}
			},
			onDropdownHide: function (element){
				if($('#regionSelect option:selected').length != 0){
				
					var selected = [];
					selectedRegion = [];
					var o = $('#regionSelect option:selected');
					$(o).each(function(index, region){
						selectedRegion.push($(this).val());
						selected.push([$(this).val()]);
					});
					updateMultiSelect("Region", selected);
				}
			}
		});
		
		costCenterSelect = $('#costCenterSelect').multiselect({	
			includeSelectAllOption: true,
			buttonText: function(options, select) {
				if (options.length == 0) {
					return 'Select Cost Center <b class="caret"></b>';
				}
				else if (options.length > 3) {
					return options.length + ' selected <b class="caret"></b>';
				}
				else {
					var selected = '';
					options.each(function() {
						selected += $(this).text() + ', ';
					});
					return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
				}
			},
			onDropdownHide: function (element){
				if($('#costCenterSelect option:selected').length != 0){
					var selected = [];
					selectedCostCenter = [];
					var o = $('#costCenterSelect option:selected');
					$(o).each(function(index, costCenter){
						selectedCostCenter.push($(this).val());
						selected.push([$(this).val()]);
					});
					updateMultiSelect("Cost Center", selected);
				}
			}
		});
		
		groupSelect = $('#group').multiselect({
			includeSelectAllOption: true,
			buttonText: function(options, select) {
				if (options.length == 0) {
					return 'Select Group <b class="caret"></b>';
				}
				else if (options.length > 3) {
					return options.length + ' selected <b class="caret"></b>';
				}
				else {
					var selected = '';
					options.each(function() {
						selected += $(this).text() + ', ';
					});
					return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
				}
			},
			onDropdownHide: function (element){
				if($('#group option:selected').length != 0){
					var selected = [];
					selectedGroup = [];
					var o = $('#group option:selected');
					$(o).each(function(index, group){
						selectedGroup.push($(this).val());
						selected.push([$(this).val()]);
					});
					updateMultiSelect("Group", selected);
				}
			}
		});	
	}
	
	function drillDown ( d ) {
		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			'<tr>'+
				'<td>Full name:</td>'+
				'<td>'+d.name+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Extension number:</td>'+
				'<td>'+d.extn+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Extra info:</td>'+
				'<td>And any further details here (images etc)...</td>'+
			'</tr>'+
		'</table>';
	}
	
	function addEventListenerToCell(t, n){
		var api = t.api();
		// Add event listener for opening and closing details
		$( n+" tbody").on('click', 'td', function () {
			var tr = $(this).closest('tr');
			var aData = table.fnGetData(api.row(tr));
            var period = aData.Period;
			var comp_id = aData.comp_id;
			var position = table.fnGetPosition(this);
			var col = position[2];
			var oSettings = table.fnSettings();
			col_id = oSettings.aoColumns[col].sTitle;
			var col_data = this.innerText;
			var row = api.row( tr );
			if(n == "#grossToNet"){
				getChildTable(comp_id, col_id, col_data, period);
				}
			else {
				showCellDetails(row);
				}
		} );
	}
	
	function showCellDetail (){
		if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
		}
		else {
			// Open this row
			row.child( drillDown(row.data()) ).show();
			tr.addClass('shown');
		}
	}
	
	function getChildTable(comp_id, column, col_data, period){
	$.ajax({
		"dataType": 'JSON',
		"url": 'gross_to_net.php',
		"data": {   action: 'getChildTable',
				    comp_id: comp_id,
				    column: column,
				    col_data: col_data,
                    period: period},
		"type": "POST",
		"success": function(data){
			drawChildTable(data);
		}
	})
	}	

	$('.time-line-control').on("click", function(e){
		e.preventDefault();
		$("i",this).toggleClass("icon-circle-arrow-up icon-circle-arrow-down");
		$(this).toggleClass("fa fa-chevron-down fa fa-chevron-up");
		$(this).next('.timeline-content').slideToggle(1000);
	});
	
	
	$('body').on('click', '#refineSearch', function(e){
		$.ajax({
			"dataType": 'JSON',
			"url": 'gross_to_net.php',
			"data": { action: 'getWhereTable',
					  region: selectedRegion,
					  country: selectedCountry,
					  costCenter: selectedCostCenter,
					  group: selectedGroup,
                      start: startRange,
                      end: endRange},
			"type": "POST",
			"success": function(data){
				oSettings = table.fnSettings();
				table.fnClearTable(this);

				for (var i=0; i<data.aaData.length; i++){
					table.oApi._fnAddData(oSettings, data.aaData[i]);


				}

                for(var i=0; i<data.aoColumns.length; i++){
                   if(data.aoColumns[i].show == false){
                        var bVis = false;
                        var iCol = i;
                        table.fnSetColumnVis(iCol, bVis, true);
                   }
                    else{
                        var bVis = true;
                        var iCol = i;
                       table.fnSetColumnVis(iCol, bVis, true);
                   }
                }

              /*  var columnDefs =  [
                    {
                        "targets": [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 ,28],
                        "visible": false
                    },
                    {
                        "render": function ( data, type, row ) {
                            if(data != null){
                                return numberWithCommas(data);
                            }
                            else{
                                return data;
                            }
                        },
                        "targets": [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
                    },
                    {
                        "targets" : [2, 3,4,5,6,7,8,9,],
                        "createdCell": function(td, cellData, rowData, row, col){
                            $(td).append('<i class="fa fa-chevron-down pull-right"></i>');
                        }
                    }]; */


                   // console.log(oSettings.columnDefs);
                table.oApi._fnApplyColumnDefs(oSettings, columnDefs, oSettings.aoColumns);
                table.oApi._fnSetCellData(oSettings, 0, 0, "dfjklsjlsj");

				oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
				table.fnDraw();
			}		 
		});
	});


      function initDatePicker(){
          dateRange = $('#reportRange').daterangepicker({
                  opens: (App.isRTL() ? 'left' : 'right'),
                  startDate: moment().subtract('days', 29),
                  endDate: moment(),
                  minDate: '01/01/1970',
                  maxDate: '12/31/2014',
                  showDropdowns: true,
                  showWeekNumbers: true,
                  timePicker: false,
                  timePickerIncrement: 1,
                  timePicker12Hour: true,
                  ranges: {
                     // 'Today': [moment(), moment()],
                    //  'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                      'Last Period': [moment().startOf('month').subtract('months', 1), moment()],
                      'Last Quarter': [moment().startOf('month').subtract('months', 3), moment()]
                    //  'This Month': [moment().startOf('month'), moment().endOf('month')],
                    //  'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                  },
                  buttonClasses: ['btn'],
                  applyClass: 'green',
                  cancelClass: 'default',
                  format: 'MM/DD/YYYY',
                  separator: ' to ',
                  locale: {
                      applyLabel: 'Apply',
                      fromLabel: 'From',
                      toLabel: 'To',
                      customRangeLabel: 'Custom Range',
                      daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                      monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                      firstDay: 1
                  }
              },
              function (start, end) {

                  startRange = start.format('YYYYMMDD').slice(0, -2).toString();
                  endR= parseInt(end.format('YYYYMMDD').slice(0,-2))-1;
                  endRange = endR.toString();
                 // console.log(startRange);
                 // console.log(endRange);
                  $('#reportRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              }
          );
          $('#reportRange span').html(moment().subtract('days', 29).format('YYYY MM') + ' - ' + moment().format('YYYY MM'));

      }
	
 
	