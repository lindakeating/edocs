/**
 * Created by janitor on 23/06/14.
 */
var table;
var childTable;
var regionSelect;
var countrySelect;
var selectedRegion = [];
var selectedCountry = [];
var selectedCostCenter = [];
var selectedGroup = [];
var vAaData = [];

// TODO: Let user select period1 and period2
var period1 = '201405';
var period2 = '201404';

// Render columns in here as regular text
var textCols = {
  'Period':true, 'Region': true,  Group: true, Country: true, Department: true, 'Cost Center': true, 'comp_id': true
};

$.ajax({
    "dataType": 'JSON',
    "url": 'variance.php',
    "type": 'POST',
    'data': {
        action: 'getParentTable',
        period1: period1,
        period2: period2
    },
    "success": function(data){
        tableName = "#grossToNet";
        drawTable(data);
        addEventListenerToCell(table, tableName);
        writeSelectHeadings(table);
        drawChart(data);
    }
});

// TODO: Change chart to show variance or remove chart
function drawChart(d){
    var datasets = [];
    $.each(d.aaData, function(i, val){
        arr = [];
        arr.push([1, val['Net Salary']]);
        arr.push([2, val['Total Gross Salary']]);
        arr.push([3, val['Total ER Cost']]);
        object = new Object();
        object.label = val['Country'];
        object.label = val['Country'];
        object.data = arr;
        datasets.push(object);

        var options = {
            series: {
                lines: { show: true },
                points: {
                    radius: 3,
                    show: true
                }
            },
            paging: false,
            yaxis: { tickLength:0 },
            xaxis: { tickLength:0 },
            xaxis: {
                ticks: [[1,'Net Salary'],[2,'Total Gross Salary'],[3,'Total ER Cost']]
            },
            legend : {
                container: $("#legend"),
                show: true,
                position: "ne",
                backgroundColor: "green",
                backgroundOpacity: 0.5
            }
        };

        //$.plot($('#placeholder'), datasets, options);
    });
}

function drawSparkLineChart(d){
    sparkValues = [];
    $.each(d.aaData, function(i, val){
        values = [];
        values.push(val['Total Gross Salary']);
        values.push(val['Total Taxable Salary']);
        values.push(val['Total ER Cost']);
        sparkValues.push(values);
    });
    $('#chart').sparkline(sparkValues, {type: 'bar',height: '8.5em', width: '1000px',  enableTagOptions: true , barWidth: '20px'});
}

function numberWithCommas(x) {
    if (x == undefined || !x ) {
        return '';
    }
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function drawTable(data){
    console.log(data.aoColumns);
    table = $('#grossToNet').dataTable({
        "aaData" :data.aaData,
        "aoColumns": data.aoColumns,
        "sDom": '<"block-inline pull-left"C>lfrtip<"toolbar">',
        "oColVis": {
            "bRestore": true,
            "align": "left"
        },
        "destroy": true,
        "paging": false,
        "jQueryUI": true,
        "destroy": true,
        "scrollX": "100%",
        "bFilter": false,
        "bLengthChange": false,
        "oTableTools": {
            "sRowSelect": "single"
        },
        "language": {
            "decimal": ",",
            "thousands": ","
        },
        "columnDefs": [
            {
                "targets": [2,3,4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,28 ],
                "visible": false
            },
            {
                "render": function ( data, type, row ) {
                    if(data != null && (data.length > 2)) {
                        var colName = data[2];
                        if (textCols[colName]) {
                            return data[0];
                        }
                        var val1 = numberWithCommas(data[0]);
                        var val2 = numberWithCommas(data[1]);
                        var percent = Math.round((Math.abs(data[0]) > 0.00001) ? (data[1] * 100)/data[0] : 0);
                        var s= '<table class="varianceTable">' +
                            '<thead><th><b>Period #1</b></th><th><b>Period #2</b></th><th><b>Difference</b></th><th><b>Percent</b></th></thead>' +
                            '<tbody><tr>' +
                            '<td>' + val1 +'</td>' +
                            '<td>' + val2 +'</td>' +
                            '<td>' + numberWithCommas(data[0] - data[1]) +'</td>' +
                            '<td>' + (100 - percent) +'%</td>' +
                            '</tr></tbody>' +
                            '</table>';
                        console.log(s);
                        return s;
                    }
                    else{
                        return null;
                    }
                },

                "render_orig": function ( data, type, row ) {
                    if(data != null && (data.length > 2)) {
                        var colName = data[2];
                        if (textCols[colName]) {
                            return data[0];
                        }
                        var val = numberWithCommas(data[0]);
                        var percent = Math.round((Math.abs(data[0]) > 0.00001) ? (data[1] * 100)/data[0] : 0);
                        if (percent == 0 || Math.abs(percent) > 9999999) {
                            return val;
                        } else {
                            return val + " (" + percent + "%)";
                        }
                    }
                    else{
                        return null;
                    }
                },


                //"targets": [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
                "targets": [0, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
            }
        ],
        "footerCallback": function(tFoot, aaData, iStart, iEnd, aiDisplay){

            var api = this.api();
            $($(tFoot).children()).remove();

            var aRow = aaData[0];
            var pCnt = 0;
            var properties = [];
            for(var p in aRow) {
                if (aRow.hasOwnProperty(p) ){
                    pCnt += 1;
                    properties.push(p);
                }
            }
            var totalsP1 = [];
            var totalsP2 = [];
            for (var j=0; j < pCnt; j += 1) {
                totalsP1[j] = 0;
                totalsP2[j] = 0;
            }
            for(var i= 0; i<aaData.length; i++){
                // for each column
                for (var j=0; j < pCnt; j += 1) {
                    var propName = properties[j];
                    var t;
                    if (aaData[i][propName] && ( !isNaN(t = parseFloat(aaData[i][propName][0])))) {
                        totalsP1[j] += t;
                    }
                    if (aaData[i][propName] && ( !isNaN(t = parseFloat(aaData[i][propName][1])))) {
                        totalsP2[j] += t;
                    }
                }
            }
            var myTable = $('#grossToNet');
            for (var j=0; j < pCnt; j += 1) {
                var a = document.createElement('th');

                if(api.column(j).visible()){
                    var s = '';
                    if (j >= 5) {
                        var t1 = totalsP1[j];
                        var t2 = totalsP2[j];
                        var val = numberWithCommas(t1);
                        var percent = Math.round((Math.abs(t1) > 0.00001) ? (t2 * 100)/t1 : 0);
                        if (percent == 0 || Math.abs(percent) > 9999999) {
                            s = val;
                        } else {
                            s = val + " (" + percent + "%)";
                        }
                    }
                    $(a).html(s);
                    $(tFoot).append(a);
                }
            }
        },
        "initComplete":function(){
        },
        "bInfo" : false
    });

}

function drawChildTable(data){
    if(typeof childTable === 'undefined'){
        childTable = $('#childTable').dataTable({
            "aaData" :data.aaData,
            "aoColumns": data.aoColumns,
            "sDom": '<"block-inline pull-left"C>lfrtip<"toolbar">',
            "oColVis": {
                "bRestore": true,
                "align": "left"
            },
            "destroy": true,
            "scrollX": "100%",
            "bFilter": false,
            "bLengthChange": false,
            "oTableTools": {
                "sRowSelect": "single"
            },
            "language": {
                "decimal": ",",
                "thousands": ","
            },

            "columnDefs": [
                {
                    "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,27,28],
                    "visible": false
                },
                {
                    "render": function ( data, type, row ) {
                        if(data != null && (data.length > 2)) {
                            var colName = data[2];
                            if (textCols[colName]) {
                                return data[0];
                            }
                            var val = numberWithCommas(data[0]);
                            var percent = Math.round((Math.abs(data[0]) > 0.00001) ? (data[1] * 100)/data[0] : 0);
                            if (percent == 0 || Math.abs(percent) > 9999999) {
                                return val;
                            } else {
                                return val + " (" + percent + "%)";
                            }
                        }
                        else{
                            return null;
                        }
                    },
                    "targets": [0, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]
                }
            ],
            "footerCallback": function(tFoot, aaData, iStart, iEnd, aiDisplay){

                var api = this.api();
                $($(tFoot).children()).remove();

                var aRow = aaData[0];
                var pCnt = 0;
                var properties = [];
                for(var p in aRow) {
                    if (aRow.hasOwnProperty(p) ){
                        pCnt += 1;
                        properties.push(p);
                    }
                }
                var totals = [];
                for (var j=0; j < pCnt; j += 1) {
                    totals[j] = 0;
                }
                for(var i= 0; i<aaData.length; i++){
                    // for each column
                    for (var j=0; j < pCnt; j += 1) {
                        var propName = properties[j];
                        var t;
                        if (aaData[i][propName] && ( !isNaN(t = parseFloat(aaData[i][propName])))) {
                            totals[j] += t;
                            console.log("total[" + i + ", " + j + "]=" + totals[j] + " " + propName);
                        }
                    }
                }
                var myTable = $('#grossToNet');
                for (var j=0; j < pCnt; j += 1) {
                    var a = document.createElement('th');

                    if(api.column(j).visible()){
                        $(a).html(j < 5 ? '' : numberWithCommas("" + totals[j]));
                        $(tFoot).append(a);
                    }
                }
            },
            "initComplete":function(){
            },
            "bInfo" : false
        });
    }
    else {
        oSettings = childTable.fnSettings();
        childTable.fnClearTable(this);
        for (var i=0; i<data.aaData.length; i++){
            table.oApi._fnAddData(oSettings, data.aaData[i]);
        }
        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
        childTable.fnDraw();

    }
}


function writeSelectHeadings(t){
    var api = t.api();
    var oSettings = t.fnSettings();
    var col_id = oSettings.aoColumns;
    var selectBoxes = $('<div id="selectBoxes" class="block-inline pull-left"></div>').appendTo($('#grossToNet_wrapper .ColVis'));

    var select = $('<select id="regionSelect" class="multiselect" data-role="multiselect" multiple="multiple" title="Select Region" data-name="Select Region"></select>')
        .appendTo($('#selectBoxes'));

    select.append('<option value="AMERICAS">AMERICAS</option>'+
        '<option value="EMEA">EMEA</option>'+
        '<option value="BENELUX">BENELUX</option>'+
        '<option value="APAC">APAC</option>');

    var selectCountry = $('<select id="countrySelect" class="multiselect" data-role="multiselect" multiple="multiple" title="Select Country" data-name="Select Country"></select>')
        .appendTo($('#selectBoxes'));
    selectCountry.append(' <option value="UK">UK</option>'+
        '<option value="CN">CN</option>'+
        '<option value="IE">IE</option>'+
        '<option value="NL">NL</option>'+
        '<option value="SG">SG</option>'+
        '<option value="US">US</option>');

    var startPeriod = $('<select id="startPeriod" title="Select Country" data-name="Select Country"></select>')
        .appendTo($('#selectBoxes'));
    startPeriod.append(
        '<option selected disabled>Select From</option>'+
        '<option value="201401">January 2014</option>'+
        '<option value="201402">February 2014</option>'+
        '<option value="201403">March 2014</option>'+
        '<option value="201404">April 2014</option>'+
        '<option value="201405">May 2014</option>');

    var endPeriod = $('<select id="endPeriod"  title="Select Country" data-name="Select Country"></select>')
        .appendTo($('#selectBoxes'));
    endPeriod.append(
            '<option selected disabled>Select To</option>'+
            '<option value="201402">February 2014</option>'+
            '<option value="201403">March 2014</option>'+
            '<option value="201404">April 2014</option>'+
            '<option value="201405">May 2014</option>');


    initMultiSelect();

    var searchButton = $('<div class="btn-group"><button type="button" id="refineSearch" class="multiselect btn btn-default">Submit Search</button></div>')
        .appendTo($('#selectBoxes'));



};

var resetButton = $('<div class="btn-group pull-right"><button type="button" id="reset" class="multiselect btn btn-default ">Reset Search</button></div>')
    .appendTo($('#drilldown'));

$('body').on('click', '#reset', function(){
    $('#selectBoxes').empty();
    writeSelectHeadings(table);
    initMultiSelect();
    selectedRegion = [];
    selectedCountry = [];
    selectedCostCenter = [];
    selectedCostCenter = [];
    selectedGroup = [];
})

function updateMultiSelect(selectBox, selected){
    $.ajax({
        url: 'multiselect.php',
        "type": 'POST',
        "data": {selected: selected,
            column: selectBox},
        'success': function(data){
            json = JSON.parse(data);

            if(json.region != null){
                regionSelect.multiselect('dataprovider', json.region);
                regionSelect.multiselect('rebuild');
                regionSelect.multiselect('select', selectedRegion);
            }

            if(json.country != null){
                countrySelect.multiselect('dataprovider', json.country);
                countrySelect.multiselect('rebuild');
                countrySelect.multiselect('select', selectedCountry );
            }

            if(json.costCenter != null){
                costCenterSelect.multiselect('dataprovider', json.costCenter);
                costCenterSelect.multiselect('rebuild');
                costCenterSelect.multiselect('select', selectedCostCenter );
            }

            if(json.group != null){
                groupSelect.multiselect('dataprovider', json.group);
                groupSelect.multiselect('rebuild');
                groupSelect.multiselect('select', selectedGroup );
            }
        },
        'error': function(d){
            console.log(d);
        }
    });
}

function initMultiSelect(){
    countrySelect = $('#countrySelect').multiselect({
        includeSelectAllOption: true,
        buttonText: function(options, select) {
            if (options.length == 0) {
                return 'Select Country <b class="caret"></b>';
            }
            else if (options.length > 3) {
                return options.length + ' selected <b class="caret"></b>';
            }
            else {
                var selected = '';
                options.each(function() {
                    selected += $(this).text() + ', ';
                });
                return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
            }
        },
        onDropdownHide: function (element){
            if($('#countrySelect option:selected').length != 0){
                var selected = [];
                selectedCountry = [];
                var o = $('#countrySelect option:selected');
                $(o).each(function(index, country){
                    selectedCountry.push($(this).val());
                    selected.push([$(this).val()]);
                });
                updateMultiSelect("Country", selected);

            }
        }
    });

    regionSelect = $('#regionSelect').multiselect({
        includeSelectAllOption: true,
        buttonText: function(options, select) {
            if (options.length == 0) {
                return 'Select Region <b class="caret"></b>';
            }
            else if (options.length > 3) {
                return options.length + ' selected <b class="caret"></b>';
            }
            else {
                var selected = '';
                options.each(function() {
                    selected += $(this).text() + ', ';
                });
                return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
            }
        },
        onDropdownHide: function (element){
            if($('#regionSelect option:selected').length != 0){

                var selected = [];
                selectedRegion = [];
                var o = $('#regionSelect option:selected');
                $(o).each(function(index, region){
                    selectedRegion.push($(this).val());
                    selected.push([$(this).val()]);
                });
                updateMultiSelect("Region", selected);
            }
        }
    });

    costCenterSelect = $('#costCenterSelect').multiselect({
        includeSelectAllOption: true,
        buttonText: function(options, select) {
            if (options.length == 0) {
                return 'Select Cost Center <b class="caret"></b>';
            }
            else if (options.length > 3) {
                return options.length + ' selected <b class="caret"></b>';
            }
            else {
                var selected = '';
                options.each(function() {
                    selected += $(this).text() + ', ';
                });
                return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
            }
        },
        onDropdownHide: function (element){
            if($('#costCenterSelect option:selected').length != 0){
                var selected = [];
                selectedCostCenter = [];
                var o = $('#costCenterSelect option:selected');
                $(o).each(function(index, costCenter){
                    selectedCostCenter.push($(this).val());
                    selected.push([$(this).val()]);
                });
                updateMultiSelect("Cost Center", selected);
            }
        }
    });

    groupSelect = $('#group').multiselect({
        includeSelectAllOption: true,
        buttonText: function(options, select) {
            if (options.length == 0) {
                return 'Select Group <b class="caret"></b>';
            }
            else if (options.length > 3) {
                return options.length + ' selected <b class="caret"></b>';
            }
            else {
                var selected = '';
                options.each(function() {
                    selected += $(this).text() + ', ';
                });
                return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
            }
        },
        onDropdownHide: function (element){
            if($('#group option:selected').length != 0){
                var selected = [];
                selectedGroup = [];
                var o = $('#group option:selected');
                $(o).each(function(index, group){
                    selectedGroup.push($(this).val());
                    selected.push([$(this).val()]);
                });
                updateMultiSelect("Group", selected);
            }
        }
    });
}

function drillDown ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
        '<td>Full name:</td>'+
        '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Extension number:</td>'+
        '<td>'+d.extn+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Extra info:</td>'+
        '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
        '</table>';
}

function addEventListenerToCell(t, n){
    var api = t.api();
    // Add event listener for opening and closing details
    $( n+" tbody").on('click', 'td', function () {
        var tr = $(this).closest('tr');
        var aData = table.fnGetData(api.row(tr));
        var comp_id = aData.comp_id;
        var position = table.fnGetPosition(this);
        var col = position[1];
        var oSettings = table.fnSettings();
        var col_id = oSettings.aoColumns[col].sTitle;
        var col_data = this.innerText;
        var row = api.row( tr );
        if(n == "#grossToNet"){
            getChildTable(comp_id, col_id, col_data);
        }
        else {
            showCellDetails(row);
        }
    } );
}

function showCellDetail (){
    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    }
    else {
        // Open this row
        row.child( drillDown(row.data()) ).show();
        tr.addClass('shown');
    }
}

function getChildTable(comp_id, column, col_data){
    $.ajax({
        "dataType": 'JSON',
        "url": 'variance.php',
        "data": {action: 'getChildTable',
            comp_id: comp_id,
            column: column,
            col_data: col_data,
            period1: period1,
            period2: period2
            },

        "type": "POST",
        "success": function(data){
            drawChildTable(data);
        }
    })
}

$('.time-line-control').on("click", function(e){
    e.preventDefault();
    $("i",this).toggleClass("icon-circle-arrow-up icon-circle-arrow-down");
    $(this).toggleClass("fa fa-chevron-down fa fa-chevron-up");
    $(this).next('.timeline-content').slideToggle(1000);
});


$('body').on('click', '#refineSearch', function(e){
    $.ajax({
        "dataType": 'JSON',
        "url": 'variance.php',
        "data": { action: 'getWhereTable',
            region: selectedRegion,
            country: selectedCountry,
            costCenter: selectedCostCenter,
            group: selectedGroup,
            period1: period1,
            period2: period2
        },
        "type": "POST",
        "success": function(data){
            oSettings = table.fnSettings();
            table.fnClearTable(this);

            for (var i=0; i<data.aaData.length; i++){
                table.oApi._fnAddData(oSettings, data.aaData[i]);
            }
            // only iterate over the first 5 columns
            for(var i=0; i<5; i++){
                if(data.aoColumns[i].show == false){
                    var bVis = false;
                    var iCol = i;
                    table.fnSetColumnVis(iCol, bVis, true);
                }
                else{
                    var bVis = true;
                    var iCol = i;
                    table.fnSetColumnVis(iCol, bVis, true);
                }
            }

            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            table.fnDraw();
        }
    });
});

$('body').on('change', '#startPeriod', function(e){
   period1 =  $( "#startPeriod" ).val();
    console.log(period1);
});

$('body').on('change', '#endPeriod', function(e){
    period2 = $( "#endPeriod" ).val();
    console.log(period2);
});





