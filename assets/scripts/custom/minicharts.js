function miniCharts(data, chartType){
		
	$("#sparkline_bar").sparkline(data[0], chartType);
	
	$("#sparkline_bar2").sparkline(data[1], chartType);
	
	$("#sparkline_line").sparkline(data[2], chartType);
				
	$("#sparkline_line3").sparkline(data[3], chartType);
								
}

function loadCharts(){
		professionalProfile = [		
				[48,52,67,34, 34],
				[59,41,23,64,42],
				[52,48, 23,53,21],
				[22,10,23,45,62]];
				
		ageProfile = [
				[48,52,67,34,34],
				[59,41,23,64,42],
				[52,48, 23,53,21],
				[22,10,23,45,62]];
				
		genderProfile = [		
					[48,52],
					[59,41],
					[52,48],
					[22,10]];
					
		genderObj ={
					type: 'pie',
					width: '80',
					height: '80',
					sliceColors: ['#27A9E3','#852B99']
				}
		
		professionalObj ={
					type: 'line',
					width: '80',
					height: '80',
					sliceColors: ['#27A9E3','#852B99']
				};
		ageObject = {
					type: 'bar',
					width: '80',
					height: '80',
					sliceColors: ['#27A9E3','#852B99']
				};
			
		if(event.toElement.name == 'profile'){
			miniCharts(professionalProfile, professionalObj);
				
		}
		
		if(event.toElement.name == 'age'){
			miniCharts(ageProfile, ageObject);		
		}
		
		if(event.toElement.name == 'gender'){
			miniCharts(genderProfile, genderObj);		
		}

}


