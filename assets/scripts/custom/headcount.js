/**
 * Created by Linda on 21/06/14.
 */

//get Chart data
var table;
var selectSettings;
var dateRange;
var startRange;
var endRange;
var chart;
var selected;

$.ajax({
    "dataType": 'JSON',
    "url": 'headcount.php',
    "type": 'POST',
    'data': {action: 'initTable'},
    "success": function(data){
        drawChart(data);
        drawTable(data);
        writeSelectHeadings(table);
    },
    "error": function(e){
        console.log(e);
    }
});


$('body').on('click','#refineSearch',function(){
    selection = $('#selectSettings option:selected');
    selected = [];
    $(selection).each(function(i,v){
        selected.push(v.innerText)
    })
    refineSearch(selected, startRange, endRange);
})

function drawChart(d){
    console.log(d);
    categories = [];
    series = [];
    for(var i=0; i< d.aaData.length; i++){
        categories.push(d.aaData[i].Region);
    }

    for(var i=0; i< d.aaData.length; i++){
        obj = {};
        obj.type = 'column';
        obj.name = d.aaData[i].Region;
        fte = parseInt(d.aaData[i]['Total FTE']);
        joiners = parseInt(d.aaData[i]['Joiners']);
        leavers = parseInt(d.aaData[i]['Leavers']);
        obj.data = [fte,joiners, leavers]
        series.push(obj);
    }

    var $container = $('#headcountChart');

    var options = {
        chart: {
            renderTo: $container[0],
            height: 300
        },
        colors: ['Teal', 'Red','LightSteelBlue', 'Orange' ],
        title: {
            text: 'Joiners / Leavers'
        },
        xAxis: {
            categories: ['Total FTE', 'Leavers', 'Joiners']
        },
        labels: {
            items: [{
                html: 'Employee Overview',
                style: {
                    left: '50px',
                    top: '18px,',
                    color: 'black'
                }
            }]
        },
        series : series
    }
   chart =  new Highcharts.Chart(options);
}

function redrawTable(d){
    oSettings = table.fnSettings();
    table.fnClearTable(this);
    showColumns = [selected[0], 'Total FTE', 'Joiners', 'Leavers'];

    for (var i=0; i< d.aaData.length; i++){
        table.oApi._fnAddData(oSettings, d.aaData[i]);
    }
    for (var i=0; i<oSettings.aoColumns.length; i++){
            console.log(oSettings.aoColumns[i]['mData']);
        console.log(oSettings.aoColumns[i]['mDataProp']);
        if($.inArray(oSettings.aoColumns[i]['mDataProp'], showColumns) == -1){
            var bVis = false;
            var iCol = i;
            table.fnSetColumnVis(iCol, bVis, true);
        }
        else{
            var bVis = true;
            var iCol = i;
            table.fnSetColumnVis(iCol, bVis, true);
        }
    }
    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
}

function redrawChart(d){
    sel = selected[0];
    series = [];
    for(var i=0; i< d.aaData.length; i++){
        obj = {};
        obj.type = 'column';
        obj.name = d.aaData[i][sel];
        fte = parseInt(d.aaData[i]['Total FTE']);
        joiners = parseInt(d.aaData[i]['Joiners']);
        leavers = parseInt(d.aaData[i]['Leavers']);
        obj.data = [fte,joiners, leavers]
        series.push(obj);
    }

    while(chart.series.length >0)
        chart.series[0].remove(true);
    for(var i=0; i< d.aaData.length; i++){
        chart.addSeries(series[i]);
    }
    chart.redraw();
}

function drawTable(data){
    table = $('#headcount').dataTable({
        "aaData" :data.aaData,
        "aoColumns": data.aoColumns,
        "sDom": '<"block-inline pull-left"C>lfrtip<"toolbar">',
        "oColVis": {
            "bRestore": true,
            "align": "left"
        },
        "destroy": true,
        "paging": false,
        "jQueryUI": true,
        "destroy": true,
        "scrollX": "100%",
        "bFilter": false,
        "bLengthChange": false,
        "oTableTools": {
            "sRowSelect": "single"
        },
        "language": {
            "decimal": ",",
            "thousands": ","
        },
        "columnDefs": [
            {
                "targets": [],
                "visible": false
            }
        ],
        "footerCallback": function(tFoot, aaData, iStart, iEnd, aiDisplay){

            var api = this.api();
            $($(tFoot).children()).remove();

            var aRow = aaData[0];
            var pCnt = 0;
            var properties = [];
            for(var p in aRow) {
                if (aRow.hasOwnProperty(p) ){
                    pCnt += 1;
                    properties.push(p);
                }
            }
            var totals = [];
            for (var j=0; j < pCnt; j += 1) {
                totals[j] = 0;
            }
            for(var i= 0; i<aaData.length; i++){
                // for each column
                for (var j=0; j < pCnt; j += 1) {
                    var propName = properties[j];
                    var t;
                    if (aaData[i][propName] && ( !isNaN(t = parseFloat(aaData[i][propName])))) {
                        totals[j] += t;
                    }
                }
            }
            var myTable = $('#grossToNet');
            for (var j=0; j < pCnt; j += 1) {
                var a = document.createElement('th');

                if(api.column(j).visible()){
                    $(a).html(j < 9 ? '' : numberWithCommas("" + totals[j]));
                    $(tFoot).append(a);
                }
            }
        },
        "initComplete":function(){
        },
        "bInfo" : false
    });

}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function writeSelectHeadings(t){
    var api = t.api();
    var oSettings = t.fnSettings();
    var col_id = oSettings.aoColumns;
    var selectBoxes = $('<div id="selectBoxes" class="block-inline pull-left"></div>').appendTo($('#headcount_wrapper .ColVis'));

    var select = $('<select id="selectSettings" class="multiselect" data-role="multiselect" title="Select Option" data-name="Select Region"></select>')
        .appendTo($('#selectBoxes'));

    select.append('<option value="Region">Region</option>'+
        '<option value="Department">Department</option>'+
        '<option value="Cost Center">Cost Center</option>'+
        '<option value="Company Name">Company Name</option>');

    //initMultiSelect();

    var reportRange = $('<div id="reportRange" class="btn default"><i class="fa fa-calendar"></i>&nbsp; <span> </span><b class="fa fa-angle-down"></b></div>')
        .appendTo($('#selectBoxes'));

    var searchButton = $('<div class="btn-group"><button type="button" id="refineSearch" class="multiselect btn btn-default">Submit Search</button></div>')
        .appendTo($('#selectBoxes'));

    initDatePicker();
}

function initMultiSelect(){
    selectSettings = $('#selectSettings').multiselect({
        includeSelectAllOption: true,
        buttonText: function(options, select) {
            if (options.length == 0) {
                return 'Select By <b class="caret"></b>';
            }
            else if (options.length > 3) {
                return options.length + ' selected <b class="caret"></b>';
            }
            else {
                var selected = '';
                options.each(function() {
                    selected += $(this).text() + ', ';
                });
                return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
            }
        }
    })
}

function refineSearch(p, start, end){
    $.ajax({
        "dataType": "JSON",
        "url": 'headcount.php',
        "type": 'POST',
        'data': {
            action: 'refineSearch',
            parameters: p,
            start: start,
            end: end
        },
        "success": function(data){
            redrawTable(data);
            redrawChart(data);
        },
        "error": function(e){
            console.log(e);
        }
    })
}

function initDatePicker(){
    dateRange = $('#reportRange').daterangepicker({
            opens: (App.isRTL() ? 'left' : 'right'),
            startDate: moment().subtract('days', 29),
            endDate: moment(),
            minDate: '01/01/1970',
            maxDate: '12/31/2014',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        },
        function (start, end) {
            console.log("Callback has been called!");
            startRange = start.format('YYYY/MM/DD');
            endRange = end.format('YYYY/MM/DD');
            $('#reportRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );
    $('#reportRange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
}
