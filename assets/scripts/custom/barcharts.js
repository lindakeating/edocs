function initBarChart1(){
	var data = [];
		var min = 97;
		var max = 100;
		function getRandomNumber (min, max) {
			 return Math.floor(Math.random() * (max - min + 1)) + min;
			//return (Math.random()*max+min).toFixed(2);
		}
		for (var i= 0; i < 12; i++){
			var newArray = []
			var num = getRandomNumber(min, max);
			newArray.push(i, num)
			data.push(newArray);
		}
		
		function averageArray(){
			var sum = 0;
			for (var i=0; i < data.length; i++){
				sum += parseInt(data[i][1], 10);
			}
			var avg = sum/data.length;
			return avg;
		}


/* var data = [ 
	[1, 99.9], 
	[1, 98.7], 
	[2, 98.34], 
	[3, 99.8], 
	[4, 99], 
	[5, 95.2],
	[6, 98.9],
	[7, 98.86],
	[8, 95.2],
	[9, 98.7],
	[10, 98.8],
	[11, 99.9] ]; */
	


var dataset= [ { 
		label: "YTD AVG 99.6%",
		data: data,
		animator: { start: 100, steps: 10, duration: 100, direction: "right" },
		threshold: {
			below: 98,
			color: "rgb(200, 20, 30)"
			},
		lines: {
			steps: false
		
		},
		color: '#35AA47'
	}];
						
var options = {
		series: {
			lines: {
				show: true,
				fill:true
					}
				},
			points: {
				show: true			
			},
			xaxis: {
				tickLength: 0
				},
			yaxis: {
				min: 90,
				max: 100
				}
			}
			
	$.plotAnimator("#bar-chart", dataset, options );
}

function initBarChart2(){

		var data = [];
		var min = 0;
		var max = 4;
		function getRandomNumber (min, max) {
			return (Math.random()*max+min).toFixed(2);
		}
		for (var i= 0; i < 12; i++){
			var newArray = []
			var num = getRandomNumber(min, max);
			newArray.push(i + 1, num)
			data.push(newArray);
		}
		
		function averageArray(){
			var sum = 0;
			for (var i=0; i < data.length; i++){
				sum += parseInt(data[i][1], 10);
			}
			var avg = sum/data.length;
			return avg;
		}
		
		function rainbow() {
			// 30 random hues with step of 12 degrees
			var hue = Math.floor(Math.random() * 30) * 12;

			return $.Color({
				hue: hue,
				saturation: 0.9,
				lightness: 0.6,
				alpha: 1
			}).toHexString();
		}
		
		function getRandomColor() {
			var letters = '0123456789ABCDEF'.split('');
			var color = '#';
			for (var i = 0; i < 6; i++ ) {
				color += letters[Math.floor(Math.random() * 16)];
			}
			return color;
		}
		
		var dataset= [ { 
				label: "YTD AVG " + averageArray().toFixed(2) + " %",
				data: data,
				color: "#E02222" 
			}];
							
		var options = {
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center",
						color: "#852B99",
						fill:1
							}
						},
					xaxis: {
						mode: "categories",
						tickLength: 0
						}
					}
							
		

		if(event.toElement == null){
			$.plot("#bar-chart-2", dataset, options );
		}
		else if (event.toElement.name == 'emea' || event.toElement.name == 'latam' || event.toElement.name == 'benelux' || event.toElement.name == 'apac' ){
		
			var datasetEMEA= [ { 
				label: event.toElement.name + " AVG " +  averageArray().toFixed(2) + " %",
				data: data, 
				color: rainbow()
			}];
			
		var optionsEMEA = {
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						align: "center",
						color: rainbow(),
						fill:1
							}
						},
					xaxis: {
						mode: "categories",
						tickLength: 0
						}
					};
		
		
			$.plot("#bar-chart-2", datasetEMEA, optionsEMEA);
		}
	}

function payrollCosts(){

var data = [ 
	["Jan", 5310000], 
	["Feb", 5360000], 
	["Mar", 4900700], 
	["Apr", 5200300], 
	["May", 5105000], 
	["Jun", 5020000],
	["Jul", 4810000],
	["Aug", 5360000],
	["Sept",5100700],
	["Oct", 5200300],
	["Nov", 5105000],
	["Dec", 5020000] ];
	
var data1= [ 
	["Jan", .05], 
	["Feb", .001], 
	["Mar", .01], 
	["Apr", .008], 
	["May", .02], 
	["Jun", .03],
	["Jul", .08],
	["Aug", .06],
	["Sept",.02],
	["Oct", .02],
	["Nov", .009],
	["Dec", .008] ];


var dataset= [ { 
		label: "YTD AVG €5,205,200",
		data: data, 
		color: "#35AA47" 
	}];

var dataset1= [ { 
		label: "YTD AVG .01%",
		data: data1, 
		color: "#E02222" 
	}];
					
var options = {
		series: {
			bars: {
				show: true,
				barWidth: 0.6,
				align: "center",
				color: "#852B99",
				fill:1
					}
				},
			xaxis: {
				mode: "categories",
				tickLength: 0
				},
			yaxis:{
				min: 4500000,
				max: 6000000
			}
		};
var options1 = {
		series: {
			bars: {
				show: true,
				barWidth: 0.6,
				align: "center",
				color: "#852B99",
				fill:1
					}
				},
			xaxis: {
				mode: "categories",
				tickLength: 0
				},
			};

	$.plot("#total-payroll-costs", dataset,options );
	$.plot("#bar-chart-2", dataset1,options1 );

}

function accuracySLA() {
                function randValue() {
                    return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
                }
                var ice = [
                    [1, 100],
                    [2, 99],
                    [3, 99.54],
                    [4, 98.56],
                    [5, 99],
                    [6, 99.7],
                    [7, 95],
                    [8, 98.4],
                    [9, 97.5],
                    [10,99.3],
                    [11,98.89],
                    [12, 99],
                ];
                var heat = [
                    [1, 96],
                    [2, 94],
                    [3, 99],
                    [4, 97],
                    [5, 99],
                    [6, 96],
                    [7, 99],
                    [8, 98],
                    [9, 99],
                    [10,96],
                    [11, 99],
                    [12, 97],
                ];
				var metal = [
                    [1, 98],
                    [2, 97],
                    [3, 99],
                    [4, 96],
                    [5, 99],
                    [6, 97],
                    [7, 98],
                    [8, 96],
                    [9, 98],
                    [10,97],
                    [11, 98],
                    [12, 99],
                ];
				var mineral = [
                    [1, 96],
                    [2, 98],
                    [3, 97],
                    [4, 98],
                    [5, 96],
                    [6, 98],
                    [7, 98],
                    [8, 99],
                    [9, 99],
                    [10,96],
                    [11, 99],
                    [12, 99],
                ];

                var plot = $.plot($("#chart_2"), [{
                            data: ice,
                            label: "Ice Ltd",
                            lines: {
                                lineWidth: 1,
                            },
                            shadowSize: 0

                        }, {
                            data: heat,
                            label: "Heat Ltd",
                            lines: {
                                lineWidth: 1,
                            },
                            shadowSize: 0
                        },
						{
                            data: metal,
                            label: "Metal Ltd",
                            lines: {
                                lineWidth: 1,
                            },
                            shadowSize: 0
                        },
						{
                            data: mineral,
                            label: "Mineral Ltd",
                            lines: {
                                lineWidth: 1,
                            },
                            shadowSize: 0
                        },
                    ], {
                        series: {
                            lines: {
                                show: true,
                                lineWidth: 2,
                                fill: true,
                                fillColor: {
                                    colors: [{
                                            opacity: 0.05
                                        }, {
                                            opacity: 0.01
                                        }
                                    ]
                                }
                            },
                            points: {
                                show: true,
                                radius: 3,
                                lineWidth: 1
                            },
                            shadowSize: 2
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        },
                        colors: ["#d12610", "#37b7f3", "#52e136",'#F123E1'],
                        xaxis: {
                            ticks: 11,
                            tickDecimals: 0,
                            tickColor: "#eee",
                        },
                        yaxis: {
                            ticks: 11,
                            tickDecimals: 0,
                            tickColor: "#eee",
							min: 85,
							max: 105
                        }
                    });


                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 15,
                            border: '1px solid #333',
                            padding: '4px',
                            color: '#fff',
                            'border-radius': '3px',
                            'background-color': '#333',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
				
                $("#chart_2").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
		
                            showTooltip(item.pageX, item.pageY, item.series.label + " Month " + parseInt(x) + " achieved " + parseInt(y) + " %");
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }

function accuracyClient() {
    function randValue() {
                    return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
                }
            var ice = [
                [1, 99],
                [2, 99],
                [3, 98.6],
                [4, 97.5],
                [5, 95],
                [6, 99],
                [7, 98.6],
                [8, 99.2],
                [9, 98.5],
                [10,99.1],
                [11,98.5],
                [12, 99],
            ];
            var heat = [
                [1, 98.5],
                [2, 98.8],
                [3, 99],
                [4, 99.2],
                [5, 99],
                [6, 95.2],
                [7, 98.4],
                [8, 97.5],
                [9, 98.6],
                [10,99],
                [11, 98.7],
                [12, 97.9],
            ];
			 metal = [
                [1, 99.6],
                [2, 99.2],
                [3, 99.5],
                [4, 99],
                [5, 99.3],
                [6, 99.2],
                [7, 99.0],
                [8, 99.6],
                [9, 98.5],
                [10,99.4],
                [11, 98.9],
                [12, 99],
            ];
			var mineral = [
                   [1, 98.4],
                   [2, 98.9],
                   [3, 98.5],
                   [4, 98.7],
                   [5, 99],
                   [6, 98.9],
                   [7, 99.2],
                   [8, 99.1],
                   [9, 99.2],
                   [10,96],
                   [11, 99.1],
                   [12, 99.3],
               ];

        var plot = $.plot($("#localClientChart"), 
			[{
                data: ice,
                label: "Ice Ltd",
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0

                }, {
                data: heat,
                label: "Heat Ltd",
                lines: {
                    lineWidth: 1,
					},
                shadowSize: 0
                    }, {
				data: metal,
                label: "Metal Ltd",
                lines: {
                    lineWidth: 1,
                        },
                shadowSize: 0
                    },{
                            data: mineral,
                            label: "Mineral Ltd",
                            lines: {
                                lineWidth: 1,
                            },
                            shadowSize: 0
                        },
             ], {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }
                            ]
                        }
                    },
                    points: {
                        show: true,
                        radius: 3,
                        lineWidth: 1
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1
                },
                colors: ["#d12610", "#37b7f3", "#52e136",'#F123E1'],
                
				xaxis: {
                    ticks: [[1,'Jan'],
							[2,'Feb'],
							[3,'Mar'],
							[4,'Apr'],
							[5,'May'],
							[6,'Jun'],
							[7,'Jul'],
							[8,'Aug'],
							[9,'Sep'],
							[10,'Oct'],
							[11,'Nov'],
							[12,'Dec']],
                    tickDecimals: 0,
                    tickColor: "#eee",
                        },
                yaxis: {
                    ticks: 11,
                    tickDecimals: 0,
                    tickColor: "#eee",
					min: 90,
					max: 100
                        },
				legend: {
					position: "se",
						}
                    });

                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                            position: 'absolute',
                            display: 'none',
                            top: y + 5,
                            left: x + 15,
                            border: '1px solid #333',
                            padding: '4px',
                            color: '#fff',
                            'border-radius': '3px',
                            'background-color': '#333',
                            opacity: 0.80
                        }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
				
                $("#localClientChart").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
		
                            showTooltip(item.pageX, item.pageY, item.series.label + " Month " + parseInt(x) + " achieved " + parseInt(y) + " %");
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }
