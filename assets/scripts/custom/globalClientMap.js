var MapsVector = function () {

    var setMap = function (name) {
        sample_data = {
            ae: "239.65",
            af: "16.63",
            ag: "1.1",
            al: "11.58",
            am: "8.83",
            ao: "85.81",
            ar: "351.02",
            at: "366.26",
            au: "1219.72",
            az: "52.17",
            ba: "16.2",
            bb: "3.96",
            bd: "105.4",
            be: "461.33",
            bf: "8.67",
            bg: "44.84",
            bh: "21.73",
            bi: "1.47",
            bj: "6.49",
            bn: "11.96",
            bo: "19.18",
            br: "2023.53",
            bs: "7.54",
            bt: "1.4",
            bw: "12.5",
            by: "52.89",
            bz: "1.43",
            ca: "1563.66",
            cd: "12.6",
            cf: "2.11",
            cg: "11.88",
            ch: "522.44",
            ci: "22.38",
            cl: "199.18"  ,
            cm: "21.88",
            cn: "5745.13",
            co: "283.11",
            cr: "35.02",
            cv: "1.57",
            cy: "22.75",
            cz: "195.23",
            de: "3305.9",
            dj: "1.14",
            dk: "304.56",
            dm: "0.38",
            do: "50.87",
            dz: "158.97",
            ec: "61.49",
            ee: "19.22",
            eg: "216.83",
            er: "2.25",
            es: "1374.78",
            et: "30.94",
            fi: "231.98",
            fj: "3.15",
            fr: "2555.44",
            ga: "12.56",
            gb: "2258.57",
            gd: "0.65",
            ge: "11.23",
            gh: "18.06",
            gm: "1.04",
            gn: "4.34",
            gq: "14.55",
            gr: "305.01",
            gt: "40.77",
            gw: "0.83",
            gy: "2.2",
            hk: "226.49",
            hn: "15.34",
            hr: "59.92",
            ht: "6.5",
            hu: "132.28",
            id: "695.06",
            ie: "204.14",
            il: "201.25",
            in: "1430.02",
            iq: "84.14",
            ir: "337.9",
            is: "12.77",
            it: "2036.69",
            jm: "13.74",
            jo: "27.13",
            jp: "5390.9",
            ke: "32.42",
            kg: "4.44",
            kh: "11.36",
            ki: "0.15",
            km: "0.56",
            kn: "0.56",
            kr: "986.26",
            kw: "117.32",
            kz: "129.76",
            la: "6.34",
            lb: "39.15",
            lc: "1",
            lk: "48.24",
            lr: "0.98",
            ls: "1.8",
            lt: "35.73",
            lu: "52.43",
            lv: "23.39",
            ly: "77.91",
            ma: "91.7",
            md: "5.36",
            me: "3.88",
            mg: "8.33",
            mk: "9.58",
            ml: "9.08",
            mm: "35.65",
            mn: "5.81",
            mr: "3.49",
            mt: "7.8",
            mu: "9.43",
            mv: "1.43",
            mw: "5.04",
            mx: "1004.04",
            my: "218.95",
            mz: "10.21",
            na: "11.45",
            ne: "5.6",
            ng: "206.66",
            ni: "6.38",
            nl: "770.31",
            no: "413.51",
            np: "15.11",
            nz: "138",
            om: "53.78",
            pa: "27.2",
            pe: "153.55",
            pg: "8.81",
            ph: "189.06",
            pk: "174.79",
            pl: "438.88",
            pt: "223.7",
            py: "17.17",
            qa: "126.52",
            ro: "158.39",
            rs: "38.92",
            ru: "1476.91",
            rw: "5.69",
            sa: "434.44",
            sb: "0.67",
            sc: "0.92",
            sd: "65.93",
            se: "444.59",
            sg: "217.38",
            si: "46.44",
            sk: "86.26",
            sl: "1.9",
            sn: "12.66",
            sr: "3.3",
            st: "0.19",
            sv: "21.8",
            sy: "59.63",
            sz: "3.17",
            td: "7.59",
            tg: "3.07",
            th: "312.61",
            tj: "5.58",
            tl: "0.62",
            tm: 0,
            tn: "43.86",
            to: "0.3",
            tr: "729.05",
            tt: "21.2",
            tw: "426.98",
            tz: "22.43",
            ua: "136.56",
            ug: "17.12",
            undefined: "5.73",
            us: "14624.18",
            uy: "40.71",
            uz: "37.72",
            vc: "0.58",
            ve: "285.21",
            vn: "101.99",
            vu: "0.72",
            ws: "0.55",
            ye: "30.02",
            za: "354.41",
            zm: "15.69",
            zw: "5.57"
        }
        var data = {
            map: 'world_en',
            backgroundColor: null,
            borderColor: '#333333',
            borderOpacity: 0.5,
            borderWidth: 1,
            color: '#c6c6c6',
            enableZoom: true,
            hoverColor: '#c9dfaf',
            hoverOpacity: null,
            values: sample_data,
            normalizeFunction: 'linear',
            scaleColors: ['#C8EEFF', '#006491'],
            selectedColor: '#c9dfaf',
            selectedRegion: true,
            showTooltip: true,
            onLabelShow: function(event, label, code){
                switch(code){
                    case "us":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2120 Average Employees <br> Total Payroll Costs <br> €1,521,500';
                        break;
                    case"ae":
                        label[0].innerHTML = 'no data';
                        break;
                    case"af":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ag":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"al":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"am":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ao":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ar":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2120 Average Employees <br> Total Payroll Costs <br> €1,521,500';
                        break;
                    case"at":
                        label[0].innerHTML = label[0].innerHTML + '<br> 923 Average Employees <br> Total Payroll Costs <br> €1,357,00';

                        break;
                    case"au":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"az":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ba":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"bb":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"bd":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"be":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1670 Average Employees <br> Total Payroll Costs <br> €1,473,254';

                        break;
                    case"bf":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bg":
                        label[0].innerHTML = label[0].innerHTML + '<br> 642 Average Employees <br> Total Payroll Costs <br> €914,247';

                        break;
                    case"bi":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bj":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bn":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bo":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1120 Average Employees <br> Total Payroll Costs <br> €1,121,500';

                        break;
                    case"br":
                        label[0].innerHTML = label[0].innerHTML + '<br> 3120 Average Employees <br> Total Payroll Costs <br> €3,621,500';

                        break;
                    case"bs":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bt":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bw":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"by":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bz":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ca":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cd":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cf":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cg":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ch":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2320 Average Employees <br> Total Payroll Costs <br> €2,641,500';

                        break;
                    case"cl":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cm":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cn":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"co":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2569 Average Employees <br> Total Payroll Costs <br> €2,851,400';

                        break;
                    case"cr":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1520 Average Employees <br> Total Payroll Costs <br> €1,721,000';

                        break;
                    case"cu":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cv":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cy":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cz":
                        label[0].innerHTML = label[0].innerHTML + '<br> 984 Average Employees <br> Total Payroll Costs <br> €1,241,500';

                        break;
                    case"de":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2347 Average Employees <br> Total Payroll Costs <br> €4,245,500';

                        break;
                    case"dj":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"dk":
                        label[0].innerHTML = label[0].innerHTML + '<br> 574 Average Employees <br> Total Payroll Costs <br> €628,000';

                        break;
                    case"dm":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1260 Average Employees <br> Total Payroll Costs <br> €1,751,000';

                        break;
                    case"do":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"dz":

                        break;
                    case"ec":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1680 Average Employees <br> Total Payroll Costs <br> €2,521,000';

                        break;
                    case"ee":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"eg":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"er":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"es":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"fi":

                        break;
                    case"fj":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1340 Average Employees <br> Total Payroll Costs <br> €2,678,000';

                        break;
                    case"fk":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"fr":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1437 Average Employees <br> Total Payroll Costs <br> €2,354,000';

                        break;
                    case"ga":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gb":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gd":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ge":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gf":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gh":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gl":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gm":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gn":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gq":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gr":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gt":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1599 Average Employees <br> Total Payroll Costs <br> €1,521,900';

                        break;
                    case"gw":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gy":

                        break;
                    case"hn":
                        label[0].innerHTML = label[0].innerHTML + '<br> 4527 Average Employees <br> Total Payroll Costs <br> €1,254,700';

                        break;
                    case"hr":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1427 Average Employees <br> Total Payroll Costs <br> €2,547,300';

                        break;
                    case"ht":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"hu":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2314 Average Employees <br> Total Payroll Costs <br> €2,345,500';

                        break;
                    case"id":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ie":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1120 Average Employees <br> Total Payroll Costs <br> €1,521,500';
                        break;
                    case"il":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1230 Average Employees <br> Total Payroll Costs <br> €2,331,500';

                        break;
                    case"in":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"iq":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"it":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2346 Average Employees <br> Total Payroll Costs <br> €2,687,500';

                        break;
                    case"is":

                        break;
                    case"JM":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1472 Average Employees <br> Total Payroll Costs <br> €1,326,000';

                        break;

                    case "JO":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "JP":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KH":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KP":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KZ":
                        label[0].innerHTML = label[0].innerHTML + '<br> 850 Average Employees <br> Total Payroll Costs <br> €1,850,500';

                        break;
                    case "LA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LB":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LC":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LS":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LT":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LV":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LY":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MD":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ML":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MT":
                        label[0].innerHTML = label[0].innerHTML + '<br> 640 Average Employees <br> Total Payroll Costs <br> €291,500';

                        break;
                    case "MU":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1954 Average Employees <br> Total Payroll Costs <br> €951,500';
                        break;
                    case "MV":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MX":
                        label[0].innerHTML = label[0].innerHTML + '<br> 3780 Average Employees <br> Total Payroll Costs <br> €4,211,000';

                        break;
                    case "MY":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NC":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NI":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1400 Average Employees <br> Total Payroll Costs <br> €1,774,500';

                        break;
                    case "NL":
                        label[0].innerHTML = label[0].innerHTML + '<br> 620 Average Employees <br> Total Payroll Costs <br> €984,500';

                        break;
                    case "NO":
                        label[0].innerHTML = label[0].innerHTML + '<br> 3120 Average Employees <br> Total Payroll Costs <br> €3,621,500';

                        break;
                    case "NP":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "OM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PA":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1830 Average Employees <br> Total Payroll Costs <br> €2,521,400';

                        break;
                    case "PE":
                        label[0].innerHTML = label[0].innerHTML + '<br> 842 Average Employees <br> Total Payroll Costs <br> €927,500';

                        break;
                    case "PF":

                        label[0].innerHTML = 'no data available'
                        break;
                    case "PG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PH":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PL":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2540 Average Employees <br> Total Payroll Costs <br> €2,456,500';

                        break;
                    case "PT":

                        break;
                    case "PY":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1573 Average Employees <br> Total Payroll Costs <br> €2,496,300';

                        break;
                    case "QA":
                        label[0].innerHTML = label[0].innerHTML + '<br> 3512 Average Employees <br> Total Payroll Costs <br> €5,241,000';

                        break;
                    case "RE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "RO":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2349 Average Employees <br> Total Payroll Costs <br> €1,954,500';

                        break;
                    case "RS":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "RU":
                        label[0].innerHTML = label[0].innerHTML + '<br> 4,689 Average Employees <br> Total Payroll Costs <br> €6,943,500';

                        break;
                    case "RW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SB":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SC":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SD":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SI":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SL":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SO":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ST":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SV":
                        label[0].innerHTML = label[0].innerHTML + '<br> 820 Average Employees <br> Total Payroll Costs <br> €721,500';

                        break;
                    case "SY":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TD":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TH":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TJ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TL":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TT":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "UA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "UG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "US":
                        label[0].innerHTML = label[0].innerHTML + '<br> 4120 Average Employees <br> Total Payroll Costs <br> €6,587,300';
                        break;
                    case "UY":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2354 Average Employees <br> Total Payroll Costs <br> €928,500';

                        break;
                    case "UZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "VE":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1080 Average Employees <br> Total Payroll Costs <br> €2,829,500';

                        break;
                    case "VN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "VU":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "YE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ZA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ZM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ZW":
                        label[0].innerHTML = 'no data available'
                        break;

                }
            },
            onRegionOver: function (event, code, region) {
                //sample to interact with map
                if (code == 'ca') {
                    event.preventDefault();
                }
            },
            onRegionClick: function (element, code, region) {
                //sample to interact with map
                var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
                alert(message);
            }
        };
        data.map = name + '_en';
        var map = jQuery('#vmap_' + name);
        if (!map) {
            return;
        }
        map.width(map.parent().width());
        map.vectorMap(data);
    }


    return {
        //main function to initiate map samples
        init: function () {
            setMap("world");
            setMap("usa");
            setMap("europe");
            setMap("russia");
            setMap("germany");

            // redraw maps on window or content resized
            App.addResponsiveHandler(function(){
                setMap("world");
                setMap("usa");
                setMap("europe");
                setMap("russia");
                setMap("germany");
            });
        }

    };

}();