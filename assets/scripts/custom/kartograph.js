function kartograph_map(){
	// initialize tooltips
	var map = kartograph.map('#chart_1_1');

	map.loadMap('world.svg', function() {
		map.addLayer('countries', {
        styles: {
            fill: '#ee9900'
        },
        title: function(d) {
            return d.countryName;
        }
    });
});
}
