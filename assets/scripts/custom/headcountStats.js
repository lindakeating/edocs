$('body').on('click', '#tab2', function(){
    $.ajax({
        "dataType": 'JSON',
        "url": '../headcount/headcount.php',
        "type": 'POST',
        'data': {action: 'initStats'},
        "success": function(data){
            console.log(this);
            drawStatsChart(data);
        },
        "error": function(e){
            console.log(e);
        }
    });
});

$('body').on('click', '#tab3', function(){
    $.ajax({
        "dataType": 'JSON',
        "url": 'jsonFiles/regionLongevity.json',
        "type": 'POST',
        "success": function(data){
            drawLongevityChart(data);
        },
        "error": function(e){
            console.log(e);
        }
    })
});


$('body').on('change', '#selectPosition', function(){
    region = $(this).val();
    $.ajax({
        "dataType": 'JSON',
        "url": '../headcount/headcount.php',
        "type": 'POST',
        'data': {action: 'initStats',
                region: region
        },
        "success": function(data){
            drawStatsChart(data);
        },
        "error": function(e){
            console.log(e);
        }
    });
})

$('body').on('change', '#selectLongevity', function(){
    url = $(this).val();
    console.log(url);
    $.ajax({
        "dataType": 'JSON',
        "url": 'jsonFiles/'+url+'.json',
        "type": 'POST',
        "success": function(data){
            drawLongevityChart(data);
        },
        "error": function(e){
            console.log(e);
        }
    });
})

function drawLongevityChart(d){
    categories = ['Less Than a Year', '1 - 3 Years', '3- 5 Years', '5 + Years'];
    series = [];
    for (var key in d){
        if(d.hasOwnProperty(key)){
           // categories.push(key);
            series.push(d[key])
        }
    }
    $container = $('#longevityChart');
    chartOptions = {};
    chartOptions.colors = ['Teal', 'Red','LightSteelBlue', 'Orange' ];
    chartOptions.series = series;
    chartOptions.title = "Longevity Chart";
    chartOptions.chart = {
            renderTo: $container[0],
            height: 300,
            width: $('#tab_1_1_3').width()
    };
    chartOptions.xAxis = {
        categories : categories
    };
    chartOptions.yAxis = {
            min: 0,
            title: {
            text: 'Longevity'
        }
    };
    chartOptions.labels = {
        items: [{
            html: 'Longevity',
            style: {
                left: '50px',
                top: '18px,',
                color: 'black'
            }
        }]
    };
    chartOptions.series = series;
    chart =  new Highcharts.Chart(chartOptions);

}

function drawStatsChart(d){
    categories = [];
    series = [];
    for(var i=0; i< d.aaData.length; i++){
        categories.push(d.aaData[i]['Position']);
    }
    obj = {};
    obj.type = 'column';
    obj.name = 'position';
    data = [];
    colors = ['Teal', 'Red','LightSteelBlue', 'Orange' ]
    dataColorindex = 0;
    for(var i=0; i< d.aaData.length; i++){
        dataObj = {};
        if(dataColorindex < 4){
            dataObj.color = colors[dataColorindex];
            dataColorindex++;
        }
        else{
            dataColorindex = 0;
            dataObj.color = colors[dataColorindex];
        }
        dataObj.y = parseInt(d.aaData[i]['p'])

        //data.push(parseInt(d.aaData[i]['p']));
        obj.data = data;
        data.push(dataObj);
    }
    obj.data = data;
    series.push(obj);

    var $container = $('#positionChart');

    var options = {
        chart: {
            renderTo: $container[0],
            height: 300,
            width: $('#tab_1_1_2').width()
        },
        colors: ['Teal', 'Red','LightSteelBlue', 'Orange' ],
        title: {
            text: 'Position by Region'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Position'
            }
        },
        labels: {
            items: [{
                html: 'Position by Region',
                style: {
                    left: '50px',
                    top: '18px,',
                    color: 'black'
                }
            }]
        },
        series : series,
        plotOptions: {
            column: {
                colorsByPoint: true,
                colors: ['Teal', 'Red','LightSteelBlue', 'Orange' ]
            }
        }
    }
    chart =  new Highcharts.Chart(options);
}