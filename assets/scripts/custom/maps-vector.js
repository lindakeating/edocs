var MapsVector = function () {

    var setMap = function (name) {

        var data = {
            map: 'world_en',
            backgroundColor: null,
            borderColor: '#333333',
            borderOpacity: 0.5,
            borderWidth: 1,
            color: '#c6c6c6',
            enableZoom: true,
            hoverColor: '#c9dfaf',
            hoverOpacity: null,
            values: sample_data,
            normalizeFunction: 'linear',
            scaleColors: ['#C8EEFF', '#006491'],
            selectedColor: '#c9dfaf',
            selectedRegion: true,
            showTooltip: true,
            onLabelShow: function(event, label, code){
                switch(code){
                    case "us":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2120 Average Employees <br> Total Payroll Costs <br> €1,521,500';
                        break;
                    case"ae":
                        label[0].innerHTML = 'no data';
                        break;
                    case"af":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ag":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"al":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"am":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ao":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ar":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2120 Average Employees <br> Total Payroll Costs <br> €1,521,500';
                        break;
                    case"at":
                        label[0].innerHTML = label[0].innerHTML + '<br> 923 Average Employees <br> Total Payroll Costs <br> €1,357,00';

                        break;
                    case"au":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"az":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"ba":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"bb":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"bd":
                        label[0].innerHTML = 'no data available';
                        break;
                    case"be":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1670 Average Employees <br> Total Payroll Costs <br> €1,473,254';

                        break;
                    case"bf":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bg":
                        label[0].innerHTML = label[0].innerHTML + '<br> 642 Average Employees <br> Total Payroll Costs <br> €914,247';

                        break;
                    case"bi":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bj":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bn":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bo":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1120 Average Employees <br> Total Payroll Costs <br> €1,121,500';

                        break;
                    case"br":
                        label[0].innerHTML = label[0].innerHTML + '<br> Accuracy SLA 98% <br> Completion SLA 99%<br> ';

                        break;
                    case"bs":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bt":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bw":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"by":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"bz":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ca":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cd":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cf":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cg":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ch":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2320 Average Employees <br> Total Payroll Costs <br> €2,641,500';

                        break;
                    case"cl":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cm":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cn":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"co":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2569 Average Employees <br> Total Payroll Costs <br> €2,851,400';

                        break;
                    case"cr":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1520 Average Employees <br> Total Payroll Costs <br> €1,721,000';

                        break;
                    case"cu":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cv":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cy":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"cz":
                        label[0].innerHTML = label[0].innerHTML + '<br> 984 Average Employees <br> Total Payroll Costs <br> €1,241,500';

                        break;
                    case"de":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2347 Average Employees <br> Total Payroll Costs <br> €4,245,500';

                        break;
                    case"dj":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"dk":
                        label[0].innerHTML = label[0].innerHTML + '<br> 574 Average Employees <br> Total Payroll Costs <br> €628,000';

                        break;
                    case"dm":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1260 Average Employees <br> Total Payroll Costs <br> €1,751,000';

                        break;
                    case"do":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"dz":

                        break;
                    case"ec":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1680 Average Employees <br> Total Payroll Costs <br> €2,521,000';

                        break;
                    case"ee":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"eg":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"er":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"es":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"fi":

                        break;
                    case"fj":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1340 Average Employees <br> Total Payroll Costs <br> €2,678,000';

                        break;
                    case"fk":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"fr":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1437 Average Employees <br> Total Payroll Costs <br> €2,354,000';

                        break;
                    case"ga":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gb":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gd":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ge":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gf":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gh":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gl":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gm":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gn":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gq":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gr":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gt":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1599 Average Employees <br> Total Payroll Costs <br> €1,521,900';

                        break;
                    case"gw":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"gy":

                        break;
                    case"hn":
                        label[0].innerHTML = label[0].innerHTML + '<br> 4527 Average Employees <br> Total Payroll Costs <br> €1,254,700';

                        break;
                    case"hr":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1427 Average Employees <br> Total Payroll Costs <br> €2,547,300';

                        break;
                    case"ht":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"hu":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2314 Average Employees <br> Total Payroll Costs <br> €2,345,500';

                        break;
                    case"id":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"ie":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1120 Average Employees <br> Total Payroll Costs <br> €1,521,500';
                        break;
                    case"il":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1230 Average Employees <br> Total Payroll Costs <br> €2,331,500';

                        break;
                    case"in":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"iq":
                        label[0].innerHTML = 'no data available'
                        break;
                    case"it":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2346 Average Employees <br> Total Payroll Costs <br> €2,687,500';

                        break;
                    case"is":

                        break;
                    case"JM":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1472 Average Employees <br> Total Payroll Costs <br> €1,326,000';

                        break;

                    case "JO":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "JP":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KH":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KP":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "KZ":
                        label[0].innerHTML = label[0].innerHTML + '<br> 850 Average Employees <br> Total Payroll Costs <br> €1,850,500';

                        break;
                    case "LA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LB":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LC":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LS":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LT":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LV":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "LY":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MD":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ML":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MT":
                        label[0].innerHTML = label[0].innerHTML + '<br> 640 Average Employees <br> Total Payroll Costs <br> €291,500';

                        break;
                    case "MU":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1954 Average Employees <br> Total Payroll Costs <br> €951,500';
                        break;
                    case "MV":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MX":
                        label[0].innerHTML = label[0].innerHTML + '<br> 3780 Average Employees <br> Total Payroll Costs <br> €4,211,000';

                        break;
                    case "MY":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "MZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NC":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NI":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1400 Average Employees <br> Total Payroll Costs <br> €1,774,500';

                        break;
                    case "NL":
                        label[0].innerHTML = label[0].innerHTML + '<br> 620 Average Employees <br> Total Payroll Costs <br> €984,500';

                        break;
                    case "NO":
                        label[0].innerHTML = label[0].innerHTML + '<br> 3120 Average Employees <br> Total Payroll Costs <br> €3,621,500';

                        break;
                    case "NP":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "NZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "OM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PA":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1830 Average Employees <br> Total Payroll Costs <br> €2,521,400';

                        break;
                    case "PE":
                        label[0].innerHTML = label[0].innerHTML + '<br> 842 Average Employees <br> Total Payroll Costs <br> €927,500';

                        break;
                    case "PF":

                        label[0].innerHTML = 'no data available'
                        break;
                    case "PG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PH":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "PL":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2540 Average Employees <br> Total Payroll Costs <br> €2,456,500';

                        break;
                    case "PT":

                        break;
                    case "PY":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1573 Average Employees <br> Total Payroll Costs <br> €2,496,300';

                        break;
                    case "QA":
                        label[0].innerHTML = label[0].innerHTML + '<br> 3512 Average Employees <br> Total Payroll Costs <br> €5,241,000';

                        break;
                    case "RE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "RO":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2349 Average Employees <br> Total Payroll Costs <br> €1,954,500';

                        break;
                    case "RS":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "RU":
                        label[0].innerHTML = label[0].innerHTML + '<br> 4,689 Average Employees <br> Total Payroll Costs <br> €6,943,500';

                        break;
                    case "RW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SB":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SC":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SD":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SI":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SK":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SL":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SO":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ST":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SV":
                        label[0].innerHTML = label[0].innerHTML + '<br> 820 Average Employees <br> Total Payroll Costs <br> €721,500';

                        break;
                    case "SY":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "SZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TD":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TH":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TJ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TL":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TR":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TT":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TW":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "TZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "UA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "UG":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "US":
                        label[0].innerHTML = label[0].innerHTML + '<br> 4120 Average Employees <br> Total Payroll Costs <br> €6,587,300';
                        break;
                    case "UY":
                        label[0].innerHTML = label[0].innerHTML + '<br> 2354 Average Employees <br> Total Payroll Costs <br> €928,500';

                        break;
                    case "UZ":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "VE":
                        label[0].innerHTML = label[0].innerHTML + '<br> 1080 Average Employees <br> Total Payroll Costs <br> €2,829,500';

                        break;
                    case "VN":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "VU":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "YE":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ZA":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ZM":
                        label[0].innerHTML = 'no data available'
                        break;
                    case "ZW":
                        label[0].innerHTML = 'no data available'
                        break;

                }
            },
            onRegionOver: function (event, code, region) {
                //sample to interact with map
                if (code == 'ca') {
                    event.preventDefault();
                }
            },
            onRegionClick: function (element, code, region) {
                //sample to interact with map
                var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
                alert(message);
            }
        };
        data.map = name + '_en';
        var map = jQuery('#vmap_' + name);
        if (!map) {
            return;
        }
        map.width(map.parent().width());
        map.vectorMap(data);
    }


    return {
        //main function to initiate map samples
        init: function () {
            setMap("world");
            setMap("usa");
            setMap("europe");
            setMap("russia");
            setMap("germany");

            // redraw maps on window or content resized
            App.addResponsiveHandler(function(){
                setMap("world");
                setMap("usa");
                setMap("europe");
                setMap("russia");
                setMap("germany");
            });
        }

    };

}();