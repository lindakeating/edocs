function pieChartNormal(){jQuery('.chart').easyPieChart({
       animate:   2000,
       barColor:  '#28B779',
       lineWidth: 7,
       lineCap:	'butt',
	   size: 90
   });
  }

 function pieChartRed(){
   jQuery('#easyRedPieChart').easyPieChart({
       animate:   2000,
       barColor:  '#E02222',
       lineWidth: 7,
       lineCap:	'butt',
		size: 90
   });
}