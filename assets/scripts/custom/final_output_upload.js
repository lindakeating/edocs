	FileTypes = {
		grosstonet: false,
		journal : false,
		payslips : false,
		globalreporting: false,
		count: 0
	};

	
	$('body').on('change', '#payroll_type', function(){
		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "grossto net"){
			FileTypes.grosstonet = true;
			FileTypes.count = FileTypes.count + 1;
		}
		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "journal"){
			FileTypes.journal = true;	
			FileTypes.count = FileTypes.count + 1;
		}
		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "payslips"){
			FileTypes.payslips = true;
			FileTypes.count = FileTypes.count + 1;
		}
		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "globalreporting"){
			FileTypes.globalreporting = true;
			FileTypes.count = FileTypes.count + 1;
		}
	}); 
	
 	function checkFiles(){
		if(FileTypes.count < 4){
			$('#verifyFiles .modal-title').html('<h3>You have not uploaded all standard file types</h3>');
			$('#verifyFiles .modal-body').html('<p>The following file types are missing: </p><ol>');
			if(FileTypes.grosstonet == false){
				$('#verifyFiles .modal-body').append('<li>Gross To Net</li>');			
			};
			if(FileTypes.journal == false){
				$('#verifyFiles .modal-body').append('<li>Journal</li>');			
			}
			if(FileTypes.payslips == false){
				$('#verifyFiles .modal-body').append('<li>Payslips</li>');			
			}
			if(FileTypes.globalreporting == false){
				$('#verifyFiles .modal-body').append('<li>Global Reporting</li>');			
			}

			$('#verifyFiles .modal-body').append('</ol>');
			$('#verifyFiles .modal-footer').html('<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Go back</button>');
			
		}
		else{
				var employees = document.getElementById('employees');				
				var request = 	$.ajax({
									type: "POST",
									url: "updatePayslips.php",
									data: { employees: employees.value }	
								});
			$('#verifyFiles .modal-title').html('You have successfully uploaded all file types');
			$('#verifyFiles .modal-body').html('Verify Payslip Count');
			$('#verifyFiles .modal-body').append('<div class=\"table-responsive\">'+
												'<table class=\"table table-hover\">'+
													'<thead>'+
														'<tr>'+
															'<td>Check</td>'+
															'<td>Payroll</td>'+
															'<td>Inputed</td>'+
															'<td>Result</td>'+
														'</tr>'+
													'</thead>'+
													'<tbody>'+
													'<tr class="green">'+
													'<td>Total Number of Employees Processed in Period</td>'+
											 		'<td id=\"noPayroll\">'+employees.value+'</td>'+
											 		'<td id=\"noInputted\">'+employees.value+'</td>'+
											 		'<td><span class=\"label label-sm label-success\"><i class=\"fa fa-check \"></i></span></td>'+
											 	'</tr>'+
											 	'</tbody>'+
											 '</table>'+
											 '</div>')
			$('#verifyFiles .modal-footer').html('<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Go back</button>');
			$('#verifyFiles .modal-footer').append('<button type=\"button\" class=\"btn btn-success\" ><a href=\"../setstatus.php?sg=4&st=5&so=2&nsg=5&nst=3&nso=1\" >YES</a></button>');
		} 

		$('#verifyFiles').modal();	
	}