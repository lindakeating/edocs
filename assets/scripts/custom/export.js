
    dateRange = $('#exportRange').daterangepicker({
            opens: (App.isRTL() ? 'left' : 'right'),
            startDate: moment().subtract('days', 29),
            endDate: moment(),
            minDate: '01/01/1970',
            maxDate: '12/31/2014',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                // 'Today': [moment(), moment()],
                //  'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last Period': [moment().startOf('month').subtract('months', 1), moment()],
                'Last Quarter': [moment().startOf('month').subtract('months', 3), moment()],
                'Last 6 Months': [moment().startOf('month'), moment().endOf('month')],
                'Last Year': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        },
        function (start, end) {

            startRange = start.format('YYYYMMDD').slice(0, -2).toString();
            endR= parseInt(end.format('YYYYMMDD').slice(0,-2))-1;
            endRange = endR.toString();
            // console.log(startRange);
            // console.log(endRange);
            $('#exportRange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );
    $('#exportRange span').html(moment().subtract('days', 29).format('YYYY MM') + ' - ' + moment().format('YYYY MM'));

$('#downloadFiles').on('click', function(e){
    e.preventDefault();
    if($('#fileFormat option:selected').text() == "XLS"){
        window.location.href = "dataout/ie_ice_ltd.xlsx";
    }
    else if($('#fileForm option:selected')== "Text"){
        window.location.href= "dataout/ie_ice_ltd.txt";
    }
    else if($('#fileForm option:selected')== "XML"){
        window.location.href = "dataout/ie_ice_ltd.xml";
    }
});
