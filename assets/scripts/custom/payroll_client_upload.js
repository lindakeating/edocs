	ClientFileTypes = {
		PayrollVariations: false,
		Starters : false,
		Leavers : false,
		MasterDataChanges: false,
		count: 0
	};

		$('body').on('click', '#start_upload', function(){
		
		console.log(this);
		alert(this);
		
		});
	
	$('body').on('change', '#payroll_type', function(){

		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "PayrollVariations"){
			ClientFileTypes.PayrollVariations = true;
			ClientFileTypes.count = ClientFileTypes.count + 1;
		}
		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "Starters"){
			ClientFileTypes.Starters = true;	
			ClientFileTypes.count = ClientFileTypes.count + 1;
		}
		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "Leavers"){
			ClientFileTypes.Leavers = true;
			ClientFileTypes.count = ClientFileTypes.count + 1;
		}
		if( this.options[this.selectedIndex].value.replace(/\s+/, "") == "MasterData Changes"){
			ClientFileTypes.MasterDataChanges = true;
			ClientFileTypes.count = ClientFileTypes.count + 1;
		}
	});
		
	$('#submitFiles').on('click',function(){
		if(ClientFileTypes.count < 4){
			$('#myModal .modal-title').html('You have not uploaded all  file types');
			$('#myModal .modal-body').html('The following file types are missing: <ol>');
			if(ClientFileTypes.PayrollVariations == false){
				$('#myModal .modal-body').append('<li>Payroll Variations</li>');			
			};
			if(ClientFileTypes.Starters == false){
				$('#myModal .modal-body').append('<li>Starters</li>');			
			}
			if(ClientFileTypes.Leavers == false){
				$('#myModal .modal-body').append('<li>Leavers</li>');			
			}
			if(ClientFileTypes.MasterDataChanges == false){
				$('#myModal .modal-body').append('<li>Master Data Changes</li>');			
			}
			$('#myModal .modal-body').append('</ol>');
			
				$('#myModal .modal-body').append('<div class="form-body"><div class="form-group"><label class=\"col-md-3 control-label\" for=\"comments\">Comments </label>'+
										'<div class="col-md-9"><textarea name=\"comments\" class=\"form-control col-md-9\" placeholder=\"comments\"></textarea></div></div></div>');
										
			$('#myModal .modal-footer').html('<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Go back</button>');
			$('#myModal .modal-footer').append('<button type=\"button\" class=\"btn btn-success\" ><a href=\"../setstatus.php?sg=1&st=3&so=2\">That\'s ok! Continue</a></button>');
			
		}
		
		else{
			$('#myModal .modal-title').html('You have successfully uploaded all file types');
			$('#myModal .modal-body').html('Do you wish to confirm submission?');
			$('#myModal .modal-body').append('<div class="form-body"><div class="form-group"><label class=\"col-md-3 control-label\" for=\"comments\">Comments </label>'+
										'<div class="col-md-9"><textarea name=\"comments\" class=\"form-control col-md-9\" placeholder=\"comments\"></textarea></div></div></div>');			
			$('#myModal .modal-footer').html('<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Go back</button>');
			$('#myModal .modal-footer').append('<button type=\"button\" class=\"btn btn-success\" ><a href=\"../setstatus.php?sg=1&st=3&so=2\">Confirm</a></button>');
		}

		$('#myModal').modal();	
	});