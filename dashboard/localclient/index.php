<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$localClient = true;
$localTmf = null;
$gClientFlag = null;
$vmapFlag = null;
$CalendarFlag = true;
$UploadFlag = null;
$SortTableFlag = null;
$WizardFlag = null;
$EditableTableFlag = null;

?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				TMF | Local
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row ">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							 3 / 0
						</div>
						<div class="desc">
							 Today's Tasks<br> Open / Overdue
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							5 / 0
						</div>
						<div class="desc">
							Total Tickets<br>Open / Overdue
								
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat purple">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							98.7% / 98.9% 
						</div>
						<div class="desc">
							Completion SLA <br>  Previous Mth / YTD 
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat yellow">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							99.9% / 98.8% 
						</div>
						<div class="desc">
							Accuracy SLA <br> Previous Mth /YTD 
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="clearfix">
		<div class="row">
			<div class="col-md-6">
				<div id="calendar" class="has-toolbar">
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet paddingless">
					<div class="portlet-body">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab_1_1" data-toggle="tab">
									Overdue Tasks </a>
								</li>
								<li>
									<a href="#tab_1_2" data-toggle="tab">
									My Payroll Tasks</a>
								</li>
								<li>
									<a href="#tab_1_3" data-toggle="tab">
									Recent My Payroll Activities </a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1_1">
									<div class="scroller" style="height: 305px;" data-always-visible="1"data-rail-visible1="1">
										<div class="scroller" >
											<ul class="feeds">
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-danger">
																	<i class="fa fa-bolt"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	Ice Ltd Payroll rejected - 250 employees 
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															Just now
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-danger">
																	<i class="fa fa-bolt"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	Heat Ltd Payroll rejected - 50 employees
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															20 mins
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-success">
																	<i class="fa fa-thumbs-o-up"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	Ice Ltd Payroll Rejected January (resolved) - 25 employees 
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															24 mins
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-success">
																	<i class="fa fa-thumbs-o-up"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	Frost Ltd Payroll Rejected January (resolved) - 25 employees 
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															24 mins
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-danger">
																	<i class="fa fa-bolt"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	Metal Ltd Payroll rejected  - 75 employees 
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															1 hour
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-danger">
																	<i class="fa fa-bolt"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	Frozen Ltd Paryoll Rejected - 50 employees
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															2 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-default">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															2 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-warning">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															5 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-info">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															18 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-default">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															21 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-info">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															22 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-default">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															21 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-info">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															22 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-default">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															21 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-info">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															22 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-default">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															21 hours
														</div>
													</div>
												</li>
												<li>
													<div class="col1">
														<div class="cont">
															<div class="cont-col1">
																<div class="label label-sm label-info">
																	<i class="fa fa-bullhorn"></i>
																</div>
															</div>
															<div class="cont-col2">
																<div class="desc">
																	New order received. Please take care of it.
																</div>
															</div>
														</div>
													</div>
													<div class="col2">
														<div class="date">
															22 hours
														</div>
													</div>
												</li>
											</ul>
										</div>
										<div class="slimScrollBar" ></div>
										<div class="slimScrollRail" ></div>
									</div>
								</div>
								<div class="tab-pane " id="tab_1_2">
									<div class="portlet-body tasks-widget">
										<div class="task-content">
											<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
												<!-- START TASK LIST -->
												<ul class="task-list">
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Submit Draft Payroll Ice Ltd | Ice | 23/05/14
															</span>
															<span class="label label-sm label-success">
																Complete
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Submit Tax return for Heat Ltd | Heat | 23/05/14
															</span>
															<span class="label label-sm label-success">
																Complete
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Submit Final Payroll Outputs for Warm Ltd | 22/05/14
															</span>
															<span class="label label-sm label-danger">
																Overdue
															</span>
															<span class="task-bell">
																<i class="fa fa-bell-o"></i>
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Submit Draft Payroll for Metal Ltd | 23/05/14
															</span>
															<span class="label label-sm label-warning">
																Outstanding
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Submit Final Payroll for Outputs for Calm Ltd | 23/05/14
															</span>
															<span class="label label-sm label-warning">
																Outstanding
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Submit Final Payroll Outputs for Calm Ltd | 23/05/14
															</span>
															<span class="label label-sm label-warning">
																Outstanding
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Submit Tax return for Metal Ltd | 23/05/14
															</span>
															<span class="label label-sm label-warning">
																Outstanding
															</span>															
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li>
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Company Staff Meeting
															</span>
															<span class="label label-sm label-success">
																Complete
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
													<li class="last-line">
														<div class="task-checkbox">
															<input type="checkbox" class="liChild" value=""/>
														</div>
														<div class="task-title">
															<span class="task-title-sp">
																Create quarterly report for Ice Ltd | 24/05/2014
															</span>
															<span class="label label-sm label-warning">
																Outstanding
															</span>
														</div>
														<div class="task-config">
															<div class="task-config-btn btn-group">
																<a class="btn btn-xs default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
																</a>
																<ul class="dropdown-menu pull-right">
																	<li>
																		<a href="#">
																			<i class="fa fa-check"></i> Complete
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-pencil"></i> Edit
																		</a>
																	</li>
																	<li>
																		<a href="#">
																			<i class="fa fa-trash-o"></i> Cancel
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</li>
												</ul>
												<!-- END START TASK LIST -->
											</div>
										</div>
										<div class="task-footer">
											<span class="pull-right">
												<a href="#">
													See All Tasks <i class="m-icon-swapright m-icon-gray"></i>
												</a>
												&nbsp;
											</span>
										</div>
									</div>
								</div>						
								<div class="tab-pane" id="tab_1_3">
									<div class="scroller" style="height: 305px;" data-always-visible="1"data-rail-visible1="1">
										<ul class="feeds">
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-thumbs-o-up"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																Heat Ltd approved draft payroll today
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														15:29
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-warning">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																Metal Ltd raised a ticket today 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														15:01
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-warning">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																Ice Ltd raised a ticket today 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														15:00
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-thumbs-o-up"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																May Moore submitted Ice Draft payroll for internal approval 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														1 hour
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-thumbs-o-up"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																May Moore submitted final output reports for Metal Ltd
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														1 hour
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-danger">
																<i class="fa fa-thumbs-o-down"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																James Lyons rejected Warm Ltd Draft payroll
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														2 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-danger">
																<i class="fa fa-thumbs-o-down"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																Ice Ltd Payroll rejected (250 employees)
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														2 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-default">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																Metal Ltd registered 6 new employees
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														3 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-default">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																Ice Ltd submitted payroll for approval
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														4 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-warning">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																Metal Ltd raised a ticket 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														5 hours
													</div>
												</div>
											</li>
										</ul>
										<div class="slimScrollBar" ></div>
										<div class="slimScrollRail" ></div>
									</div>
								</div>
							</div>
						</div>
						<!--END TABS-->	
					</div>
									
				</div>													
			</div>
		</div>
	</div>
		<!-- BEGIN BAR CHARTS -->
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>Accuracy SLA by Client Entity
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="javascript:;" class="reload">
							</a>
							<a href="javascript:;" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div id="localClientChart" class="chart">
						</div>
					</div>
				</div>
			</div>			
			<div class="col-md-6 col-sm-12">
				<div class="portlet box blue">
					<div class="portlet-title line">
						<div class="caption">
							<i class="fa fa-comments"></i>My Ticket Details
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="" class="reload">
							</a>
							<a href="" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body" id="chats">
						<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
							<ul class="chats">
								<li class="in">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar2.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Mary Moore
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</span>
									</div>
								</li>
								<li class="out">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar1.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Bob Nilson
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</span>
									</div>
								</li>
								<li class="in">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar2.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Mary Moore
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</span>
									</div>
								</li>
								<li class="out">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar3.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Richard Doe
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</span>
									</div>
								</li>
								<li class="in">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar2.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Mary Moore
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</span>
									</div>
								</li>
								<li class="out">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar1.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Bob Nilson
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</span>
									</div>
								</li>
								<li class="in">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar2.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Mary Moore
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
										</span>
									</div>
								</li>
								<li class="out">
									<img class="avatar img-responsive" alt="" src="/assets/img/avatar1.jpg"/>
									<div class="message">
										<span class="arrow">
										</span>
										<a href="#" class="name">
											Bob Nilson
										</a>
										<span class="datetime">
											at May 23, 2014 11:09
										</span>
										<span class="body">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. sed diam nonummy nibh euismod tincidunt ut laoreet.
										</span>
									</div>
								</li>
							</ul>
						</div>
						<div class="chat-form">
							<div class="input-cont">
								<input class="form-control" type="text" placeholder="Type a message here..."/>
							</div>
							<div class="btn-cont">
								<span class="arrow">
								</span>
								<a href="" class="btn blue icn-only">
									<i class="fa fa-check icon-white"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>			

		</div>
		
		<!-- END BAR CHARTS -->
		
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>