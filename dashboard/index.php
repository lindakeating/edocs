<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$localClient = null;
$localTmf = null;
$vmapFlag = true;
$gClientFlag = null;
$CalendarFlag = null;
$UploadFlag = null;
$SortTableFlag = null;
$WizardFlag = null;
$EditableTableFlag = null;

?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Global TMF Dashboard
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row ">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue ">
					<div id="show">
						<div class="visual">
							<i class=""></i>
						</div>
						<div class="details">
							<div class="number">
								99.1%
							</div>
							<div class="desc">
								Total Company SLA
							</div>
						</div>
					</div>
					<div id="hidden" class="hidden">
						<div class="visual">
							<i class=""></i>
						</div>
						<div class="details">
							<div class="number">
								1
							</div>
							<div class="desc">
								sdkjlsljf
							</div>
						</div>
					</div>
					<a id="showMore" class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							 1,080 / 10,000
						</div>
						<div class="desc">
							 Total Payrolls / Employees
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat purple">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							 215 / 23
						</div>
						<div class="desc">
							 Total Open Tickets / Over Tickets
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat yellow">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							 500 / 4
						</div>
						<div class="desc">
							 Total Open / Overdue Payrolls
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="clearfix">
		<div class="row">
			<div class="col-md-6">
				<!-- BEGIN WORLD PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>World
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
								</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
							<a href="javascript:;" class="reload">
								</a>
							<a href="javascript:;" class="remove">
								</a>
						</div>
					</div>
					<div class="portlet-body">
						<div id="vmap_world" class="vmaps">
						</div>
					</div>
				</div>
				<!-- END WORLD PORTLET-->
			</div>
			<div class="col-md-6">
				<div class="portlet box red">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>Rejected Draft Payroll % Total Company YTD 
						</div>
						<ul class="pull-right dropdown-portal">
							<li class="btn-group ">
								<button type="button" class="btn dropdown-toggle custom-red" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span>
									Chart Type
								</span>
								<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu  pull-right" role="menu">
									<li>
										<a href="#emea" id="position-profile" onclick="initBarChart2();" name="emea" >
											EMEA Rejected Payroll
										</a>
									</li>
									<li>
										<a href="#latam" id="age-profile" onclick="initBarChart2()" name="latam">
											LATAM Rejected Payroll
										</a>
									</li>
									<li>
										<a href="#apac" id="age-profile" onclick="initBarChart2()" name="apac">
											APAC Rejected Payroll
										</a>
									</li>
									<li>
										<a href="#benelux" id="age-profile" onclick="initBarChart2()" name="benelux">
											BENELUX Rejected Payroll
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="portlet-body" >
						<div id="bar-chart-2" class="bar-chart-height" >
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- BEGIN BAR CHARTS -->
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="portlet paddingless">
					<div class="portlet-body">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab_1_1" data-toggle="tab">
									Rejected Payroll </a>
								</li>
								<li>
									<a href="#tab_1_2" data-toggle="tab">
									Accuracy </a>
								</li>
								<li>
									<a href="#tab_1_3" data-toggle="tab">
									Rejected Payroll Per Company </a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1_1">
									<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 290px;"><div class="scroller" style="overflow: hidden; width: auto; height: 290px;" data-always-visible="1" data-rail-visible="0" data-initialized="true">
										<ul class="feeds">
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-danger">
																<i class="fa fa-bolt"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 Ice Ltd Payroll rejected - 250 employees 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 Just now
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-danger">
																<i class="fa fa-bolt"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 Heat Ltd Payroll rejected - 50 employees
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 20 mins
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-thumbs-o-up"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 Ice Ltd Payroll Rejected January (resolved) - 25 employees 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 24 mins
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-success">
																<i class="fa fa-thumbs-o-up"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 Frost Ltd Payroll Rejected January (resolved) - 25 employees 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 24 mins
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-danger">
																<i class="fa fa-bolt"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 Metal Ltd Payroll rejected  - 75 employees 
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 1 hour
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-danger">
																<i class="fa fa-bolt"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 Frozen Ltd Paryoll Rejected - 50 employees
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 2 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-default">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 2 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-warning">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 5 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-info">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 18 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-default">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 21 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-info">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 22 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-default">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 21 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-info">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 22 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-default">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 21 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-info">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 22 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-default">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 21 hours
													</div>
												</div>
											</li>
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">
															<div class="label label-sm label-info">
																<i class="fa fa-bullhorn"></i>
															</div>
														</div>
														<div class="cont-col2">
															<div class="desc">
																 New order received. Please take care of it.
															</div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date">
														 22 hours
													</div>
												</div>
											</li>
										</ul>
									</div><div class="slimScrollBar" style="background-color: rgb(187, 187, 187); width: 7px; position: absolute; opacity: 0.4; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; top: 28px; height: 139.00826446280993px; display: block; background-position: initial initial; background-repeat: initial initial;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; background-color: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px; background-position: initial initial; background-repeat: initial initial;"></div></div>
								</div>
								<div class="tab-pane" id="tab_1_2">
									<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 290px;">
										<div class="scroller" style="overflow: hidden; width: auto; height: 290px;" data-always-visible="1" data-rail-visible1="1" data-initialized="true">
												<div class="row">
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">EMEA</div>
															</div>
															<div class="portlet-body">
																<div class="chart" data-percent="99.6">99.60%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">LATAM</div>
															</div>
															<div class="portlet-body">
																<div class="chart" data-percent="99.99">99.99%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">APAC</div>
															</div>
															<div class="portlet-body">
																<div id="easyRedPieChart"  data-percent="95">95.00%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">BENELUX</div>
															</div>
															<div class="portlet-body">
																<div class="chart" data-percent="99">99.00%</div>
															</div>
														</div>
													</div>
												</div>																					
										</div>
									</div>
									<div class="slimScrollBar" style="background-color: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; display: block; background-position: initial initial; background-repeat: initial initial;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; background-color: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px; background-position: initial initial; background-repeat: initial initial;">
									</div>
								</div>
								<div class="tab-pane" id="tab_1_3">
									<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 290px;"><div class="scroller" style="overflow: hidden; width: auto; height: 290px;" data-always-visible="1" data-rail-visible1="1" data-initialized="true">
										<div class="portlet box red">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-thumbs-down"></i>Rejected Final Payroll
												</div>
												<div class="tools">
													<a href="javascript:;" class="collapse"></a>
													<a href="#portlet-config" data-toggle="modal" class="config"></a>
													<a href="javascript:;" class="reload"></a>
													<a href="javascript:;" class="remove"></a>
												</div>
											</div>
											<div class="portlet-body">
												<div class="table-responsive">
													<table class="table table-hover">
														<thead>
															<tr>
																<th></th>
																<th>EMEA</th>
																<th>LATAM</th>
																<th>APAC</th>
																<th>BENELUX</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>YTD</td>
																<td>0</td>
																<td>6</td>
																<td>6</td>
																<td>0<td>										
															</tr>
															<tr>
																<td>MTD</td>
																<td>2</td>
																<td>0</td>
																<td>2</td>
																<td>0</td>
															</tr>
															<tr>
																<td>This Quater</td>
																<td>2</td>
																<td>6</td>
																<td>4</td>
																<td>0</td>
															</tr>
															<tr>
																<td>Last Quarter</td>
																<td>0</td>
																<td>0</td>
																<td>0</td>
																<td>0</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<div class="slimScrollBar" style="background-color: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; z-index: 99; right: 1px; display: block; background-position: initial initial; background-repeat: initial initial;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-top-left-radius: 7px; border-top-right-radius: 7px; border-bottom-right-radius: 7px; border-bottom-left-radius: 7px; background-color: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px; background-position: initial initial; background-repeat: initial initial;"></div></div>
									</div>
								</div>
							</div>
						<!--END TABS-->							
					</div>													
				</div>	
			</div>			
			<div class="col-md-6 col-sm-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>Completion SLA YTD
						</div>
						<ul class="pull-right dropdown-portal">
							<li class="btn-group ">
								<button type="button" class="btn dropdown-toggle green" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span>
									Chart Type
								</span>
								<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu  pull-right" role="menu">
									<li>
										<a href="#emea" id="ema-sla" onclick="initBarChart1();" name="emea" >
											EMEA SLA
										</a>
									</li>
									<li>
										<a href="#latam" id="latam-sla" onclick="initBarChart1()" name="latam">
											LATAM SLA
										</a>
									</li>
									<li>
										<a href="#apac" id="apac-sla" onclick="initBarChart1()" name="apac">
											APAC SLA
										</a>
									</li>
									<li>
										<a href="#benelux" id="benelux-sla" onclick="initBarChart1()" name="benelux">
											BENELUX SLA
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="portlet-body" >
						<div id="bar-chart" class="bar-chart-height">
						</div>
					</div>
				</div>				
			</div>			

		</div>
		<!-- END BAR CHARTS -->
		
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>