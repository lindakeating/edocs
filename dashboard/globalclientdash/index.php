<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");

$gClientFlag = true;
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Global Client Dashboard
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row ">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							 7,950
						</div>
						<div class="desc">
							 AVG Number Employees<br>(2014)
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							€5,205,200 
						</div>
						<div class="desc">
							AVG Payroll Costs<br>Per Month (2014) 
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat purple">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							215 / 23 
						</div>
						<div class="desc">
							Total Number of Open Tickets / Overdue Tickets 
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat yellow">
					<div class="visual">
						<i class=""></i>
					</div>
					<div class="details">
						<div class="number">
							500 / 4 
						</div>
						<div class="desc">
							Total Number of Open Payrolls / Overdue Payrolls 
						</div>
					</div>
					<a class="more" href="#">
						 View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="clearfix">
		<div class="row">
			<div class="col-md-6">
				<!-- BEGIN WORLD PORTLET-->
				<div class="portlet box grey">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>World
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
								</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
							<a href="javascript:;" class="reload">
								</a>
							<a href="javascript:;" class="remove">
								</a>
						</div>
					</div>
					<div class="portlet-body no-padding">
						<div id="vmap_world" class="vmaps " >
						</div>
					</div>
				</div>
				<!-- END WORLD PORTLET-->
			</div>
			<div class="col-md-6">
				<!-- BEGIN TAB PORTLET-->
				<div class="portlet box blue ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>Head Count Statistics
						</div>						
						<ul class="pull-right dropdown-portal">
							<li class="btn-group ">
								<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span>
									Chart Type
								</span>
								<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu  pull-right" role="menu">
									<li>
										<a href="#" id="position-profile" onclick="loadCharts();" name="profile" >
											Position Profile
										</a>
									</li>
									<li>
										<a href="#" id="age-profile" onclick="loadCharts()" name="age">
											Age Profile
										</a>
									</li>
									<li>
										<a href="#" id="age-profile" onclick="loadCharts()" name="gender">
											Gender Profile
										</a>
									</li>
								</ul>
							</li>
						</ul>						
					</div>
					<div class="portlet-body">
						<div class="row row-chart" id="gender">
							<div class="col-md-6">
								<div class="sparkline-chart">
									<div class="number" id="sparkline_bar">
									</div>
									<a class="title" href="#">
										EMEA 
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm">
							</div>
							<div class="col-md-6">
								<div class="sparkline-chart">
									<div class="number" id="sparkline_bar2">
									</div>
									<a class="title" href="#">
										LATAM
									</a>
								</div>
							</div>
							<div class="margin-bottom-10 visible-sm">
							</div>
							<div class="col-md-6">
								<div class="sparkline-chart">
									<div class="number" id="sparkline_line3">
									</div>
									<a class="title" href="#">
										APAC 
									</a>
								</div>
							</div>
							<div class="col-md-6">
								<div class="sparkline-chart">
									<div class="number" id="sparkline_line">
									</div>
									<a class="title" href="#">
										BENELUX 
									</a>
								</div>
							</div>
						</div>						
					</div>
				</div>				
			</div>
		</div>
		<!-- BEGIN BAR CHARTS -->
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="portlet paddingless">
					<div class="portlet-body">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li >
									<a href="#tab_1_1" data-toggle="tab">
									Gender Ratio </a>
								</li>
								<li class="active">
									<a href="#tab_1_2" data-toggle="tab">
									Accuracy </a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane " id="tab_1_1">
									<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 290px;">										
										<div class="row">
											<div class="col-lg-3 col-md-4 cold-sm-6">										
												<div id="headCountPie1" class="chart">
												</div>
											</div>
											<div class="col-lg-3 col-md-4 cold-sm-6">
												<div id="headCountPie2" class="chart">
												</div>
											</div>
											<div class="col-lg-3 col-md-4 cold-sm-6">
												<div id="headCountPie3" class="chart">
												</div>
											</div>
											<div class="col-lg-3 col-md-4 cold-sm-6">
												<div id="headCountPie4" class="chart">
												</div>
											</div>													
										</div>
										<div class="row text-center">
											<div class="col-lg-3 col-md-4 cold-sm-6 ">
												EMEA
											</div>
											<div class="col-lg-3 col-md-4 cold-sm-6">
												LATAM
											</div>
											<div class="col-lg-3 col-md-4 cold-sm-6">
												APAC
											</div>
											<div class="col-lg-3 col-md-4 cold-sm-6">
												BENELUX
											</div>
										</div>
										<div class="row padding-left" >
											<a href="#" class="btn  blue">
												<span class="fa fa-female">
											</span>
												 Female
											</a>
											<a href="#" class="btn  green">
												<span class="fa fa-male">
											</span>
												 Male
											</a>													
										</div>
										
									</div>
								</div>								
								<div class="tab-pane active" id="tab_1_2">
									<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 290px;">
										<div class="scroller" style="overflow: hidden; width: auto; height: 290px;" data-always-visible="1" data-rail-visible1="1" data-initialized="true">
												<div class="row">
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">EMEA</div>
															</div>
															<div class="portlet-body">
																<div class="chart" data-percent="99.6">99.60%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">LATAM</div>
															</div>
															<div class="portlet-body">
																<div class="chart" data-percent="99.99">99.99%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">APAC</div>
															</div>
															<div class="portlet-body">
																<div id="easyRedPieChart"  data-percent="95">95.00%</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3 col-md-6 col-sm-6">
														<div class="portlet box grey">
															<div class="portlet-title">
																<div class="caption">BENELUX</div>
															</div>
															<div class="portlet-body">
																<div class="chart" data-percent="99">99.00%</div>
															</div>
														</div>
													</div>
												</div>																				
										</div>
									</div>
									<div class="slimScrollBar" ></div>
									<div class="slimScrollRail" ></div>
								</div>																
							</div>
						</div>
						<!--END TABS-->							
					</div>													
				</div>				
			</div>			
			<div class="col-md-6 col-sm-12">
				<div class="portlet box red">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>Rejected Draft Payroll % Total Company YTD 
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="javascript:;" class="reload">
							</a>
							<a href="javascript:;" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body" >
						<div id="bar-chart-2" class="bar-chart-height" >
						</div>
					</div>
				</div>
			</div>			

		</div>
		
		<!-- END BAR CHARTS -->
		
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>