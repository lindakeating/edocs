<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");

$WizardFlag = true;
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php");
 ?>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				TMF Offices
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="note note-success">
					<div class="alert alert-block alert-success fade in">
						<h4 class="alert-heading">Success!</h4>
						<h4 class="alert-heading"> You have successfully created the office <?php echo( $_SESSION['newUser']['localName']) ?></h4>
						<h5 class="alert-heading">An email has been sent to <?php echo($_SESSION['newUser']['fname'].' '.$_SESSION['newUser']['lname']) ?> to notify them of your action. </h5>
					</div>
				</div>
			</div>
			<div 
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>