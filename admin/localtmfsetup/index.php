<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$datePicker = true;
$WizardFlag = true;
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php");
 ?>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="modal fade" id="form_success">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">					
						<h4 class="modal-title">Congratulations! </h4>
					</div>
					<div class="modal-body">
						<p>You have successfully created a local TMF office. You must now go to user set up to create office users and assign them to clients.</p>
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
		</div>
		<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> <?php 
				
				
				echo($_SESSION['newUser']['localName']); ?> Office Creation
								<span class="step-title">
									 Step 1 of 4
								</span>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form  class="form-horizontal" id="createLocalTmf" method="POST">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
													<span class="number">
														 1
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Local Office 
													</span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
													<span class="number">
														 2
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Out of Office
													</span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step">
													<span class="number">
														 3
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Payroll Process
													</span>
												</a>
											</li>
											<li>
												<a href="#tab4" data-toggle="tab" class="step">
													<span class="number">
														 4
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> File Submission
													</span>
												</a>
											</li>
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												You have some form errors. Please check below.
											</div>
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Local TMF details provided by global TMF</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Local Office Name
													</label>
													<div class="col-md-4">
														<input type="hidden" class="form-control" name="office_name" value="<?php echo($_SESSION['newUser']['localName']); ?>" >
														<input type="text" class="form-control"  value="<?php echo($_SESSION['newUser']['localName']); ?>" disabled/>
														<span class="help-block">
															 Office Name
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Platform
													</label>
													<div class="col-md-4">
														<input type="hidden" class="form-control" name="platform"  value="<?php echo($_SESSION['newUser']['platform']); ?>" >
														<input type="text" class="form-control" id="submit_form_password" value="<?php echo($_SESSION['newUser']['platform']); ?>" disabled/>
														<span class="help-block">
															 Platform
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Country Code
													</label>
													<div class="col-md-4">
														<input type="hidden" class="form-control" name="country" value="<?php echo($_SESSION['newUser']['country']); ?>" disabled/>
														<input type="text" class="form-control" value="<?php echo($_SESSION['newUser']['country']); ?>" disabled/>
														<span class="help-block">
															 Country Code
														</span>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab2">
												<h3 class="block">Please input your out of office days</h3>
												<div class="form-group">
													<label class="control-label col-md-3">Weekly Out of Office days
													<span class="required">
														 *
													</span>
													</label>
													<div class="col-md-4">
														<div class="checkbox-list">
															<div class="row">
																<div class="col-md-6 col-sm-6">
																	<label class="checkbox-inline override">
																		<input type="checkbox" id="mondayCheckbox" value="monday" name="weekly_out_of_office[]"> Monday
																	</label>
																</div>
																<div class="col-md-6 col-sm-6">
																	<label class="checkbox-inline override">
																		<input type="checkbox" id="tuesdayCheckbox" value="tuesday" name="weekly_out_of_office[]"> Tuesday
																	</label>
																</div>
																<div class="col-md-6 col-sm-6">
																	<label class="checkbox-inline override">
																		<input type="checkbox" id="wednesdayCheckbox" value="wednesday" name="weekly_out_of_office[]"> Wednesday
																	</label>
																</div>
																<div class="col-md-6 col-sm-6">
																	<label class="checkbox-inline override">
																		<input type="checkbox" id="thursdayCheckbox" value="thursday" name="weekly_out_of_office[]"> Thursday
																	</label>
																</div>
																<div class="col-md-6 col-sm-6">
																	<label class="checkbox-inline override">
																		<input type="checkbox" id="fridayCheckbox" value="friday" name="weekly_out_of_office[]" > Friday
																	</label>
																</div>
																<div class="col-md-6 col-sm-6">																
																	<label class="checkbox-inline override">
																		<input type="checkbox" id="saturdayCheckbox" value="saturday" name="weekly_out_of_office[]" > Saturday
																	</label>
																</div>
																<div class="col-md-6 col-sm-6">
																	<label class="checkbox-inline override">
																		<input type="checkbox" id="sundayCheckbox" value="sunday" name="weekly_out_of_office[]"> Sunday
																	</label>
																</div>
															</div>
														</div>	
														<span class="help-block">
															 Check each weekly out of office day
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Select Public Holidays
													<span class="required">
														 *
													</span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" value="select a date" id="dp1" >															
													</div>						
												</div>
												<div id="dates" >
												</div>

											</div>
											<div class="tab-pane" id="tab3">
												<h3 class="block">Please provide your payroll process days</h3>
												
												<div class="form-group">
													<label class="control-label col-md-3">Pay Day
													</label>
													<div class="col-md-4">
														<input type="hidden" class="form-control" name="pay_day" value="0" />
														<input type="text" class="form-control"  value="0" disabled/>
														<span class="help-block">
														 Pay day is always set to 0
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Final Approval
													<span class="required">
														 *
													</span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="final_approval" value="-1"/>
														<span class="help-block">
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Final Output
													<span class="required">
														 *
													</span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="final_output" value="-1"/>
														<span class="help-block">
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Draft Approval
													<span class="required">
														 *
													</span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="draft_approval" value="-2"/>
														<span class="help-block">
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Draft Output
													<span class="required">
														 *
													</span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="draft_output" value="-3"/>
														<span class="help-block">
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Input
													<span class="required">
														 *
													</span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="input" value="-6"/>
														<span class="help-block">
														</span>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab4">
												<div class="col-md-6">
													<h3 class=" block">Provide File Input Types</h3>
												</div>
												<div class="col-md-6">
													<h3 class=" block">Provide File Output Types</h3>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2">Master Data Changes
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="manInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
													<label class="control-label col-md-2">Global Reporting File
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="hidden" checked="checked" name="manOutput[]" checked>
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" disabled checked="checked">
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="hidden" checked="checked" name="reqOutput[]" checked>
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" disabled checked="checked">
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>	
												<div class="form-group">
													<label class="control-label col-md-2">Payroll Changes
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="manInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
													<label class="control-label col-md-2">Payslip Files
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="hidden" checked="checked" name="manOutput[]" checked>
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" disabled checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="hidden" checked="checked" name="reqOutput[]" checked>
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" disabled checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>	
												<div class="form-group">
													<label class="control-label col-md-2">Starters
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" name="manInput[]" value="Yes" class="radio" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
													<label class="control-label col-md-2">GL File
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="hidden" checked="checked" name="manOutput[]" checked>
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" disabled checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="hidden" checked="checked" name="reqOutput[]" checked>
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" disabled checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2">Leavers
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" name="manInput[]" value="Yes" class="radio" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
													<label class="control-label col-md-2">Gross to Net
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
														<input type="checkbox" class="hidden" checked="checked" name="manOutput[]" checked>
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" disabled checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="hidden" checked="checked" name="reqOutput[]" checked>
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" disabled checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2">Bonus/Commissions File
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" name="manInput[]" class="radio" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
													<label class="control-label col-md-2">Social Security Declaration
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" checked >
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" checked >
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>
	`											<div class="form-group">
													<label class="control-label col-md-2">Absence Data
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" name="manInput[]" class="radio" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqInput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
													<label class="control-label col-md-2">Tax Declaration
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>
												<div class="form-group">

														<label class="control-label col-md-2">Other (please specify)
														</label>
														<div class="col-md-4">
														<input type="text" class="form-control " name="other" placeholder="Other Files" checked>
														</input>
														</div>
														<label class="control-label col-md-2">Bank File
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" checked >
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-2">
													</label>
													<div class="col-md-2">
													</div>
													<div class="col-md-2">
													</div>
													<label class="control-label col-md-2">Bank List
													</label>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="manOutput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Mandatory
														</span>
													</div>
													<div class="col-md-2">
														<div id="file2" class="switch" data-toggle="buttons" >
															<input type="checkbox" class="radio" name="reqOutput[]" value="Yes" checked>
														</div>
														<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
														<span class="help-block">
															 Required
														</span>
													</div>
												</div>
												<div class="form-group">
												<div class="col-md-6">
												</div>
													<label class="control-label col-md-2">Other (please specify)
													</label>
													<div class="col-md-4">
													<input type="text" class="form-control " name="other" placeholder="Other Files">
													</input>
													</div>														
												</div>												
											</div>											
										</div>
									</div>
									<div class="form-actions fluid">
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-offset-3 col-md-9">
													<a href="javascript:;" class="btn default button-previous">
														<i class="m-icon-swapleft"></i> Back
													</a>
													<a href="javascript:;" class="btn blue button-next">
														 Continue <i class="m-icon-swapright m-icon-white"></i>
													</a>
													<input id="submitLocalClient" type="submit" class="btn green button-submit"><i class="m-icon-swapright m-icon-white"></i>
													</input>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>			
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>