<?php
$PageTitle = "Ticket Management";

include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Manage 
				Tickets
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-edit"></i>Ticket Management
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="btn-group">
									<button id="sample_editable_1_new" class="btn green">
									Add New <i class="fa fa-plus"></i>
									</button>
								</div>
								<div class="btn-group pull-right">
									<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="#">
												 Print
											</a>
										</li>
										<li>
											<a href="#">
												 Save as PDF
											</a>
										</li>
										<li>
											<a href="#">
												 Export to Excel
											</a>
										</li>
									</ul>
								</div>
							</div>
							<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
							<thead>
							<tr>
								<th>
									 Ticket Reason Codes
								</th>
								<th>
									 Ticket Reason Priority
								</th>
								<th>
									 Ticket Priority Leval SLa
								</th>
								<th>
									 Edit
								</th>
								<th>
									 Delete
								</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									New Input Request
								</td>
								<td>
									 Level 1
								</td>
								<td >
									 1 Day
								</td>
								<td>
									<a class="edit" href="javascript:;">
										 Edit
									</a>
								</td>
								<td>
									<a class="delete" href="javascript:;">
										 Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>
									 Query Draft Report
								</td>
								<td>
									 Level 1
								</td>
								<td>
									 1 Day
								</td>
								<td>
									<a class="edit" href="javascript:;">
										 Edit
									</a>
								</td>
								<td>
									<a class="delete" href="javascript:;">
										 Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>
									 Reject Final Payroll - New Input File
								</td>
								<td>
									 Level 1
								</td>
								<td>
									 3 hrs
								</td>
								<td>
									<a class="edit" href="javascript:;">
										 Edit
									</a>
								</td>
								<td>
									<a class="delete" href="javascript:;">
										 Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>
									 Reject Final Payroll - Input Interpretation
								</td>
								<td>
									 Level 1
								</td>
								<td>
									 3 hrs
								</td>
								<td>
									<a class="edit" href="javascript:;">
										 Edit
									</a>
								</td>
								<td>
									<a class="delete" href="javascript:;">
										 Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>
									Other 1
								</td>
								<td>
									 Level 1
								</td>
								<td>
									 3 hrs
								</td>
								<td>
									<a class="edit" href="javascript:;">
										 Edit
									</a>
								</td>
								<td>
									<a class="delete" href="javascript:;">
										 Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>
									 Other 2
								</td>
								<td>
									Level 1
								</td>
								<td>
									 3 hrs
								</td>
								<td>
									<a class="edit" href="javascript:;">
										 Edit
									</a>
								</td>
								<td>
									<a class="delete" href="javascript:;">
										 Delete
									</a>
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>