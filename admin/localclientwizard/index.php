<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$datePicker = true;
$WizardFlag = true;
$localClientWizard= true;
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php");
 ?>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content">

		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
        <div class="modal fade" id="form_success">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Success: Local Client Creation</h4>
                    </div>
                    <div class="modal-body">
                        <p>You have successfully created your new client, to amend these details go to client manager</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
		<div id="tmf-out-of-office-days" class="col-md-6 hidden">

		</div>
		<div class="hidden weekly-tmf-ooo">
			<h4 class="block">Weekly Out of Office Days</h4>

				<?php 
					$sql = "SELECT * FROM out_of_office";
					$result = mysql_query($sql);
					$weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
					$i = 0;
					while($row = mysql_fetch_assoc($result)){
						if($row['weekly_out_of_office']== 'true'){
							echo ('<div class="col-md-3 input-group pull-left">
									 <input type="checkbox" name="weekly_out_of_office" disabled unchecked>
									 <span class="help-block">'.$weekdays[$i].'</span>
								</div>');							
						}
						else{
							echo ('<div class="col-md-3 input-group pull-left">
									 <input type="checkbox" name="weekly_out_of_office" checked >
									 <span class="help-block">'.$weekdays[$i].'</span>
								</div>');
						}
						$i++;
					}
				?>
		</div>
		<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="client_form_wizard">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Global Client Wizard
								<span class="step-title">
									 Step 1 of 4
								</span>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form  class="form-horizontal" id="submit_form" data-ajax="false">
								<div class="form-wizard">
									<div class="form-body no-padding">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
													<span class="number">
														 1
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Client Details
													</span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
													<span class="number">
														 2
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Single Entities
													</span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step">
													<span class="number">
														 3
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Calendar Details
													</span>
												</a>
											</li>
											<li>
												<a href="#tab4" data-toggle="tab" class="step">
													<span class="number">
														 4
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Pay Day Rules
													</span>
												</a>
											</li>
											<li>
												<a href="#tab5" data-toggle="tab" class="step">
													<span class="number">
														 5
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Pay Process Days
													</span>
												</a>
											</li>
											<li>
												<a href="#tab6" data-toggle="tab" class="step">
													<span class="number">
														 6
													</span>
													<span class="desc">
														<i class="fa fa-check"></i> Optional Tasks
													</span>
												</a>
											</li>
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Client Details</h3>												
												<div class="form-group">
													<label class="control-label col-md-3">Client Name
													</label>
													<div class="col-md-4">
														<input type="hidden" id="form_name" class="form-control" name="form_name" value="local_client_wizard"/>
														<input type="text" class="form-control" name="client_name"/>
														<span class="help-block">
															 Client Name
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Currency
													</label>
													<div class="col-md-4">
														<select class="form-control">
															<option value="EUR">€</option>
															<option value="GBP">British £</option>
															<option value="USD">US $</option>
															<option value="AUD">AUS $</option>												
														</select>
														<span class="help-block">
															Currency
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Currency Rule
													</label>
													<div class="col-md-4">
														<select class="form-control">
															<option value="">Select a Currency Rule</option>
															<option value="monthEndRate">Month End Rate</option>
															<option value="averageRate">Average Rate</option>
												
														</select>
														<span class="help-block">
															Currency
														</span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">No. of client payrolls
													</label>
													<div class="col-md-4">
														<select type="text" class="form-control 1-100" name="no_client_payrolls" id="no_client_payrolls"></select>
														<span class="help-block">
															 No. Client Payrolls
														</span>
													</div>
												</div>
												<div class="col-md-6">
													<h3 class="block">File Output Types</h3>
												</div>
												<div class="col-md-6">
													<h3 class="block">Subscribe</h3>
												</div>
													<div class="form-group">
														<label class="control-label col-md-3">Global Reporting File
														</label>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="text" class="hidden" name="fileType[]" value="Global Reporting File">
																<input type="checkbox" class="hidden" name="manFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="manfileOutput[]" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Mandatory
															</span>
														</div>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="hidden" name="reqFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="reqFileOutput[]" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Required
															</span>
														</div>
														<label class="control-label col-md-2">Global Reporting</label>
														<div class="col-md-2">
															<div id="globalReporting" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="radio" name="globalReporting" value="Yes"  >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Subscribe
															</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Payslip File
														</label>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="text" class="hidden" name="fileType[]" value="Payslip File">
																<input type="checkbox" class="hidden" name="manFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="manPaySlip" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Mandatory
															</span>
														</div>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="hidden" name="reqFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="reqPaySlip" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Required
															</span>
														</div>
														<label class="control-label col-md-2">Self Service Portal</label>
														<div class="col-md-2">
															<div id="selfServicePortal"  class="switch" data-toggle="buttons" >
																<input type="checkbox" class="radio" name="selfServicePortal" value="Yes"  >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Subscribe
															</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">GL File
														</label>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="text" class="hidden" name="fileType[]" value="GL File" >
																<input type="checkbox" class="hidden" name="manFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="manGLFile" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Mandatory
															</span>
														</div>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="hidden" name="reqFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="reqGLFile" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Required
															</span>
														</div>														
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Gross To Net
														</label>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="text" class="hidden" name="fileType[]" value="Gross To Net" >
																<input type="checkbox" class="hidden" name="manFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="manGrossToNet" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Mandatory
															</span>
														</div>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="hidden" name="reqFileOutput[]" value="Yes" checked>
																<input type="checkbox" class="radio" name="reqGrossToNet" value="Yes" disabled checked>
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Required
															</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Social Security Declaration
														</label>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="text" class="hidden" name="fileType[]" value="Social Security Declaration" >
																<input type="checkbox" class="radio" name="manFileOutput[]" value="Yes" >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Mandatory
															</span>
														</div>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="radio" name="reqFileOutput[]" value="Yes" >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Required
															</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Tax Declaration
														</label>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="text" class="hidden" name="fileType[]" value="Tax Declaration" >
																<input type="checkbox" class="radio" name="manFileOutput[]" value="Yes" >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Mandatory
															</span>
														</div>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="radio" name="reqFileOutput[]" value="Yes" >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Required
															</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Bank File
														</label>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="text" class="hidden" name="fileType[]" value="Bank File" >
																<input type="checkbox" class="radio" name="manFileOutput[]" value="Yes" >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Mandatory
															</span>
														</div>
														<div class="col-md-2">
															<div id="file2" class="switch" data-toggle="buttons" >
																<input type="checkbox" class="radio" name="reqFileOutput[]" value="Yes" >
															</div>
															<!--<input name="mandatory3" id="mandatory2" type="hidden" value="yes">-->
															<span class="help-block">
																Required
															</span>
														</div>
													</div>
												<div class="form-group">
													<label class="control-label col-md-3">Other (please specify)
													</label>
													<div class="col-md-4">
													<input type="text" class="form-control " name="other" placeholder="Other Files">
													</input>
													</div>														
												</div>
											</div>
											<div class="tab-pane" id="tab2">
												<h3 class="block">Entity Details</h3>
												<div id="client_payroll_names">
												</div>
											</div>
											<div class="tab-pane" id="tab3">
											<div class="col-md-6">
												<h3 class="block">Provide Out of Office Days</h3>
											</div>
												<div class="form-group">
													<div id="calendar_entity" class="col-md-12">
													</div>													
												</div>																								
											</div>
											<div class="tab-pane" id="tab4">
												<h3 class="block">Provide Pay Day Rules</h3>
												<div id="pay_day_rules" >
													
												</div>														
											</div>
											<div class="tab-pane" id="tab5">
												<h3 class="block">Payroll Process Days - System Tasks</h3>
												<div id="pay_process_days" >
													
												</div>														
											</div>
											<div class="tab-pane" id="tab6">
												<h3 class="block">Specify Optional Tasks</h3>
												<div id="optional_tasks" >
													
												</div>														
											</div>												
										</div>
									</div>
									<div class="form-actions fluid">
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-offset-3 col-md-9">
													<a href="javascript:;" class="btn default button-previous">
														<i class="m-icon-swapleft"></i> Back
													</a>
													<a href="javascript:;" class="btn blue button-next">
														 Continue <i class="m-icon-swapright m-icon-white"></i>
													</a>
													<input id="submitLocalClient" type="submit" class="btn green button-submit" data-toggle="modal" data-target="#form_success"><i class="m-icon-swapright m-icon-white"></i>
													</input>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>			
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>