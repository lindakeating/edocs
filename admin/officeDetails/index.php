<?php
$PageTitle = "Local Office Management";
$maps = true;
$tree = true;

include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
    <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    Widget settings form goes here
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue">Save changes</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN STYLE CUSTOMIZER -->
    <div class="theme-panel hidden-xs hidden-sm">
        <div class="toggler">
        </div>
        <div class="toggler-close">
        </div>
        <div class="theme-options">
            <div class="theme-option theme-colors clearfix">
						<span>
							 THEME COLOR
						</span>
                <ul>
                    <li class="color-black current color-default" data-style="default">
                    </li>
                    <li class="color-blue" data-style="blue">
                    </li>
                    <li class="color-brown" data-style="brown">
                    </li>
                    <li class="color-purple" data-style="purple">
                    </li>
                    <li class="color-grey" data-style="grey">
                    </li>
                    <li class="color-white color-light" data-style="light">
                    </li>
                </ul>
            </div>
            <div class="theme-option">
						<span>
							 Layout
						</span>
                <select class="layout-option form-control input-small">
                    <option value="fluid" selected="selected">Fluid</option>
                    <option value="boxed">Boxed</option>
                </select>
            </div>
            <div class="theme-option">
						<span>
							 Header
						</span>
                <select class="header-option form-control input-small">
                    <option value="fixed" selected="selected">Fixed</option>
                    <option value="default">Default</option>
                </select>
            </div>
            <div class="theme-option">
						<span>
							 Sidebar
						</span>
                <select class="sidebar-option form-control input-small">
                    <option value="fixed">Fixed</option>
                    <option value="default" selected="selected">Default</option>
                </select>
            </div>
            <div class="theme-option">
						<span>
							 Sidebar Position
						</span>
                <select class="sidebar-pos-option form-control input-small">
                    <option value="left" selected="selected">Left</option>
                    <option value="right">Right</option>
                </select>
            </div>
            <div class="theme-option">
						<span>
							 Footer
						</span>
                <select class="footer-option form-control input-small">
                    <option value="fixed">Fixed</option>
                    <option value="default" selected="selected">Default</option>
                </select>
            </div>
        </div>
    </div>
    <!-- END STYLE CUSTOMIZER -->
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                About Us 
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li class="btn-group">
                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li>
                            <a href="#">
                                Action
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Another action
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Something else here
                            </a>
                        </li>
                        <li class="divider">
                        </li>
                        <li>
                            <a href="#">
                                Separated link
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <i class="fa fa-home"></i>
                    <a href="index.html">
                        Home
                    </a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">
                        Pages
                    </a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">
                        About Us
                    </a>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row margin-bottom-30">
            <div class="col-md-6">
                <div class="space20">
                </div>
                <div class="well">
                    <h4>Address</h4>
                    <address>
                        <strong>Local TMF Office</strong><br>
                        34 St Stephens Green<br>
                        Co. Dublin<br>
                        <abbr title="Phone">P:</abbr> (234) 145-1810 </address>
                    <address>
                        <strong>Email</strong><br>
                        <a href="mailto:#">
                            first.last@email.com
                        </a>
                    </address>
                    <ul class="social-icons margin-bottom-10">
                        <li>
                            <a href="#" data-original-title="facebook" class="facebook">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="github" class="github">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="Goole Plus" class="googleplus">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="linkedin" class="linkedin">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="rss" class="rss">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="skype" class="skype">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="twitter" class="twitter">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="youtube" class="youtube">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        <div class="col-md-6">
            <div id="gmap_marker" class="gmaps">
            </div>
        </div>
    </div>
    <!--/row-->
    <!-- Meer Our Team -->
    <div class="headline">
        <h3>Meet Our Team</h3>
    </div>
    <div class="row thumbnails">
        <div class="col-md-3">
            <div class="meet-our-team">
                <h3>Bob Nilson </h3>
                <img src="/assets/img/pages/2.jpg" alt="" class="img-responsive"/>
                <div class="team-info">
                    <p>
                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                    </p>
                    <ul class="social-icons pull-right">
                        <li>
                            <a href="#" data-original-title="twitter" class="twitter">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="facebook" class="facebook">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="linkedin" class="linkedin">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="Goole Plus" class="googleplus">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="skype" class="skype">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="meet-our-team">
                <h3>Marta Doe </h3>
                <img src="/assets/img/pages/3.jpg" alt="" class="img-responsive"/>
                <div class="team-info">
                    <p>
                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                    </p>
                    <ul class="social-icons pull-right">
                        <li>
                            <a href="#" data-original-title="twitter" class="twitter">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="facebook" class="facebook">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="linkedin" class="linkedin">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="Goole Plus" class="googleplus">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-original-title="skype" class="skype">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 well">
        <h3>Documents</h3>
        <div id="tree_1" class="tree-demo col-md-6">
            <ul>
                <li>
                    Country Documents
                    <ul>
                        <li data-jstree='{ "selected" : false }'>
                            <a href="#">
                                France
                            </a>
                        </li>
                        <li data-jstree='{ "icon" : "fa fa-briefcase icon-success " }'>
                            custom icon URL
                        </li>
                        <li data-jstree='{ "opened" : true }'>
                            Ireland
                            <ul>
                                <li data-jstree='{ "disabled" : false, "selected" : true, "icon": "fa fa-file-text icon-danger" }'>
                                    <a href="countrySummary.pdf" download="countrySummary.pdf" target="_blank" >Country Summary</a>
                                </li>
                                <li data-jstree='{ "icon": "fa fa-file-text icon-danger" }'>
                                    <a href="legal.pdf" download="legal.pdf" target="_blank"> Legal Structures</a>
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    Incorporation Procedures
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    On-going Obligations
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Tax Implications
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Labour Environment
                                </li>

                            </ul>
                        </li>
                        <li data-jstree='{  "opened" : false}'>
                            <a href="#">
                                Italy
                            </a>
                            <ul  data-jstree='{"opened": false}'>
                                <li data-jstree='{ "disabled" : false,  "icon": "fa fa-file-text icon-danger" }'>
                                    Country Summary
                                </li>
                                <li data-jstree='{ "icon": "fa fa-file-text icon-danger" }'>
                                    Legal Structures
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    Incorporation Procedures
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    On-going Obligations
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Tax Implications
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Labour Environment
                                </li>

                            </ul>
                        </li>
                        <li data-jstree='{ "opened" : false}' class="jstree-closed">
                            <a href="#">
                                Spain
                            </a>
                            <ul  data-jstree='{"opened": false}' class="jstree-closed">
                                <li data-jstree='{"opened": false, "disabled" : false,  "icon": "fa fa-file-text icon-danger" }'>
                                    Country Summary
                                </li>
                                <li data-jstree='{ "opened": false, "icon": "fa fa-file-text icon-danger" }'>
                                    Legal Structures
                                </li>
                                <li data-jstree='{"opened": false, "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    Incorporation Procedures
                                </li>
                                <li data-jstree='{ "opened": false, "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    On-going Obligations
                                </li>
                                <li data-jstree='{ "opened": false, "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Tax Implications
                                </li>
                                <li data-jstree='{"opened": false, "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Labour Environment
                                </li>

                            </ul>
                        </li>
                        <li data-jstree='{  "opened" : false}' class="jstree-closed">
                            <a href="#">
                                US
                            </a>
                            <ul  data-jstree='{"opened": false}'>
                                <li data-jstree='{ "disabled" : false,  "icon": "fa fa-file-text icon-danger" }'>
                                    Country Summary
                                </li>
                                <li data-jstree='{ "icon": "fa fa-file-text icon-danger" }'>
                                    Legal Structures
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    Incorporation Procedures
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    On-going Obligations
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Tax Implications
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Labour Environment
                                </li>

                            </ul>
                        </li>
                        <li data-jstree='{  "opened" : false}'>
                            <a href="#">
                                Italy
                            </a>
                            <ul  data-jstree='{"opened": false}'>
                                <li data-jstree='{ "disabled" : false,  "icon": "fa fa-file-text icon-danger" }'>
                                    Country Summary
                                </li>
                                <li data-jstree='{ "icon": "fa fa-file-text icon-danger" }'>
                                    Legal Structures
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    Incorporation Procedures
                                </li>
                                <li data-jstree='{ "type" : "file",  "icon": "fa fa-file-text icon-danger" }'>
                                    On-going Obligations
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Tax Implications
                                </li>
                                <li data-jstree='{ "type" : "file" ,  "icon": "fa fa-file-text icon-danger"}'>
                                    Labour Environment
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        </div>
    </div>
    <!--/thumbnails-->
    <!-- //End Meer Our Team -->
    <!-- END PAGE CONTENT-->
    </div>
    </div>

<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>