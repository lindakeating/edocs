<?php
session_start();

if($_SERVER['DOCUMENT_ROOT']!= "C:/wamp/www/"){
	// Create connection
	$host = 'localhost';
	$username = 'tmf';
	$password = 'tmf';
	$database = 'tmf';
	
	$con = mysql_connect($host, $username, $password); 
	$db_selected = mysql_select_db($database, $con);

	// Check connection
	if (mysqli_connect_errno()) {
  	echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}	
}
else{

	// Create connection
	$host = 'localhost';
	$username = 'tmf';
	$password = 'tmf';
	$database = 'tmf';
	
	
	$con = mysql_connect($host, $username, $password); 
	$db_selected = mysql_select_db($database, $con);

	if(!$connection = mysql_connect($host, $username, $password))
		die('Error connecting to '.$host.'. '.mysql_error());

	if(!mysql_select_db($database))
		die('Error selecting '.$database.'. '.mysql_error());
}


$PageURL = substr($_SERVER['REQUEST_URI'], 1);
$PageURL = explode("/", $PageURL);


switch ($PageURL[0]) {
		
	case "dashboard":
		$PageTitle = "Dashboard";
		break;
		
	case "mypayroll":
		$PageTitle = "My Payroll";
		break;
		
	case "reports":
		$PageTitle = "Reports";
		break;
	
	case "selfservicemanager":
		$PageTitle = "Self Service Manager";
		break;
			
}

switch ($PageURL[1]) {
	case "globaltmfdash":
		$PageTitle = "Global Dashboard ";
		break;
		
	case "localtmf":
		$PageTitle =" Local ";
		break;
		
	case "globalclientdash":
		$PageTitle = "Global Dashboard | Client";
		break;
		
	case "localclient":
		$PageTitle = "Local | Client";
		break;
		
	case "processing":
		$PageTitle = "Payroll Processing";
		break;
		
	case "mytasks":
		$PageTitle = "My Tasks";
		break;
	
	case "mytickets":
		$PageTitle = "My Tickets";
		break;
}

$TodaysTasks = 2;
$OverdueTasks = 3;
?>
<!DOCTYPE html>

<!-- 

-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>TMF | <?php echo $PageTitle; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-notific8/jquery.notific8.min.css"/>
<link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet"/>
<link href="/assets/css/pages/timeline.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/assets/css/jquery.qtip.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/assets/css/pages/tasks.css" rel="stylesheet"/>

<link href="/assets/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
<link href="/assets/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
<link href="/assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<link href="/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet"/>
<link href="/assets/plugins/flot/examples/examples.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2-metronic.css"/>
<link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css"/>
<link rel="stylesheet" href="/assets/css/colVis.css"/>
<link href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="/assets/plugins/bootstrap-switch/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="/assets/plugins/jstree/dist/themes/default/style.min.css"/>



    <!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->