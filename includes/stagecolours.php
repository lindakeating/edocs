<?php
switch ($Stage1)
{
	case 1:
		$Stage1Colour = "grey";
		break;
	case 2:
		$Stage1Colour = "yellow";
		break;
	case 3:
		$Stage1Colour = "grey";
		break;
	case 4:
		$Stage1Colour = "grey";
		break;
	case 5:
		$Stage1Colour = "green";
		break;
	case 6:
		$Stage1Colour = "yellow";
		break;
	case 7:
		$Stage1Colour = "red";
		break;
	case 8:
		$Stage1Colour = "red";
		break;
	default:
		$Stage1Colour = "blue";
		break;
}

switch ($Stage2)
{
	case 1:
		$Stage2Colour = "blue";
		break;
	case 2:
		$Stage2Colour = "blue";
		break;
	case 3:
		$Stage2Colour = "grey";
		break;
	case 4:
		$Stage2Colour = "grey";
		break;
	case 5:
		$Stage2Colour = "green";
		break;
	case 6:
		$Stage2Colour = "yellow";
		break;
	case 7:
		$Stage2Colour = "red";
		break;
	case 8:
		$Stage2Colour = "red";
		break;
	default:
		$Stage2Colour = "blue";
		break;
}

switch ($Stage3)
{
	case 1:
		$Stage3Colour = "blue";
		break;
	case 2:
		$Stage3Colour = "yellow";
		break;
	case 3:
		$Stage3Colour = "grey";
		break;
	case 4:
		$Stage3Colour = "grey";
		break;
	case 5:
		$Stage3Colour = "green";
		break;
	case 6:
		$Stage3Colour = "yellow";
		break;
	case 7:
		$Stage3Colour = "red";
		break;
	case 8:
		$Stage3Colour = "red";
		break;
	default:
		$Stage3Colour = "blue";
		break;
}

switch ($Stage4)
{
	case 1:
		$Stage4Colour = "blue";
		break;
	case 2:
		$Stage4Colour = "yellow";
		break;
	case 3:
		$Stage4Colour = "grey";
		break;
	case 4:
		$Stage4Colour = "grey";
		break;
	case 5:
		$Stage4Colour = "green";
		break;
	case 6:
		$Stage4Colour = "yellow";
		break;
	case 7:
		$Stage4Colour = "red";
		break;
	case 8:
		$Stage4Colour = "red";
		break;
	default:
		$Stage4Colour = "blue";
		break;
}

switch ($Stage5)
{
	case 1:
		$Stage5Colour = "blue";
		break;
	case 2:
		$Stage5Colour = "yellow";
		break;
	case 3:
		$Stage5Colour = "grey";
		break;
	case 4:
		$Stage5Colour = "grey";
		break;
	case 5:
		$Stage5Colour = "green";
		break;
	case 6:
		$Stage5Colour = "yellow";
		break;
	case 7:
		$Stage5Colour = "red";
		break;
	case 8:
		$Stage5Colour = "red";
		break;
	default:
		$Stage5Colour = "blue";
		break;
}

switch ($Stage6)
{
	case 1:
		$Stage6Colour = "blue";
		$Stage7Colour = "blue";
		$Stage8Colour = "blue";
		break;
	case 2:
		$Stage6Colour = "yellow";
		$Stage7Colour = "yellow";
		$Stage8Colour = "yellow";
		break;
	case 3:
		$Stage6Colour = "grey";
		$Stage7Colour = "grey";
		$Stage8Colour = "grey";
		break;
	case 4:
		$Stage6Colour = "grey";
		$Stage7Colour = "grey";
		$Stage8Colour = "grey";
		break;
	case 5:
		$Stage6Colour = "green";
		$Stage7Colour = "green";
		$Stage8Colour = "green";
		break;
	case 6:
		$Stage6Colour = "yellow";
		$Stage7Colour = "yellow";
		$Stage8Colour = "yellow";
		break;
	case 7:
		$Stage6Colour = "red";
		$Stage7Colour = "red";
		$Stage8Colour = "red";
		break;
	case 8:
		$Stage6Colour = "red";
		$Stage7Colour = "red";
		$Stage8Colour = "red";
		break;
	default:
		$Stage6Colour = "blue";
		$Stage7Colour = "blue";
		$Stage8Colour = "blue";
		break;
}
?>