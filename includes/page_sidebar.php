<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
			<li class="sidebar-toggler-wrapper">
				<img id="bigLogo" src="/assets/img/logo-big.png" style="background:#fff;" width="225"/>
			</li>
			<li class="sidebar-toggler-wrapper ">
				<img  id="smallLogo" class="hidden" src="/assets/img/TMF-Logo-sm.png" style="background:#fff;" />
			</li>
			<li class="sidebar-toggler-wrapper" onclick="swapLogo()">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler hidden-phone">
				</div>
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			</li>
			<li class="sidebar-search-wrapper">
				<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
				<form class="sidebar-search" action="extra_search.html" method="POST">
					<div class="form-container">
						<div class="input-box">
							<a href="javascript:;" class="remove">
							</a>
							<input type="text" placeholder="Search..."/>
							<input type="button" class="submit" value=" "/>
						</div>
					</div>
				</form>
				<!-- END RESPONSIVE QUICK SEARCH FORM -->
			</li>
			<li class="start<?php if($PageURL[0] == "dashboard") echo " active" ?>">
				
					<a href="/dashboard/">
						<i class="fa fa-dashboard"></i>
						<span class="title">
							Dashboard
						</span>
						<span class="arrow"></span>
					</a>					
					<ul class="sub-menu">
						<li class="<?php if($PageURL[0] == "dashboard" && $PageURL[1] == "globaltmfdash") echo "active" ?>">
							<a href="/dashboard/globaltmfdash/">
								<i class="fa fa-home"></i>
								Global TMF Dashboard
							</a>
						</li>
						<li class="<?php if($PageURL[0] == "dashboard" && $PageURL[1] == "globalclientdash") echo "active" ?>">
							<a href="/dashboard/globalclientdash/">
								<i class="fa fa-book"></i>
								Global Client Dashboard
							</a>
						</li>						
						<li class="<?php if($PageURL[0] == "dashboard" && $PageURL[1] == "localtmf") echo "active" ?>">
							<a href="/dashboard/localtmf/">
								<i class="fa fa-ticket"></i>
								Local TMF Dashboard
							</a>
						</li>
						<li class="<?php if($PageURL[0] == "dashboard" && $PageURL[1] == "localclient") echo "active" ?>">
							<a href="/dashboard/localclient/">
								<i class="fa fa-ticket"></i>
								Local Client Dashboard
							</a>
						</li>
					</ul>				
			</li>
			<li class="start<?php if($PageURL[0] == "mypayroll" || $PageURL[0] =="mytasks") echo " active" ?>">
				<a href="javascript:;">
					<i class="fa fa-money"></i>
					<span class="title">
						My Payroll
					</span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php if($PageURL[0] == "mypayroll" && $PageURL[1] == "") echo "active" ?>">
						<a href="/mypayroll/">
							<i class="fa fa-home"></i>
							My Payroll
						</a>
					</li>
					<li class="<?php if($PageURL[1] == "processing") echo "active" ?>">
						<a href="/mypayroll/">
							<i class="fa fa-book"></i>
							My Payroll Processing
						</a>
					</li>
					<li class="<?php if($PageURL[0] == "mytasks") echo "active" ?>">
						<a href="/mytasks/">
							<i class="fa fa-bell"></i>
							<span class="title">
								Task Manager
							</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="<?php if($PageURL[0] == "mytasks"&& $PageURL[1] == 'myassignedtasks') echo "active" ?>">
								<a href="/mytasks/myassignedtasks">
									<i class="fa fa-bell-o"></i>
									<small>My Assigned Tasks</small>
									<?php if($TodaysTasks > 0){?>
									<span class="badge badge-info">42</span>
									<?php }?>
								</a>
							</li>
                            <li class="<?php if($PageURL[0] == "mytasks"&& $PageURL[1] == "myofficetasks") echo "active" ?>">
                                <a href="/mytasks/myofficetasks">
                                    <i class="fa fa-bell-o"></i>
                                   <small> My Office Tasks</small>
                                    <?php if($TodaysTasks > 0){?>
                                        <span class="badge badge-info">140</span>
                                    <?php }?>
                                </a>
                            </li>
                            <li class="<?php if($PageURL[0] == "mytasks"&& $PageURL[1] == "mycalendar") echo "active" ?>">
								<a href="/mytasks/mycalendar">
									<i class="fa fa-bell-o"></i>
									<small>My Calendar</small>
									<?php if($OverdueTasks > 0){?>
									<span class="badge badge-warning">2</span>
									<?php }?>
								</a>
							</li>
						</ul>
					</li>
					<li class="<?php if($PageURL[1] == "mytickets") echo "active" ?>">
						<a href="/mytickets/">
							<i class="fa fa-ticket"></i>
							My Tickets
							<span class="badge badge-info">Update</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="start<?php if($PageURL[0] == "admin") echo " active" ?>">
				<a href="/admin/">
					<i class="fa fa-money"></i>
					<span class="title">
						Admin
					</span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php if($PageURL[0] == "admin" && $PageURL[1] == "payrollMasterData") echo "active" ?>">
						<a href="/admin/payrollMasterData/">
							<i class="fa fa-home"></i>
							Payroll Master Data
						</a>
					</li>
					<li class="<?php if($PageURL[0] == "admin" && $PageURL[1] == 'localOfficeManagement') echo "active" ?>">
						<a href="/admin/localOfficeManagement/">
							<i class="fa fa-book"></i>
							Local Office Management
						</a>
					</li>
					<li class="<?php if($PageURL[0] == "admin" && $PageURL[1] == 'localtmfsetup') echo "active" ?>">
						<a href="/admin/localtmfsetup/">
							<i class="fa fa-book"></i>
							Local TMF Setup
						</a>
					</li>
					<li class="<?php if($PageURL[0] == "admin" && $PageURL[1] == 'localclientwizard') echo "active" ?>">
						<a href="/admin/localclientwizard/">
							<i class="fa fa-book"></i>
							Client Management
						</a>
					</li>
					<li class="<?php if($PageURL[0] == "admin" && $PageURL[1] == 'countryManagement') echo "active" ?>">
						<a href="/admin/countryManagement/">
							<i class="fa fa-bell"></i>
							<span class="title">
								Country Management
							</span>
						</a>
					</li>
					<li class="<?php if($PageURL[0] == "admin" && $PageURL[1] == 'ticketManagement') echo "active" ?>">
						<a href="/admin/ticketManagement/">
							<i class="fa fa-ticket"></i>
							Ticket Management
						</a>
					</li>
                    <li class="<?php if($PageURL[0] == "admin" && $PageURL[1] == 'officeDetails') echo "active" ?>">
                        <a href="/admin/officeDetails/">
                            <i class="fa fa-building-o"></i>
                            About Us
                        </a>
                    </li>
				</ul>
			</li>
			<li class="<?php if($PageURL[0] == "reports") echo "active" ?>">
				<a href="/reports/">
					<i class="fa fa-bar-chart-o"></i>
					<span class="title">
						Reports
					</span>
					<span class="arrow ">
					</span>
                    <ul class="sub-menu">
                        <li class="<?php if($PageURL[0] == "reports" && $PageURL[1] == "grossToNet") echo "active" ?>">
                            <a href="/reports/grossToNet/">
                                <i class="fa fa-bar-chart-o"></i>
                                Gross To Net
                            </a>
                        </li>
                        <li class="<?php if($PageURL[0] == "reports" && $PageURL[1] == "grossAnalysis") echo "active" ?>">
                            <a href="/reports/grossAnalysis/">
                                <i class="fa fa-bar-chart-o"></i>
                                Gross Analysis
                            </a>
                        </li>
                        <li class="<?php if($PageURL[0] == "reports" && $PageURL[1] == "headcount") echo "active" ?>">
                            <a href="/reports/headcount/">
                                <i class="fa fa-bar-chart-o"></i>
                                Headcount
                            </a>
                        </li>
                        <li class="<?php if($PageURL[0] == "reports" && $PageURL[1] == "headcountStats") echo "active" ?>">
                            <a href="/reports/headcountStats/">
                                <i class="fa fa-bar-chart-o"></i>
                                Headcount Stats
                            </a>
                        </li>
                        <li class="<?php if($PageURL[0] == "reports" && $PageURL[1] == "variance") echo "active" ?>">
                            <a href="/reports/variance/">
                                <i class="fa fa-bar-chart-o"></i>
                                Variance
                            </a>
                        </li>
                        <li class="<?php if($PageURL[0] == "reports" && $PageURL[1] == "export") echo "active" ?>">
                            <a href="/reports/export/">
                                <i class="fa fa-download"></i>
                                Export Data
                            </a>
                        </li>
                    </ul>
				</a>
			</li>
			<li class="<?php if($PageURL[0] == "reports") echo "active" ?>">
				<a href="/selfservicemanager/">
					<i class="fa fa-sitemap"></i>
					<span class="title">
						Self Service Manager
					</span>
					<span class="arrow ">
					</span>
					<ul class="sub-menu">
						<li class="<?php if($PageURL[0] == "selfservicemanager" && $PageURL[1] == "register") echo "active" ?>">
							<a href="/selfservicemanager/register/">
								<i class="fa fa-home"></i>
								Register
							</a>
						</li>
					</ul>
				</a>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<script>
function swapLogo(){
$('#bigLogo').toggleClass('hidden');
$('#smallLogo').toggleClass('hidden');

}

</script>
<!-- END SIDEBAR -->