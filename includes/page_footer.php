<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
		 2014 &copy; eDocs. (<?php echo $_SESSION["user"];?>)
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>

<?php if($UploadFlag) { ?>

<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
	<div class="slides">
	</div>
	<h3 class="title"></h3>
	<a class="prev">
		 ‹
	</a>
	<a class="next">
		 ›
	</a>
	<a class="close white">
	</a>
	<a class="play-pause">
	</a>
	<ol class="indicator">
	</ol>
</div>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-upload fade">
                <td>
                    <span class="preview"></span>
                </td>
                <td>
                    <p class="name">{%=file.name%}</p>
                    {% if (file.error) { %}
                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                    {% } %}
                </td>
                <td>
                    <p class="size">{%=o.formatFileSize(file.size)%}</p>
                    {% if (!o.files.error) { %}
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                    {% } %}
                </td>
                <td>
                	<?php if($Stage1 == 1) { ?>
              		{% if (!o.files.error && !i && !o.options.autoUpload) { %}
                        <select id="payroll_type" class="form-control" name="payroll_type">
							<option name="payroll_variations" value="">Select File Type</option>
                        	<option name="payroll_variations" value="Payroll Variations">Payroll Variations</option>
                        	<option name="starters" value="Starters">Starters</option>
                        	<option name="leavers" value="Leavers">Leavers</option>
                        	<option name="ss_changes" value="Master Data Changes">Master Data Changes</option>
                        </select>
                    {% } %}
                    <?php } ?>
					<?php if($Stage4 == 3) { ?>
              		{% if (!o.files.error && !i && !o.options.autoUpload) { %}
                        <select id="payroll_type" class="form-control" name="filetype">
							<option value="gross to net">Select File Type</option>
                        	<option value="gross to net">Gross to Net</option>
                        	<option value="journal">Journal</option>
                        	<option value="payslips">Payslips</option>
                        	<option value="global reporting">Global Reporting</option>
                        </select>
                    {% } %}
                    <?php } ?>
                    {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                        <button  class="btn blue start btn-sm " >
                            <i class="fa fa-upload"></i>
                            <span>Start</span>
                        </button>
                    {% } %}
                    {% if (!i) { %}
                        <button class="btn red cancel btn-sm">
                            <i class="fa fa-ban"></i>
                            <span>Cancel</span>
                        </button>
                    {% } %}
                </td>
            </tr>
        {% } %}
				
    </script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-download fade">
                <td>
                    <span class="preview">
                        {% if (file.thumbnailUrl) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                        {% } %}
                    </span>
                </td>
                <td>
                    <p class="name">
                        {% if (file.url) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                        {% } else { %}
                            <span>{%=file.name%}</span>
                        {% } %}
                    </p>
                    {% if (file.error) { %}
                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                    {% } %}
                </td>
                <td>
                    <span class="size">{%=o.formatFileSize(file.size)%}</span>
                </td>
                <td>
                    {% if (file.deleteUrl) { %}
                    {% } else { %}
                        <button class="btn yellow cancel btn-sm">
                            <i class="fa fa-ban"></i>
                            <span>Cancel</span>
                        </button>
                    {% } %}
                </td>
            </tr>
        {% } %}
    </script>

<?php } ?>

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/assets/plugins/respond.min.js"></script>
<script src="/assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/assets/plugins/jquery-notific8/jquery.notific8.min.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php if($maps ){ ?>
    <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
    <script src="/assets/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
    <script src="/assets/scripts/custom/maps-google.js" type="text/javascript"></script>

<?php }?>
<?php if($tree ){ ?>
    <script src="/assets/plugins/jstree/dist/jstree.min.js"></script>
    <script src="/assets/scripts/custom/ui-tree.js"></script>
<?php }?>



<?php if($dataTables ){ ?>
<script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="/assets/plugins/data-tables/colVis.js"></script>
<script type="text/javascript" src="/assets/plugins/data-tables/TableTools.js"></script>
<script type="text/javascript" src="/assets/plugins/data-tables/ZeroClipboard.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>

<?php }?>

<?php if($grossToNet ) {?>
    <script type="text/javascript" src="/assets/plugins/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/grossToNet.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/grossToNetChart.js"></script>
<?php }?>

<?php if($grossAnalysis ) {?>
    <script type="text/javascript" src="/assets/plugins/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/grossAnalysis.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/grossAnalysisChart.js"></script>
<?php }?>

<?php if($variance ) {?>
    <script type="text/javascript" src="/assets/plugins/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/variance.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/varianceChart.js"></script>
<?php }?>

<?php if($headcount ) {?>
    <script type="text/javascript" src="/assets/plugins/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/headcount.js"></script>
<?php }?>

<?php if($headcountStats ) {?>
    <script type="text/javascript" src="/assets/plugins/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="/assets/scripts/custom/headcountStats.js"></script>
<?php }?>


<?php if($datePicker) { ?>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/assets/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


<?php }?>
<?php if($datePaginate){?>
<script src="/assets/plugins/moment.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-datepaginator/bootstrap-datepaginator.min.js" type="text/javascript"></script>
<?php }?>

<?php if($formValidation) { ?>
<!-- <script type="text/javascript" src="/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="/assets/plugins/bootstrap-markdown/lib/markdown.js"></script> -->
<?php }?>
<?php if($gClientFlag){ ?>
<script type="text/javascript" src="/assets/plugins/jqvmap/jqvmap/jquery.vmap.js" ></script>
<script type="text/javascript" src="/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" ></script>
<script type="text/javascript" src="/assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" ></script>
<script type="text/javascript" src="/assets/plugins/jquery-jvectormap/jquery-jvectormap-1.1.1.min.js" ></script>
<script type="text/javascript" src="/assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js" ></script>
<script type="text/javascript" src="/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.categories.min.js" ></script>
<script src="/assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

<?php } ?>
<?php if($localClient) {?>
<script src="/assets/scripts/custom/tasks.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.crosshair.min.js"></script>
<?php } ?>

<?php if($localTmf) {?>
<script src="/assets/scripts/custom/tasks.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.crosshair.min.js"></script>
<?php } ?>

<?php if($vmapFlag) {?>
<script type="text/javascript" src="/assets/plugins/jqvmap/jqvmap/jquery.vmap.js" ></script> 
<script type="text/javascript" src="/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" ></script>
<script type="text/javascript" src="/assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" ></script>
<script type="text/javascript" src="/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.js"></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.categories.min.js" ></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.threshold.min.js" ></script>
<script type="text/javascript" src="/assets/plugins/flot/jquery.flot.animator.js" ></script>
<?php } ?>
<?php if($CalendarFlag) { ?>
<script type="text/javascript" src="/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<?php } ?>

<?php if($SortTableFlag) { ?>
<script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
<?php } ?>
<script type="text/javascript" src="/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<?php if($WizardFlag) { ?>
<script type="text/javascript" src="/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="/assets/plugins/jquery-radiobutton-2.0.js" type="text/javascript"></script>
<?php } ?>
<?php if($UploadFlag) { ?>
<!-- Upload PLUGINS -->
<script src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/vendor/tmpl.min.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/vendor/load-image.min.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
<script src="/assets/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<script src="/assets/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
<?php } ?>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script  src="/assets/scripts/core/app.js"></script>
<script src="/assets/scripts/custom/payroll-notific8.js"></script>

<?php if ($export) { ?>
    <script src="/assets/scripts/custom/export.js"></script>
<?php } ?>


<?php if ($formValidation) { ?>
<script src="/assets/scripts/custom/form-validation.js"></script>
<?php } ?>

<?php if($localClient){ ?>
<script src="/assets/scripts/custom/calendar_localtmf.js" type="text/javascript"></script>
<script src="/assets/scripts/custom/barcharts.js"></script>
<?php } ?>

<?php if($localClientWizard) { ?>
<script src="/assets/scripts/custom/client_form_wizard.js" type="text/javascript"></script>
<?php } ?>

<?php if($localTmf){ ?>
<script src="/assets/scripts/custom/calendar_localtmf.js" type="text/javascript"></script>
<script src="/assets/scripts/custom/barcharts.js"></script>
<?php } ?>

<?php if($gClientFlag){ ?>
<script src="/assets/scripts/custom/maps-vector-gclient.js"></script>
<script src="/assets/scripts/custom/easy-pie-chart.js"></script>
<script src="/assets/scripts/custom/piecharts.js"></script>
<script src="/assets/scripts/custom/barcharts.js"></script>
<script src="/assets/scripts/custom/minicharts.js"></script>
<?php } ?>
<?php if($vmapFlag){?>
<script src="/assets/scripts/custom/maps-vector.js"></script>
<script src="/assets/scripts/custom/barcharts.js"></script>
<script src="/assets/scripts/custom/easy-pie-chart.js"></script>
<?php } ?>

<?php if($CalendarFlag) { ?>
<script type="text/javascript" src="/assets/scripts/custom/calendar.js"></script>
<?php } ?>

<?php if($WizardFlag) { ?>
<script src="/assets/scripts/custom/form-wizard.js"></script>
<script src="/assets/plugins/bootstrap-switch/js/bootstrap-switch.js"></script>
<script src="/assets/scripts/custom/serialize-checkboxes.js" type="text/javascript"></script>
<?php } ?>

<?php if($datePaginate) {?>
<script src="/assets/scripts/custom/ui-datepaginator.js"></script>
<?php }?>

<?php if($EditableTableFlag) { ?>
<script src="/assets/scripts/custom/table-editable.js"></script>
<?php } ?>

<?php if($UploadFlag) { ?>
<script src="/assets/scripts/custom/form-fileupload.js"></script>
<script src="/assets/scripts/custom/payroll_client_upload.js"></script>
<script src="/assets/scripts/custom/final_output_upload.js"></script>
<?php } ?>

<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   App.init();
   UINotific8.init();
   $('#bigLogo').removeClass('hidden');
   $('#smallLogo').addClass('hidden');

    <?php if($maps) {?>
        MapsGoogle.init();
    <?php }?>
    <?php if($tree) {?>
        MapsGoogle.init();
        UITree.init();
    <?php }?>


   	<?php if($timeLineFlag) { ?>
		$('.time-line-control').on("click", function(e){
			e.preventDefault();
			$("i",this).toggleClass("icon-circle-arrow-up icon-circle-arrow-down");
			$(this).toggleClass("fa fa-chevron-down fa fa-chevron-up");
			$(this).next('.timeline-content').slideToggle(1000);
		});
   <?php } ?>

   <?php if($payrollFlag) {?>   
   
   function showProceed(){
	$('#proceedButton').show();
   };
   <?php }?>
   <?php if($datePicker) {?>
   $('#dp1').datepicker({
				format: 'mm-dd-yyyy'
			}).on('changeDate', function(ev){
						
			 $('#dates').append('<div class="form-group"><label class=\"control-label col-md-3\">'+ ev.date.toDateString()+'</label><div class=\"col-md-4\"><input type=\"text\" class="form-control\" name=\"public_holiday_names[]\" ><span class=\"help-block\">Please name your holiday</span></div></div><input type=\"hidden" name="public_holiday_value[]" value=\"'+ ev.date.toLocaleDateString()+ '"/>');
			}); 
			
		
   <?php } ?>
   <?php if($datePaginate) {?>
   UIDatepaginator.init();
   <?php }?>
   <?php if($formValidation) {?>
	$('#platform_field').focusout(function(){
		var co = document.getElementById('officecountry');
		var country = co.value;
		
		var local_office_name = document.getElementById('local_office_name');
		local_office_name.value = country+'_'+this.value;
	
	});

    FormValidation.init();
	
   <?php }?>

   <?php if($vmapFlag) { ?>
   

	jQuery('#showMore').click(function(){
		jQuery('#show').fadeToggle('fast', showDiv);
		function showDiv(){
			jQuery('#hidden').fadeToggle('fast');
			jQuery('#hidden').removeClass('hidden');
			}
		
	});
	
	pieChartNormal();
	pieChartRed();
	initBarChart1();
	initBarChart2(null, null, null);
	MapsVector.init();

  
   <?php } ?> 
   
   <?php if($updatePayslips = true) {?>
	$('#verifyYes').on('click', function(){
		var request = $.ajax({
			type: "POST",
			url: "updatePayrollStatus.php"
		});
	});
   <?php }?>
   
   <?php if($localTmf) { ?>
	Tasks.initDashboardWidget();
	Calendar.init();
	accuracySLA();
	<?php } ?>
	
	<?php if($localClient) { ?>
	accuracyClient();
	<?php } ?>
   
	<?php if($gClientFlag) { ?>
	initMap();
	pieChartNormal();
	pieChartRed();
	initPieCharts();
	initBarChart2();
	//payrollCosts();
	genderProfile = [		
		[48,52],
		[59,41],
		[52,48],
		[22,10]];
	genderObj ={
					type: 'pie',
					width: '80',
					height: '80',
					sliceColors: ['#27A9E3','#852B99']
				}
	miniCharts(genderProfile, genderObj);
	<?php } ?>
	
   
   <?php if($CalendarFlag) { ?>
   Calendar.init();
   <?php } ?>
   
   <?php if($WizardFlag) { ?>
	FormWizard.init();
	
	// hide the submit button on the form wizard
	$('#form_wizard_1').find('.button-submit').hide();
	
	
	// submit the details of the 
	$('#createLocalTmf').on('submit', function(event){
	
	formData = $(this).serialize({checkboxesAsBools: true});
	$.ajax({
		url: window.location.origin+ "/admin/localtmfsetup/local_tmf_setup.php",
		type: "POST",
		data: formData,
		async: false,
		success: function(resp){			
			console.log(resp);
			$('#form_success').modal();
		},
		error: function(resp){
			console.log(resp);
			alert(resp);
		}
	});
	event.preventDefault();
	
	});
	
	var options = {
		onText: "YES",
		offText: "NO",
		onColor: 'primary',
		offColor: 'danger',
		state:false,
		onSwitchChange: function(event, state){
			this.value = "No";
		}
	};
	
	
	
	$(".radio").bootstrapSwitch(options);

	
   <?php } ?>
   
   <?php if($localClientWizard) {?>
   ClientFormWizard.init();
  
  $(function(){
    var $select = $(".1-100");
    for (i=1;i<=10;i++){
        $select.append($('<option></option>').val(i).html(i));
		
		};
		$select.prepend($('<option></option>').html('please select'));
	});
	
		
	$('#no_client_payrolls').on('change', function(){
		$('#client_payroll_names').html('');
		for(var i = 0; i< this.value; i++){
			$('#client_payroll_names').append('<div class=\"form-group no-margin form-actions fluid\">'+
													'<h4><label class=\"control-label col-md-1 title-label\">'+(i+1).toString()+'</label></h4>'+
													'<div class=\"col-md-2\">'+
														'<input type=\"text\" class=\"form-control entity_name\" name=\"entity_name[]\"/>'+
														'<span class=\"help-block\"> Entity Payroll Name</span>'+
													'</div>'+
													'<div class=\"col-md-2\">'+
														'<select type=\"text\" class=\"form-control\" name=\"country_iso[]\">'+
															'<option value=\"IE\">IE</option><option value=\"AE\">AE</option>'+
															'<option value=\"UK\">UK</option>'+
															'<option value=\"US\">US</option>'+
														'</select>'+
														'<span class=\"help-block\"> Country ISO</span>'+
													'</div>'+
													'<div class=\"col-md-2\">'+
														'<select type=\"text\" class=\"form-control\" name=\"local_tmf_office[]\">'+
															'<option value=\"IE_Platform\">IE_Platform</option>'+
															'<option value=\"AE_Platform\">AE_Platform</option>'+
															'<option value=\"SG_Platform\">SG_Platform</option>'+
															'<option value=\"US_Platform\">US_Platform</option>'+
														'</select>'+
														'<span class=\"help-block\"> Assigned Local TMF office</span>'+
													'</div>'+
													'<div class=\"col-md-2 switch\">'+
														'<input type=\"checkbox\" name=\"notification\">'+
														'<span class=\"help-block\"> Client Query Notification</span>'+
													'</div>'+
													'<div class=\"col-md-2 switch\">'+
														'<input type=\"checkbox\" name=\"twoStep\">'+
														'<span class=\"help-block\"> Two Step approval</span>'+
													'</div>'+
												'</div>');

		}
		var options = {
			onText: "ON",
			offText: "OFF",
			onColor: 'primary',
			offColor: 'danger'
		};
		
		$("[name='notification']").bootstrapSwitch(options);
		$("[name='twoStep']").bootstrapSwitch(options);
		//$("input[type='checkbox']").bootstrapSwitch(options);
	
	});
	

	$('body').on('change', '.entity_name', function(){
	
		// add in the calendars for each entity
		var clone = $('.weekly-tmf-ooo').clone();
		clone.removeClass('hidden');
		
		var cloneTmfOutOffice = $('#tmf-out-of-office-days').clone();
		cloneTmfOutOffice.removeClass('hidden');
		
		entityName = this.value;	
		var x = $('#calendar_entity').append( function(){
										$('#calendar_entity').append(
										'<div class=\"form-group no-margin form-actions fluid\">'+										
										'<div class="col-md-6 out_of_office_date_select">'+
										'<h4 class=\"block\">'+entityName+' out of office holidays</h4>'+
											'<label class=\"control-label col-md-3 title-label\">'+entityName+'</label>'+
											'<div class=\"cancel input-group col-md-6 \">'+
												'<span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>'+
												'<input class=\"col-md-8 cancel date form-control \" placeholder=\"select holiday dates\" name=\"'+this.value+'_dates[]\">'+
											'</div>'+
											'</div><div class="col-md-6 tmf-ooo">'+
											'<div class=\"row\">'+cloneTmfOutOffice[0].innerHTML+'</div><div class=\"row\">'+clone[0].innerHTML+'</div></div>'+
										'</div>');
										return this;});
										
		// initialize the bootstrap switch for each of weekly out of office days
		var weekdayOptions = {
			onText: "ON",
			offText: "OFF",
			onColor: 'primary',
			offColor: 'danger',
			size: 'mini'
		};

		$(x).children().last().find('[name="weekly_out_of_office"]').bootstrapSwitch(weekdayOptions);
	
		$('.date').last().datepicker({
			format: "dd/mm/yyyy",
			multidate:true,
			multidateSeperator: ","			
		}).on('changeDate', function(e){
			$(this).closest('.out_of_office_date_select').append('<label class=\"control-label col-md-3 label-small\">'+e.date.toDateString()+'</label><div class=\" col-md-6 input-group\"><input class=\"form-control\" type=\"text\" placeholder=\"out of office name\" name=\"out_of_office_name[]\"><span class=\"help-block\"> Out of Office Name</span></div>');		
		});
		
		$('.date').validate().cancelSubmit = true;
		$('#pay_day_rules').append("<div class=\"form-group no-margin form-actions fluid\">"+
										'<label class=\"control-label col-md-2 title-label\">'+this.value+'</label>'+
										'<div class=\"col-md-3\">'+
											'<select type=\"text\" class=\"form-control\" name=\"payroll_frequency[]\">'+
												'<option value=\"biweekly\">Select Pay Frequency</option>'+
												'<option value=\"weekly\">Weekly</option>'+
												'<option value=\"monthly\">Monthly</option>'+
											'</select>'+
											'<span class=\"help-block\"> Pay Frequency</span>'+
										'</div><div class=\"pay_frequency\"></div></div>');

		$('#pay_process_days').append('<div class=\"form-group no-margin form-actions fluid\">'+
											'<label class=\"control-label col-md-2 title-label\">'+this.value+'</label>'+
											'<div class=\"col-md-1\">'+
												'<input type=\"text\" class=\"form-control\" name=\"draft_output[]\" placeholder=\"-6\" >'+
												'<span class=\"help-block\">Inputting</span>'+
											'</div>'+
											'<div class=\"col-md-1">'+
												'<input type=\"text\" class=\"form-control\" name=\"draft_approval[]\" placeholder=\"-5\" >'+
												'<span class=\"help-block\">Processing</span>'+
											'</div>'+
											'<div class=\"col-md-1">'+
												'<input type=\"text\" class=\"form-control\" name=\"final_output[]\" placeholder=\"-4\" >'+
												'<span class=\"help-block\">Approval</span>'+
											'</div>'+
											'<div class=\"col-md-1">'+
												'<input type=\"text\" class=\"form-control\" name=\"final_approval[]\" placeholder=\"-3\" >'+
												'<span class=\"help-block\">Finalising</span>'+
											'</div>'+
											'<div class=\"col-md-1">'+
												'<input type=\"text\" class=\"form-control\" name=\"pay_day[]\" placeholder=\"0\" >'+
												'<span class=\"help-block\">Pay Day</span>'+
											'</div>'+
											'<div class=\"col-md-1">'+
												'<input type=\"text\" class=\"form-control\" name=\"pay_day[]\" placeholder=\"0\" >'+
												'<span class=\"help-block\">Report Release Day</span>'+
											'</div>'+
											'<div class=\"col-md-2">'+
												'<input type=\"text\" class=\"form-control\" name=\"pay_slip_day[]\" placeholder=\"day 0\" >'+
												'<span class=\"help-block\">Payslip Release</span>'+
											'</div>'+
										'</div>');
										
		$('#optional_tasks').append('<div class=\"form-group no-margin form-actions fluid tasks\">'+
										'<div class=\"row task-names\">'+
											'<label class=\"control-label col-md-2 title-label\">'+this.value+'</label>'+
											'<div class=\"col-md-3\">'+
												'<input type=\"text\" class=\"form-control\" name=\"'+this.value+'taskName[]\" placeholder=\"Name\" >'+
												'<span class=\"help-block\">Task Name</span>'+
											'</div>'+
											'<div class=\"col-md-3\">'+
												'<select type=\"text\" class=\"form-control\" name=\"'+this.value+'action_to[]\" >'+
													'<option value=\"\">Select Action to</option>'+
													'<option value=\"Client\">Client</option>'+
													'<option value=\"TMF\">TMF</option>'+
												'</select>'+
												'<span class=\"help-block\">Action To</span>'+
											'</div>'+
											'<div class=\"col-md-3\">'+
												'<select type=\"text\" class=\"form-control\" name=\"'+this.value+'task_day[]\" >'+
													'<option value=\"\">Select Task Day</option>'+
													'<option value=\"+6\">+6</option>'+
													'<option value=\"+5\">+5</option>'+
													'<option value=\"+4\">+4</option>'+
													'<option value=\"+3\">+3</option>'+
													'<option value=\"+2\">+2</option>'+
													'<option value=\"+1\">+1</option>'+
													'<option value=\"0\">0</option>'+	
													'<option value=\"-1\">-1</option>'+
													'<option value=\"-2\">-2</option>'+
													'<option value=\"-3\">-3</option>'+
													'<option value=\"-4\">-4</option>'+
													'<option value=\"-5\">-5</option>'+
													'<option value=\"-6\">-6</option>'+												
												'</select>'+
												'<span class=\"help-block\">Day of Task</span>'+
											'</div>'+
										'</div>'+
										'<div class=\"row\"><div class=\"col-md-8\"></div><div class=\"col-md-3\"><button type=\"button\" class=\"btn btn-primary task-add\">Add Another Task</button></div></div>'+
									'</div>');
	});
	// add the ability to add further tasks to the individual entities
	$('body').on('click', '.task-add', function(){
		$(this).closest('.form-group').children('.task-names').append('<label class=\"control-label col-md-2 title-label\">'+this.value+'</label>'+
										'<div class=\"col-md-3\">'+
											'<input type=\"text\" class=\"form-control\" name=\"'+this.value+'taskName[]\" placeholder=\"Name\" >'+
											'<span class=\"help-block\">Task Name</span>'+
										'</div>'+
										'<div class=\"col-md-3\">'+
											'<select type=\"text\" class=\"form-control\" name=\"'+this.value+'action_to[]\" >'+
												'<option value=\"\">Select Action to</option>'+
												'<option value=\"Client\">Client</option>'+
												'<option value=\"TMF\">TMF</option>'+
											'</select>'+
											'<span class=\"help-block\">Action To</span>'+
										'</div>'+
										'<div class=\"col-md-3\">'+
											'<select type=\"text\" class=\"form-control\" name=\"'+this.value+'task_day[]\" >'+
												'<option value=\"\">Select Task Day</option>'+
												'<option value=\"-1\">-1</option>'+
												'<option value=\"-2\">-2</option>'+
												'<option value=\"-3\">-3</option>'+
												'<option value=\"-4\">-4</option>'+
												'<option value=\"-5\">-5</option>'+
												'<option value=\"-6\">-6</option>'+												
											'</select>'+
											'<span class=\"help-block\">Action To</span>'+
										'</div>');
	
	});
	
	$('body').on('change', 'select[name="pay_day_frequency[]"]', function(){
		if($(this).val() == "dayx"){
			$(this).closest('.col-md-2').append(
															'<select type=\"text\" class=\"form-control\" name=\"dayx_select\">'+
																'<option value=\"1\">1</option>'+
																'<option value=\"2\">2</option>'+
																'<option value=\"3\">3</option>'+
																'<option value=\"4\">4</option>'+
																'<option value=\"5\">5</option>'+
																'<option value=\"6\">6</option>'+
																'<option value=\"7\">7</option>'+
																'<option value=\"8\">8</option>'+
																'<option value=\"9\">9</option>'+
																'<option value=\"10\">10</option>'+
																'<option value=\"11\">11</option>'+
																'<option value=\"12\">12</option>'+
																'<option value=\"13\">13</option>'+
																'<option value=\"14\">14</option>'+
																'<option value=\"15\">15</option>'+
																'<option value=\"16\">16</option>'+
																'<option value=\"17\">17</option>'+
																'<option value=\"18\">18</option>'+
																'<option value=\"18\">19</option>'+
																'<option value=\"20\">20</option>'+
																'<option value=\"21\">21</option>'+
																'<option value=\"22\">22</option>'+
																'<option value=\"23\">23</option>'+
																'<option value=\"24\">24</option>'+
																'<option value=\"25\">25</option>'+
																'<option value=\"26\">26</option>'+
																'<option value=\"27\">27</option>'+
																'<option value=\"28\">28</option>'+
															'</select>'+
															'<span class=\"help-block\"> Day X of Month</span>');
		};	
	});
	
	$('body').on('change', "select[name='payroll_frequency[]']", function(){
		if($(this).val() == "weekly"){
			$(this).closest('.form-group').children('.pay_frequency').html('<div class=\" col-md-2\">'+
																				'<select type=\"text\" class=\"form-control\" name=\"pay_day[]\">'+
																					'<option value=\"Day 1 of Week\">Day 1 of Week</option>'+
																					'<option value=\"last day of Week\">Last day of Week</option>'+
																					'<option value=\"Monday\">Monday</option>'+
																					'<option value=\"Tuesday\">Tuesday</option>'+
																					'<option value=\"Wednesday\">Wednesday</option>'+
																					'<option value=\"Thursday\">Thursday</option>'+
																					'<option value=\"Friday\">Friday</option>'+
																					'<option value=\"Saturday\">Saturday</option>'+
																					'<option value=\"Sunday\">Sunday</option>'+
																				'</select><span class=\"help-block\">Pay day</span></div>'+
																				'<div class=\"col-md-2\">'+
                                                                                    '<select type=\"text\" class=\"form-control\" name=\"bh_exception[]\">'+
                                                                                            '<option value=\"1 day before\">1 day before</option>'+
                                                                                            '<option value=\"2 day before\">2 days before</option>'+
                                                                                            '<option value=\"3 day before\">3 days before</option>'+
                                                                                            '<option value=\"4 days before\">4 days before</option>'+
                                                                                     '</select><span class=\"help-block\">BH exception</span></div>');
		}
		if($(this).val() == "monthly"){
			$(this).closest('.form-group').children('.pay_frequency').html('<div class=\" col-md-2\">'+
																				'<select type=\"text\" class=\"form-control\" name=\"pay_day_frequency[]\">'+
																					'<option value=\"Day 1 of Month\">Day 1 of Month</option>'+
																					'<option value=\"last day of Month\">Last day of Month</option>'+
																					'<option value=\"dayx\">Day X of Month</option>'+
																				'</select><span class=\"help-block\">Pay day</span></div>'+
																				'<div class=\"col-md-2\"><select type=\"text\" class=\"form-control\" name=\"bh_exception[]\"><option value=\"1 day before\">1 day before</option><option value=\"2 day before\">2 days before</option><option value=\"3 day before\">3 days before</option><option value=\"4 days before\">4 days before</option></select><span class=\"help-block\">BH exception</span></div>');
		
		}	
	});

   <?php } ?>
   
   <?php if($UploadFlag) { ?>
   FormFileUpload.init();
		$( "#verify" ).on( "click", function() {
		alert( $( this ).text() );
	});
		
   <?php } ?>
   
   <?php if($SortTableFlag) { ?>
    var table;
    payrollTable =  $('#SortTable').dataTable({
	    "aLengthMenu": [
	        [5, 15, 20, -1],
	        [5, 15, 20, "All"] // change per page values here
	    ],
        "bFilter" : false,
        // set the initial value
        "iDisplayLength": 10,
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });
    writeSelectHeadings(payrollTable);

    function writeSelectHeadings(t){
        var api = t.api();
        var oSettings = t.fnSettings();
        var col_id = oSettings.aoColumns;
        var selectBoxes = $('<div id="selectBoxes" class="block-inline pull-left"></div>').prependTo($('#SortTable_length'));

        var select = $('<select id="regionSelect" class="multiselect" data-role="multiselect" title="Select Region" data-name="Select Region"></select>')
            .appendTo($('#selectBoxes'));

        select.append('<option value="AMERICAS">Open</option>'+
            '<option value="EMEA">Closed</option>'+
            '<option value="BENELUX">Overdue</option>');

        var selectCountry = $('<select id="countrySelect" class="multiselect" data-role="multiselect"  title="Select Country" data-name="Select Country"></select>')
            .appendTo($('#selectBoxes'));
        selectCountry.append(' <option value="UK">Ireland</option>'+
            '<option value="CN">UK</option>'+
            '<option value="IE">NL</option>'+
            '<option value="SG">SG</option>'+
            '<option value="US">US</option>');


    };
   <?php } ?>

   
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->

</body>
<!-- END BODY -->
</html>