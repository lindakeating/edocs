<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="/assets/img/menu-toggler.png" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="nav navbar-nav pull-right">
			<!-- BEGIN NOTIFICATION DROPDOWN -->
			<li class="dropdown" id="header_notification_bar">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fa fa-warning"></i>
					<span class="badge">
						 6
					</span>
				</a>
				<ul class="dropdown-menu extended notification">
					<li>
						<p>
							 You have 14 new notifications
						</p>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" style="height: 250px;">
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-success">
										<i class="fa fa-plus"></i>
									</span>
									 Heat Ltd approved draft payroll today
									<span class="time">
										 Just now
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-danger">
										<i class="fa fa-bolt"></i>
									</span>
									 Metal Ltd raised a ticket today
									<span class="time">
										 15 mins
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
									</span>
									 Ice Ltd raised a ticket today
									<span class="time">
										 22 mins
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-info">
										<i class="fa fa-bullhorn"></i>
									</span>
									 May Moore submitted Ice Draft payroll for internal approval
									<span class="time">
										 40 mins
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-danger">
										<i class="fa fa-bolt"></i>
									</span>
									 May Moore submitted final output reports for Metal Ltd
									<span class="time">
										 2 hrs
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-danger">
										<i class="fa fa-bolt"></i>
									</span>
									James Lyons rejected Warm Ltd Draft payroll
									<span class="time">
										 5 hrs
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
									</span>
									 Ice Ltd Payroll rejected (250 employees)
									<span class="time">
										 45 mins
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-info">
										<i class="fa fa-bullhorn"></i>
									</span>
									 Metal Ltd registered 6 new employees
									<span class="time">
										 55 mins
									</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="label label-sm label-icon label-danger">
										<i class="fa fa-bolt"></i>
									</span>
									 Ice Ltd submitted payroll for approval
									<span class="time">
										 2 hrs
									</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="external">
						<a href="#">
							 See all notifications <i class="m-icon-swapright"></i>
						</a>
					</li>
				</ul>
			</li>
			<!-- END NOTIFICATION DROPDOWN -->
			<!-- BEGIN INBOX DROPDOWN -->
			<li class="dropdown" id="header_inbox_bar">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fa fa-envelope"></i>
					<span class="badge">
						 5
					</span>
				</a>
				<ul class="dropdown-menu extended inbox">
					<li>
						<p>
							 You have 12 new messages
						</p>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" style="height: 250px;">
							<li>
								<a href="inbox.html?a=view">
									<span class="photo">
										<img src="/assets/img/avatar2.jpg" alt=""/>
									</span>
									<span class="subject">
										<span class="from">
											 Lisa Wong
										</span>
										<span class="time">
											 Just Now
										</span>
									</span>
									<span class="message">
										 Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh...
									</span>
								</a>
							</li>
							<li>
								<a href="inbox.html?a=view">
									<span class="photo">
										<img src="/assets/img/avatar3.jpg" alt=""/>
									</span>
									<span class="subject">
										<span class="from">
											 Richard Doe
										</span>
										<span class="time">
											 16 mins
										</span>
									</span>
									<span class="message">
										 Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh...
									</span>
								</a>
							</li>
							<li>
								<a href="inbox.html?a=view">
									<span class="photo">
										<img src="/assets/img/avatar1.jpg" alt=""/>
									</span>
									<span class="subject">
										<span class="from">
											 Bob Nilson
										</span>
										<span class="time">
											 2 hrs
										</span>
									</span>
									<span class="message">
										 Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh...
									</span>
								</a>
							</li>
							<li>
								<a href="inbox.html?a=view">
									<span class="photo">
										<img src="/assets/img/avatar2.jpg" alt=""/>
									</span>
									<span class="subject">
										<span class="from">
											 Lisa Wong
										</span>
										<span class="time">
											 40 mins
										</span>
									</span>
									<span class="message">
										 Vivamus sed auctor 40% nibh congue nibh...
									</span>
								</a>
							</li>
							<li>
								<a href="inbox.html?a=view">
									<span class="photo">
										<img src="/assets/img/avatar3.jpg" alt=""/>
									</span>
									<span class="subject">
										<span class="from">
											 Richard Doe
										</span>
										<span class="time">
											 46 mins
										</span>
									</span>
									<span class="message">
										 Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh...
									</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="external">
						<a href="inbox.html">
							 See all messages <i class="m-icon-swapright"></i>
						</a>
					</li>
				</ul>
			</li>
			<!-- END INBOX DROPDOWN -->
			<!-- BEGIN TODO DROPDOWN -->
			<li class="dropdown" id="header_task_bar">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fa fa-tasks"></i>
					<span class="badge">
						 3
					</span>
				</a>
				<ul class="dropdown-menu extended tasks">
					<li>
						<p>
							 You have 3 open payrolls
						</p>
					</li>
					<li>
						<ul class="dropdown-menu-list scroller" style="height: 250px;">
							<li>
								<a href="/mypayroll/processing/1/">
									<span class="task">
										<span class="desc">
											 Frozen Limited	/ Ice Limited
										</span>
										<span class="percent">
											 100%
										</span>
									</span>
									<span class="progress">
										<span style="width: 100%;" class="progress-bar progress-bar-success" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">
												 100% Complete
											</span>
										</span>
									</span>
								</a>
							</li>
							<li>
								<a href="/mypayroll/processing/1/">
									<span class="task">
										<span class="desc">
											 Frozen Limited	/ Frost Limited
										</span>
										<span class="percent">
											 88%
										</span>
									</span>
									<span class="progress progress-striped">
										<span style="width: 88%;" class="progress-bar progress-bar-info" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">
												 88% Complete
											</span>
										</span>
									</span>
								</a>
							</li>
							<li>
								<a href="/mypayroll/processing/1/">
									<span class="task">
										<span class="desc">
											 Heat Limited / Warm Limited
										</span>
										<span class="percent">
											 50%
										</span>
									</span>
									<span class="progress">
										<span style="width: 50%;" class="progress-bar progress-bar-danger" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">
												 50% Complete
											</span>
										</span>
									</span>
								</a>
							</li>
							<li>
								<a href="/mypayroll/processing/1/">
									<span class="task">
										<span class="desc">
											 Metal Limited / Copper Limited
										</span>
										<span class="percent">
											 50%
										</span>
									</span>
									<span class="progress progress-striped">
										<span style="width: 50%;" class="progress-bar progress-bar-danger" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">
												 50% Complete
											</span>
										</span>
									</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="external">
						<a href="/mypayroll/processing/">
							 See all payrolls <i class="m-icon-swapright"></i>
						</a>
					</li>
				</ul>
			</li>
			<!-- END TODO DROPDOWN -->
			<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" 
							data-toggle="dropdown" 
							data-hover="dropdown" 
							data-close-others="true">
					<span class="username">

						<?php
                            if($PageURL[0] == "mypayroll" && $PageURL[2] == "client") {
                                echo "local client payroll specialist";
                            }
                            elseif($PageURL[0] == "mypayroll" && $PageURL[2] == "tmf" && $PageURL[4] == "b"){
                                echo "local tmf payroll checker"; }

                            elseif($PageURL[0] == "mypayroll" && $PageURL[2] == "tmf"){
                                echo "local tmf payroll specialist ";
                            }

                            elseif($PageURL[0] == "admin" && $PageURL[1] == "payrollMasterData"){
                                echo "gobal tmf administrator ";
                            } elseif($PageURL[0] == "admin" && $PageURL[1] == "localOfficeManagement"){
                                echo "gobal tmf administrator";
                            } elseif($PageURL[0] == "admin" && $PageURL[1] == "localtmfsetup"){
                                echo "local tmf administrator";
                            } elseif($PageURL[0] == "admin" && $PageURL[1] == "localclientwizard"){
                                echo "global tmf onboarding";
                            } elseif($PageURL[0] == "reports"){
                                echo "client user";
                            } elseif($PageURL[0] == "dashboard" && $PageURL[1] == "globaltmfdash"){
                                echo "global tmf user";
                            } elseif($PageURL[0] == "dashboard" && $PageURL[1] == "globalclientdash"){
                                echo "global client user";
                            }elseif ($PageURL[0] == "dashboard" && $PageURL[1] == "localtmf"){
                                echo "local tmf user";
                            }elseif ($PageURL[0] =="dashboard" && $PageURL[1] == "localclient"){
                                echo "local client user";
                            }
                            elseif ($PageURL[0] =="mytasks" && $PageURL[1] == "mycalendar"){
                                echo "Payroll Specialist";
                            }
                            elseif ($PageURL[0] =="mytasks" && $PageURL[1] == "myofficetasks"){
                                echo "Office Manager";
                            }
                            elseif ($PageURL[0] =="mytasks" && $PageURL[1] == "myassignedtasks"){
                                echo "Payroll Specialist";
                            }

                        ?>
					</span>
					<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href="/myprofile/">
							<i class="fa fa-user"></i> My Profile
						</a>
					</li>
					<li>
						<a href="/myprofile/calendar/">
							<i class="fa fa-calendar"></i> My Calendar
						</a>
					</li>
					<li>
						<a href="/mypayroll/mytickets/">
							<i class="fa fa-envelope"></i> My Tickets
							<span class="badge badge-info">
								 3
							</span>
						</a>
					</li>
					<li>
						<a href="/mypayroll/mytasks/">
							<i class="fa fa-tasks"></i> My Tasks
							<span class="badge badge-success">
								 <?php echo $OverdueTasks + $TodaysTasks; ?>
							</span>
						</a>
					</li>
					<li class="divider">
					</li>
					<li>
						<a href="javascript:;" id="trigger_fullscreen">
							<i class="fa fa-arrows"></i> Full Screen
						</a>
					</li>
					<li>
						<a href="extra_lock.html">
							<i class="fa fa-lock"></i> Lock Screen
						</a>
					</li>
					<li>
						<a href="login.html">
							<i class="fa fa-key"></i> Log Out
						</a>
					</li>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
		</ul>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->