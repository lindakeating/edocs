<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");

$WizardFlag = true;
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php");
 ?>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				TMF Offices
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="note note-success">
					<div class="alert alert-block alert-success fade in">
						<h4 class="alert-heading">Success!</h4>
						<h4 class="alert-heading"> You have successfully set up a local TMF office </h4>
						<h5 class="alert-heading">An email has been sent to <?php echo($_SESSION['newUser']['fname'].' '.$_SESSION['newUser']['lname']) ?> to notify them of your action. </h5>
					</div>
				</div>
			</div>
			<div class="col-md-4">
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-calendar"></i>Out of Office Days
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-hover">
								<thead>
									<tr>
									<?php 
										$sql = "SELECT * FROM out_of_office";
										$result = mysql_query($sql);
										while($row = mysql_fetch_assoc($result)){
											echo ("<th>".$row['weekly_out_of_office']."</th>");										
										}
										?>
									</tr>
								</thead>
								</table>
							</div>
						</div>
					</div>
			<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-calendar"></i>Annual Holidays
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-hover">
								<thead>
									<tr>
										<th>Holiday</th>
										<td>Date</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sql = "SELECT * FROM holidays";
										$result = mysql_query($sql);
										while($row = mysql_fetch_assoc($result)){
											echo ("<tr><td>".$row['name']."</td><td>".$row['date']."</td></tr>");										
										}
										?>
								</tbody>
								</table>
							</div>
						</div>
					</div>	
			</div>
			<div class="col-md-4">
					
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-calendar"></i>Payroll Process Days
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-hover">
								<thead>
									<tr>
										<th>Pay Schedule</th>
										<td>Day</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sql = "SELECT * FROM pay_days";
										$result = mysql_query($sql);
										$row = mysql_fetch_assoc($result);
										if($row != null){
										echo ("<tr><td>pay day</td><td>".$row['pay_day']."</td></tr>");
										echo ("<tr><td>final approval</td><td>".$row['final_approval']."</td></tr>");
										echo ("<tr><td>final output</td><td>".$row['final_output']."</td></tr>");
										echo ("<tr><td>draft approval</td><td>".$row['draft_approval']."</td></tr>");
										echo ("<tr><td>draft output</td><td>".$row['draft_output']."</td></tr>");
										echo ("<tr><td>input</td><td>".$row['input']."</td></tr>");
										}
										?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
			</div>
			<div class="col-md-4">
				<div class="portlet box grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-calendar"></i>File Types
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-hover">
								<thead>
									<tr>
										<th>File Types</th>
										<td>Mandatory</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sql = "SELECT * FROM file_type";
										$result = mysql_query($sql);
										while($row = mysql_fetch_assoc($result)){
											echo ("<tr><td>".$row['file_type']."</td><td>".$row['mandatory']."</td></tr>");										
										}
										?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>