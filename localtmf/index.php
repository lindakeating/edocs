<?php
$PageTitle = "TMF | My Tickets";

include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				TMF Offices
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-hover" id="SortTable">
					<thead>
					<tr>
						<th>
							 TMF Office
						</th>
						<th>
							 Country
						</th>
						<th>
							Platform
						</th>
						<th>
					</tr>
					</thead>
					<tbody>
					<tr class="odd gradeX">
						<td>
							<a href="/localtmf/view/?to=TMF%20Ireland">TMF Ireland
						</td>
						<td>
							 Ireland
						</td>
						<td>
							Platform 1
						</td>
					</tr>
					<tr class="even gradeX">
						<td>
							<a href="/localtmf/view/?to=TMF%20Ireland">TMF UK
						</td>
						<td>
							 UK
						</td>
						<td>
							Platform 1
						</td>
					</tr>
					</tbody>
				</table>
				<a href="new/" class="btn btn-success">Create New Office</a>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>