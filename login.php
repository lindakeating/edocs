<?php
session_start();

$user = $_POST["username"];

if(!empty($user))
{
	$_SESSION['user'] = $user;
}
else
{
	$_SESSION['user'] = "tmflocal";
}

if(strpos($user, "tmf"))
{
	$_SESSION["usertype"] = "TMF";
}
else
{
	$_SESSION["usertype"] = "Client";
}

header("location:/dashboard/");
?>