<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$UploadFlag = null;
$gClientFlag = null;
$localClient = null;
$localTmf = null;
$vmapFlag = null;
$SortTableFlag = null;
$WizardFlag = null;
$EditableTableFlag = null;
$CalendarFlag = true;
?>
    <!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        My Payroll
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row ">
                <div class="col-md-6 col-sm-6">
                    <div class="portlet box purple">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-calendar"></i>Todays Tasks
                            </div>
                            <div class="actions">
                                <a href="javascript:;" class="btn btn-sm yellow easy-pie-chart-reload">
                                    <i class="fa fa-repeat"></i> Reload
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="easy-pie-chart">
                                        <div class="number transactions" data-percent="55">
											<span>
												 42
											</span>
                                        </div>
                                        <a class="title" href="#">
                                            Tasks Due
                                        </a>
                                    </div>
                                </div>
                                <div class="margin-bottom-10 visible-sm">
                                </div>
                                <div class="col-md-3">
                                    <div class="easy-pie-chart">
                                        <div class="number visits" data-percent="85">
											<span>
												10
											</span>
                                        </div>
                                        <a class="title" href="#">
                                            Tasks Completed
                                        </a>
                                    </div>
                                </div>
                                <div class="margin-bottom-10 visible-sm">
                                </div>
                                <div class="col-md-3">
                                    <div class="easy-pie-chart">
                                        <div class="number bounce" data-percent="46">
											<span>
												 32
											</span>
                                        </div>
                                        <a class="title float-left" href="#">
                                            Tasks Outstanding
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="easy-pie-chart">
                                        <div class="number bounce " data-percent="46">
											<span>
												 1
											</span>
                                        </div>
                                        <a class="title" href="#">
                                            Tasks Overdue
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-calendar"></i>Tickets
                            </div>
                            <div class="actions">
                                <a href="javascript:;" class="btn btn-sm yellow easy-pie-chart-reload">
                                    <i class="fa fa-repeat"></i> Reload
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="easy-pie-chart">
                                        <div class="number transactions" data-percent="55">
											<span>
												 1
											</span>
                                        </div>
                                        <a class="title" href="#">
                                            Priorty 1 Ticket
                                        </a>
                                    </div>
                                </div>
                                <div class="margin-bottom-10 visible-sm">
                                </div>
                                <div class="col-md-4">
                                    <div class="easy-pie-chart">
                                        <div class="number visits" data-percent="85">
											<span>
												10
											</span>
                                        </div>
                                        <a class="title" href="#">
                                            Priority 2 Tickets
                                        </a>
                                    </div>
                                </div>
                                <div class="margin-bottom-10 visible-sm">
                                </div>
                                <div class="col-md-4">
                                    <div class="easy-pie-chart">
                                        <div class="number bounce" data-percent="46">
											<span>
												12
											</span>
                                        </div>
                                        <a class="title float-left" href="#">
                                            Priority 3 Tickets
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue calendar">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-reorder"></i>Calendar
                                </div>
                            </div>

                            <div class="portlet-body light-grey">
                                <div class="row">
                                    <div class="col-md-3 col-sm-12">
                                        <!-- BEGIN DRAGGABLE EVENTS PORTLET-->
                                        <h3 class="event-form-title">Draggable Events</h3>
                                        <div id="external-events">
                                            <form class="inline-form">
                                                <input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title"/><br/>
                                                <a href="javascript:;" id="event_add" class="btn green">
                                                    Add Event
                                                </a>
                                            </form>
                                            <hr/>
                                            <div id="event_box">
                                            </div>
                                            <label for="drop-remove">
                                                <input type="checkbox" id="drop-remove"/>remove after drop </label>
                                            <hr class="visible-xs"/>
                                        </div>
                                        <!-- END DRAGGABLE EVENTS PORTLET-->
                                    </div>
                                    <div class="col-md-9 col-sm-12">
                                        <div id="calendar" class="has-toolbar">
                                        </div>
                                    </div>
                                </div>
                                <!-- END CALENDAR PORTLET-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- BEGIN CONTENT -->
        </div>
        <!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>