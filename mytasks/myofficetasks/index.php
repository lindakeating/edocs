<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$UploadFlag = null;
$gClientFlag = null;
$localClient = null;
$localTmf = null;
$vmapFlag = null;
$SortTableFlag = null;
$WizardFlag = null;
$EditableTableFlag = null;
$CalendarFlag = true;
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				My Payroll
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row ">
				<div class="col-md-6 col-sm-6">
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-calendar"></i>Todays Tasks
							</div>
							<div class="actions">
								<a href="javascript:;" class="btn btn-sm yellow easy-pie-chart-reload">
									<i class="fa fa-repeat"></i> Reload
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-3">
									<div class="easy-pie-chart">
										<div class="number transactions" data-percent="55">
											<span>
												 140
											</span>
										</div>
										<a class="title" href="#">
											 Tasks Due
										</a>
									</div>
								</div>
								<div class="margin-bottom-10 visible-sm">
								</div>
								<div class="col-md-3">
									<div class="easy-pie-chart">
										<div class="number visits" data-percent="85">
											<span>
												50
											</span>
										</div>
										<a class="title" href="#">
											 Tasks Completed
										</a>
									</div>
								</div>
								<div class="margin-bottom-10 visible-sm">
								</div>
								<div class="col-md-3">
									<div class="easy-pie-chart">
										<div class="number bounce" data-percent="46">
											<span>
												 90
											</span>
										</div>
										<a class="title float-left" href="#">
											 Tasks Outstanding
										</a>
									</div>
								</div>
                                <div class="col-md-3">
                                    <div class="easy-pie-chart">
                                        <div class="number bounce " data-percent="46">
											<span>
												 3
											</span>
                                        </div>
                                        <a class="title" href="#">
                                            Tasks Overdue
                                        </a>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
            <div class="col-md-6 col-sm-6">
                <div class="portlet box red">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar"></i>Tickets
                        </div>
                        <div class="actions">
                            <a href="javascript:;" class="btn btn-sm yellow easy-pie-chart-reload">
                                <i class="fa fa-repeat"></i> Reload
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="easy-pie-chart">
                                    <div class="number transactions" data-percent="55">
											<span>
												 7
											</span>
                                    </div>
                                    <a class="title" href="#">
                                        Priorty 1 Ticket
                                    </a>
                                </div>
                            </div>
                            <div class="margin-bottom-10 visible-sm">
                            </div>
                            <div class="col-md-4">
                                <div class="easy-pie-chart">
                                    <div class="number visits" data-percent="85">
											<span>
												15
											</span>
                                    </div>
                                    <a class="title" href="#">
                                        Priority 2 Tickets
                                    </a>
                                </div>
                            </div>
                            <div class="margin-bottom-10 visible-sm">
                            </div>
                            <div class="col-md-4">
                                <div class="easy-pie-chart">
                                    <div class="number bounce" data-percent="46">
											<span>
												30
											</span>
                                    </div>
                                    <a class="title float-left" href="#">
                                        Priority 3 Tickets
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			</div>
			<div class="clearfix">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box grey ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>Select Tasks
						</div>
					</div>
					</div>
                    <div class="portlet-body">
                        <div class="col-md-4">
                             <div class="form-group">
                                 <label class="col-md-4 control-label">Tasks</label>
                                 <div class="col-md-8 margin-bottom-10">
                                     <select class="form-control">
                                         <option>Open</option>
                                         <option>Closed</option>
                                         <option>Overdue</option>
                                         <option>Outstanding</option>
                                     </select>
                                 </div>
                             </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Due up to</label>
                                <div class="col-md-8 margin-bottom-10">
                                        <div class="input-group ">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
                                            <input type="email" class="form-control" placeholder="30/06/2014">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Due Status</label>
                                <div class="col-md-8 margin-bottom-10">
                                    <select class="form-control">
                                        <option>All</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                            Task Type
                                        </th>
                                        <th>
                                            Company
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Due
                                        </th>
                                        <th>
                                            Due Status
                                        </th>
                                        <th>
                                            Comment
                                        </th>
                                        <th>
                                            Assigned To
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td >
                                            Process Payroll
                                        </td>
                                        <td>
                                            Ice Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            20/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-danger">
											 Overdue
										</span>
                                        </td>
                                        <td>
										Reject - Inputting Error(with client)
                                        </td>
                                        <td>
                                            Mary Moore
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Process Payroll
                                        </td>
                                        <td>
                                            Warm Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        Mary Moore
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Process Payroll
                                        </td>
                                        <td>
                                            Copper Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        Mary Moore
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Process Payroll
                                        </td>
                                        <td>
                                            Joyce Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            Mary Moore
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Transfer Funds
                                        </td>
                                        <td>
                                            Joy Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        John Ryan
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Finalise Payroll
                                        </td>
                                        <td>
                                            Heat Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        James Coates
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Review Payroll
                                        </td>
                                        <td>
                                            Marks Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        Mary Moore
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Process Payroll
                                        </td>
                                        <td>
                                            Hendrix Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        John Ryan
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Transfer Funds
                                        </td>
                                        <td>
                                            Fund Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        James Coates
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Tax Return
                                        </td>
                                        <td>
                                            FFL Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        James Coates
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Finalise Payroll
                                        </td>
                                        <td>
                                            PBP Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        James Coates
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Finalise Payroll
                                        </td>
                                        <td>
                                            Doc Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-warning">
											 Due Today
										</span>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        John Ryan
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            Transfer Funds
                                        </td>
                                        <td>
                                            Doc File Ltd
                                        </td>
                                        <td>
                                            Open
                                        </td>
                                        <td>
                                            30/06/2014
                                        </td>
                                        <td>
										<span class="label label-sm label-danger">
											 Overdue
										</span>
                                        </td>
                                        <td>
                                            Reject Processing Error
                                        </td>
                                        <td>
                                        Mary Moore
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>