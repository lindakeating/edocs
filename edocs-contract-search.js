/**
 * Created with JetBrains PhpStorm.
 * User: janitor
 * Date: 20/06/12
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */


function initViewData() {
    MyApp.isVisible = false;
    MyApp.clickedAway = false;

    MyApp.contractTableId ='contracts-table';
    MyApp.hashContractTableId ='#' + MyApp.contractTableId;
    MyApp.userBookmarkTo = {};
    MyApp.userBookmarkNote = {};

    /*MyApp.currentUserId = 1001;*/
    var d = new Date();
    MyApp.todayString = sprintf("%04d-%02d-%02d", d.getFullYear(), d.getMonth()+1,d.getDate());
    MyApp.newContractId = sprintf("%02d%02d%02d0001", d.getFullYear()%100, d.getMonth()+1,d.getDate());
    MyApp.newAttachmentId = sprintf("%02d%02d%02d1001", d.getFullYear()%100, d.getMonth()+1,d.getDate());
    MyApp.currentContractId = 0;
    MyApp.currentContractFiles = [];

    MyApp.yesNo = {
        'Yes':1,
        'No':2
    };

    MyApp.contractFilters = [
        { "sTitle":"Active contracts",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:1 },
        { "sTitle":"Expired contracts",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:0 },
        { "sTitle":"Terminated contracts",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:0 },
        { "sTitle":"Bookmarked",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:0 },
        { "sTitle":"Storage requests (excl. paused)",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:0, eDocsIsRange:0 },
        { "sTitle":"Storage requests (incl. paused)",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:0, eDocsIsRange:0 },
        { "sTitle":"Expires",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:0, eDocsIsRange:1, eDocsMinValue:0, eDocsMaxValue:31, eDocsIsRelativeDateRange:1 },
        { "sTitle":"Review within",                 "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"legacyId", eDocsIsEnabled:0, eDocsIsRange:1, eDocsMinValue:0, eDocsMaxValue:31, eDocsIsRelativeDateRange:1 },
        { "sTitle":"Uses subcontractors",           "sClass":"nowrap", "sWidth":"9em",  "mDataProp":"isUsesSubcontractors ", eDocsIsEnabled:0 }

    ];

    MyApp.contractColumns = [
        { "sTitle":"Options",                eDocsName:'options', eDocsIsVisible:1, "sClass":"alignRight", "sWidth":"13em", "mDataProp":"tmp",  eDocsSearch:'text', skipAdd:1 },
        { "sTitle":"Legacy ID",              eDocsName:'legacyId', eDocsIsVisible:0, "sClass":"nowrap",     "sWidth":"9em",  "sType":"numeric", "mDataProp":"id",  eDocsSearch:'text', skipAdd:1  },
        { "sTitle":"Document number",        eDocsName:'documentIdentifier',  eDocsIsVisible:1, "sClass":"nowrap",     "sWidth":"9em",   "sType":"string","mDataProp":"fmtDocumentIdentifier",  eDocsSearch:'text',eDocsType:'documentIdentifier', skipAdd:1   },
        { "sTitle":"Title",                  eDocsName:'contractTitle',  eDocsIsVisible:1, "sClass":"nowrap",     "sWidth":"40em", "sType":"string", "mDataProp":"contractTitle", eDocsSearch:'text'   },
        { "sTitle":"Contract type",          eDocsName:'contractType',  eDocsIsVisible:0, "sClass":"nowrap",     "sWidth":"30em", "sType":"string", "mDataProp":"contractType",  eDocsSearch:'mselect', eDocsSelect:'contractTypes'    },
        // columns 5-9
        { "sTitle":"Legacy contract type",    eDocsName:'legacyContractType', eDocsIsVisible:1, "sClass":"nowrap",     "sWidth":"30em", "sType":"string", "mDataProp":"legacyContractType", eDocsSearch:'text', skipAdd:1   },
        { "sTitle":"Status",                  eDocsName:'status', eDocsIsVisible:1, "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"contractStatus",  eDocsSearch:'mselect', eDocsSelect:'contractStatuses'  },
        { "sTitle":"Legacy status",           eDocsName:'legacyStatus', eDocsIsVisible:0, "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"legacyStatus", eDocsSearch:'text', skipAdd:1  },
        { "sTitle":"Added date",              eDocsName:'addedDate', eDocsIsVisible:0, "sClass":"noWrap",     "sWidth":"12em", "mDataProp":"addedDate", skipAdd:1  },
        { "sTitle":"Termination review date", eDocsName:'terminationReviewDate', eDocsIsVisible:0, "sClass":"noWrap",     "sWidth":"12em", "sType":"date","mDataProp":"terminationReviewDate"  },
        // columns 10-14
        { "sTitle":"Convenience term. date", eDocsName:'convenienceTermDate', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"date","mDataProp":"isConvenienceTerminationReviewDate", eDocsSearch:'mselect', eDocsSelect: 'yesNo',eDocsType:'bool' },
        { "sTitle":"Notice period",          eDocsName:'noticePeriod', eDocsIsVisible:0 , "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"noticePeriod" },
        { "sTitle":"Auto renewal",           eDocsName:'isAutoRenewal', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "mDataProp":"isAutoRenewal", eDocsSearch:'mselect', eDocsSelect: 'yesNo',eDocsType:'bool'  },
        { "sTitle":"Contract value range",   eDocsName:'contractValueRange', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"contractValueRange", eDocsSearch:'mselect', eDocsSelect:'contractValueRanges'  },
        { "sTitle":"Directorate",            eDocsName:'directorate', eDocsIsVisible:1,  "sClass":"noWrap",     "sWidth":"12em","sType":"string", "mDataProp":"directorate", eDocsSearch:'mselect', eDocsSelect: 'directorates'  },
        // Columns 15-19
        { "sTitle":"contracting entity",     eDocsName:'contractingEntity', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"o2ContractingEntity", eDocsSearch:'mselect', eDocsSelect:'o2ContractingEntities'  },
        { "sTitle":"Supplier",               eDocsName:'supplier', eDocsIsVisible:1,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"supplier", eDocsSearch:'mselect', eDocsSelect:'suppliers', eDocsType:'supplier'  },
        { "sTitle":"Agreement cmct. date",   eDocsName:'agreementCmctDate', eDocsIsVisible:0 , "sClass":"noWrap",     "sWidth":"12em", "sType":"date","mDataProp":"agreementCommencementDate"  },
        { "sTitle":"Signature date",         eDocsName:'signatureDate', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"date","mDataProp":"signatureDate"  },
        { "sTitle":"Contract expiry date",   eDocsName:'contractExpiryDate', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"date","mDataProp":"contractExpireDate"  },
        // Columns 20-24
        { "sTitle":"Business owner",         eDocsName:'businessOwner', eDocsIsVisible:0 , "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"businessOwner" },
        { "sTitle":"Procurement owner",      eDocsName:'procurementOwner', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"procurementOwner"  },
        { "sTitle":"Legal adviser",          eDocsName:'legalAdvisor', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"legalAdvisor" },
        { "sTitle":"Uses sub-contractors",   eDocsName:'isSubcontractors', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"3em", "mDataProp":"isUsesSubcontractors",eDocsSearch:'mselect', eDocsSelect: 'yesNo',eDocsType:'bool' },
        { "sTitle":"Key review provisions",  eDocsName:'keyReviewProvisions', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"keyReviewProvisions",  eDocsSearch: 'textarea', eDocsType:'textarea' },
        // columns 25-28
        { "sTitle":"Storage refence",        eDocsName:'storageReference', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"storageLocationReference", skipAdd:1  },
        { "sTitle":"Location",               eDocsName:'documentLocation', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"documentLocation",  eDocsSearch:'mselect',eDocsSelect:'documentLocations'  },
        { "sTitle":"Index status",           eDocsName:'indexStatus', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"indexStatus", eDocsSearch:'mselect',eDocsSelect:'indexStatuses'  },
        { "sTitle":"Notes",                  eDocsName:'notes', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"notes", eDocsSearch:'textarea', eDocsType:'textarea'  },
        { "sTitle":"Ariba reference" ,        eDocsName:'aribaReference', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"aribaReference", eDocsSearch:'text', eDocsType:'text'  },
	// column 29. A hidden column to permit searching for contracts via related document document identifier.
        { "sTitle":"Related documents" ,        eDocsName:'relatedDocuments', eDocsIsVisible:0,  "sClass":"noWrap",     "sWidth":"12em", "sType":"string","mDataProp":"relatedDocuments", eDocsSearch:'text', eDocsType:'text'  }
    ];
    initContractEditConfig({});
}

function prepareContractsTableData() {
    return MyApp.contracts.items;
}
/*** UNUSED
 function setStorageRequestState(options) {
 var documentIdentifier = options.documentIdentifier;
 var reqData = { "userId" : MyApp.currentUserId, "documentIdentifier": documentIdentifier };

 if (options.isCancel) {
 sendServerRequest({ url:'storage_requests/remove', sync: true, data: reqData, onSuccess: "noOp" });
 delete MyApp.storageRequests[documentIdentifier];
 } else {
 var storageRequest = MyApp.storageRequests[documentIdentifier] || {};
 storageRequest.documentIdentifier = documentIdentifier;

 if (options.hasOwnProperty("isOnHold")) {
 reqData.isOnHold = options.isOnHold;
 storageRequest.isOnHold = options.isOnHold;
 }
 if (options.hasOwnProperty("isSendToStorage")) {
 reqData.isSendToStorage = options.isSendToStorage;
 storageRequest.isSendToStorage = options.isSendToStorage;
 }
 sendServerRequest({ url:'storage_requests/add', sync: true, data: reqData, onSuccess: "noOp" });
 }

 //var oTable = $(MyApp.hashContractTableId).dataTable();
 return false;
 }
 **/




function viewFileInNewWindow(documentIdentifier) {
    setDocumentViewed({ documentIdentifier: documentIdentifier, isViewed:true});
    window.open('/downloads/get?document_identifier=' + documentIdentifier, '_blank', 'fullscreen=yes');
    //window.open(filePath, '_blank', 'fullscreen=yes');

    return false;
}

function setDocumentViewed(options) {
    var match;
    var id = MyApp.contractIdsByDocumentIdentifier[options.documentIdentifier];
    if (id) {
        match = MyApp.contracts.find(id);
    } else {
         id = MyApp.relatedDocumentIdsByDocumentIdentifier[options.documentIdentifier];
        if (id) {
            match = MyApp.relatedDocuments.find(id);
        }
    }
    if (match) {
        match.isViewed = options.isViewed;
    }
    /*
    var matches = MyApp.contracts.findByKeyValue('documentIdentifier', options.documentIdentifier);
    if (!matches) {
        matches = MyApp.relatedDocuments.findByKeyValue('documentIdentifier', options.documentIdentifier);
    }

    if (matches && matches.length > 0) {
        matches[0].isViewed = options.isViewed;
    } else {
        alert("setDocumentViewed: did not find match for documentIdentifier=" + options.documentIdentifier);
    }
    */
}

function storeUserBookmarkNote(contractId, value) {
    if (value === null || value === undefined) {
        value = "";
    } else {
        value = value.trim();
    }
    if (value !== "") {
        MyApp.userBookmarkNote[contractId] = value;
    } else {
        delete MyApp.userBookmarkNote[contractId];
    }
    return false;
}


function getUserBookmarkNote(contractId) {
    return(MyApp.userBookmarkNote[contractId] !== undefined) ? MyApp.userBookmarkNote[contractId] : '';
}


function isUserBookmarkTo(contractId, userId) {

    var isSet;
    if (userId === undefined) {
        isSet = MyApp.userBookmarkTo[contractId] !== undefined? 1: 0;
    } else {
        isSet = (MyApp.userBookmarkTo[contractId] !== undefined && MyApp.userBookmarkTo[contractId][userId] !== undefined) ? 1 : 0;
    }
    return isSet;
}

// is used des[ite what jetbrains says
function sendUserBookmarkAndUpdateScreen(contractId) {
    if (MyApp.userBookmarkTo[contractId] === undefined) {
        return false;
    }
    var messageText = getUserBookmarkNote(contractId);
    if (messageText === undefined || messageText === null) {
        messageText = '';
    }
    var d = new Date();
    MyApp.todayString = sprintf("%04d-%02d-%02d", d.getFullYear(), d.getMonth()+1,d.getDate());
    $.each(MyApp.userBookmarkTo[contractId], function(item, index) {
        if (MyApp.postedBookmarks[contractId] === undefined) {
            MyApp.postedBookmarks[contractId] = {};
        }

        MyApp.postedBookmarks[contractId][item] = {
            'timestamp': MyApp.todayString,
            'sender': MyApp.currentUserId,
            'message' : messageText
        };
        sendServerRequest({ url:'posted_bookmarks/add', sync: true, data: { "senderUserId" : MyApp.currentUserId, "contractId": contractId, 'note': messageText }, onSuccess: "noOp" });
    });

    // TODO: send it
    resetUserBookmarkAndUpdateScreen(contractId);
    return false;
}


function resetUserBookmark(contractId) {
    storeUserBookmarkNote(contractId, null);

    if (MyApp.userBookmarkTo[contractId] !== undefined) {
        delete MyApp.userBookmarkTo[contractId];
    }
}


function resetUserBookmarkAndUpdateScreen(contractId) {
    resetUserBookmark(contractId);
    $('#td_contract_id_' + contractId).html(renderIconCell(1,contractId));
    // TODO: what is going on below?
    $('_id_' + contractId).html(fnFormatDetails(contractId));
    resetErPostBookmarkForm();
    return false;
}


function setUserBookmarkTo(contractId, userId, isSet) {
    if (isSet) {
        if (MyApp.userBookmarkTo[contractId] === undefined) {
            MyApp.userBookmarkTo[contractId] = {};
        }
        MyApp.userBookmarkTo[contractId][userId] = 1;
    } else {
        if (MyApp.userBookmarkTo[contractId] !== undefined) {
            delete MyApp.userBookmarkTo[contractId][userId];
        }
    }
    if (MyApp.userBookmarkTo[contractId] !== undefined && Object.size(MyApp.userBookmarkTo[contractId]) == 0){
        MyApp.userBookmarkTo[contractId] = undefined;
    }
    if (MyApp.userBookmarkTo[contractId] === undefined) {
        $("#send_bookmark_contract_id_" + contractId).addClass("disabled").attr('disabled','disabled');
    } else {
        $("#send_bookmark_contract_id_" + contractId).removeClass("disabled").removeAttr('disabled');
    }
    $('#td_contract_id_' + contractId).html(renderIconCell(1,contractId));
    $('.tr_sub_contract_id_' + contractId).html(fnFormatDetails(contractId));
    resetErPostBookmarkForm();

    adjustCurrentContractOptionsHeight();
    return false;

}


function getDocIcon(contract) {
    var docIcon = 'icon-file';
    if (contract.relatedDocuments.length > 0) {
        if (contract.isHasAddendum) {
            docIcon = 'icon-paste';
        } else {
            docIcon = 'icon-copy';
        }
    }
    return docIcon;
}

function fnFormatDetails (contractId ) {
    var userOptions = '';
    //var possiblyDisabled = isUserBookmarkTo(contractId, undefined) ? '' : ' disabled" disabled="disabled';
    var contract = MyApp.contracts.find(contractId);
    if (contract == undefined) {
        return '<em>Contract with legacyId=' + contractId + ' not found</em>';
    }
    //var storageOptions = '';
    MyApp.users.each(function(item){
        var isChecked = isUserBookmarkTo(contractId, item.id);
        userOptions += '<li><a href="#"  onclick="setUserBookmarkTo('+contractId+','+item.id+','+(!isChecked)+');">' +  item.name.escapeHTML()
            + (isChecked?' <i class="icon-ok"></i>' : '')
            + '</a></li>';
    });

    var html = '';

    html +=

        '<div class="tablebox  filler" style="width:49%">'
            + '<div class="well2" id="currentContractStorageOptions_contract_id_'+contractId+'">'

            //+ createErListFilesSection(contract)
            + createErUpdateStorageRequestSection()
            /*
             + '<ul class="nav nav-pills">'
             +'<li class="dropdown">'
             +   '<a class="dropdown-toggle"  data-toggle="dropdown" href="#"><i class="icon-truck"></i> Storage request<b class="caret"></b></a>'
             +'<ul class="dropdown-menu">'
             + storageOptions
             +'</ul>'
             +'</li>'
             + '</ul>' **/
            + '</div>' // well2
            + '</div>'


            + '<div class="tablebox  filler" style="width:49%">'
            + '<div class="well2" id="currentContractPostBookmarks_contract_id_'+contractId+'">'

            //+ createErListFilesSection(contract)
            + createErPostBookmarkSection()
            /*
             + '<ul class="nav nav-pills">'
             +'<li class="dropdown">'
             +   '<a class="dropdown-toggle"  data-toggle="dropdown" href="#"><i class="icon-truck"></i> Storage request<b class="caret"></b></a>'
             +'<ul class="dropdown-menu">'
             + storageOptions
             +'</ul>'
             +'</li>'
             + '</ul>' **/
            + '</div>' // well2
            + '</div>';


    //tablebox
    //$("#currentContractStorageOptions").html(createErUpdateStorageRequestSection());
    //viewCurrrentContractStorageRequests("currentContractStorageOptions");

    /*
     +     '<div class="tablebox filler" style="width:49%">'
     + '<form class="well2 form-horizontal">'
     //+ '<fieldset>'
     + '<ul class="nav nav-pills">'
     + '<li class="dropdown">'
     +   '<a class="dropdown-toggle"  data-toggle="dropdown" href="#"> <i class="icon-user"></i> Users<b class="caret"></b></a>'
     +    '<ul class="dropdown-menu">'
     +       userOptions
     +    '</ul>'

     +'</li>'
     // TODO: escape user bookmark note
     + '<li class="spaced"><input type="text" class="span3" placeholder="Comment" value="'+getUserBookmarkNote(contractId)+'" onblur="storeUserBookmarkNote('+contractId+',this.value);"></li>'
     +' <li class="spaced"><button id="send_bookmark_contract_id_'+contractId+'" data-toggle="button" class="btn btn-mini btn-primary'+possiblyDisabled+'" onclick="sendUserBookmarkAndUpdateScreen('+contractId+');"> <i class="icon-share-alt"></i> Send bookmark</button></li>'
     //+' <li class="spaced"><button id="send_bookmark_contract_id_'+contractId+'" data-toggle="button" class="btn btn-mini btn-primary'+possiblyDisabled+'" onclick="sendUserBookmarkAndUpdateScreen('+contractId+');"> <i class="icon-paper-clip"></i> Send note</button></li>'
     +' <li class="spaced"><button data-toggle="button" class="btn btn-mini" onclick="resetUserBookmarkAndUpdateScreen('+contractId+');">Reset</button></li>'
     + '</ul>'



     + '</form></div>'

     */



    return html;
}


function setContractsTableColumnVisibility(iCol, visibility)
{
    var oTable = $(MyApp.hashContractTableId).dataTable();
    var i;

    if (visibility === 'toggle') {
        var curVisibility = oTable.fnSettings().aoColumns[iCol].bVisible;
        visibility = !curVisibility;
    }
    oTable.fnSetColumnVis( iCol, visibility ? true : false);
    var item= MyApp.contractColumns[iCol];
    if (item != undefined) {
         MyApp.visibleContractColumns[item.eDocsName] = visibility ? 1:0;
        sendServerRequest({ url:'visible_contract_columns/setVisibility', sync: true, data: { "columnName" : item.eDocsName, 'isVisible':visibility ? 1:0} });
    }

    return false;
}


function setContractsTableCommonFilterRange(index) {
    var minRawValue =$("#searchFilterMin" + index).val();
    var maxRawValue =$("#searchFilterMax" + index).val();
    var minValue =  parseInt(minRawValue,10);
    var maxValue =  parseInt(maxRawValue,10);

    if (!minRawValue.match(/^\s*[+-]?\d+\s*$/) || !maxRawValue.match(/^\s*[+-]?\d+\s*$/)
        || isNaN(minValue) || isNaN(maxValue) || (minValue > maxValue)) {
        $("#searchFilterRange" + index).addClass('error');
    } else {
        MyApp.contractFilters[index].eDocsMinValue =minValue;
        MyApp.contractFilters[index].eDocsMaxValue =maxValue;
        if (MyApp.contractFilters[index].eDocsIsRelativeDateRange) {
            MyApp.contractFilters[index].eDocsMinDate = createDateString(MyApp.contractFilters[index].eDocsMinValue);
            MyApp.contractFilters[index].eDocsMaxDate = createDateString(MyApp.contractFilters[index].eDocsMaxValue);
        }
        $("#searchFilterRange" + index).removeClass('error');
        if(MyApp.contractFilters[index].eDocsIsEnabled) {
            var oTable = $(MyApp.hashContractTableId).dataTable();
            oTable.fnDraw();

        }
    }
    return false;
}


function setContractsTableCommonFilter(iCol, isFilterEnabled)
{
    var oTable = $(MyApp.hashContractTableId).dataTable();

    //oTable.fnSetColumnVis( iCol, visibility ? true : false);
    // Redraw table
    MyApp.contractFilters[iCol].eDocsIsEnabled = isFilterEnabled;
    //$("#searchFilterCheckbox" + (iCol+1)).attr('checked',isFilterEnabled);

    if (isFilterEnabled && (iCol === 3 || iCol === 4 || iCol === 5) ) {
        MyApp.contractFilters[0].eDocsIsEnabled = true;
        MyApp.contractFilters[1].eDocsIsEnabled = true;
        MyApp.contractFilters[2].eDocsIsEnabled = true;
        $("#searchFilterCheckbox1").attr('checked',true);
        $("#searchFilterCheckbox2").attr('checked',true);
        $("#searchFilterCheckbox3").attr('checked',true);
        if (iCol === 4) {
            MyApp.contractFilters[5].eDocsIsEnabled = false;
            $("#searchFilterCheckbox6").attr('checked',false);
        } else if (iCol === 5) {
            MyApp.contractFilters[4].eDocsIsEnabled = false;
            $("#searchFilterCheckbox5").attr('checked',false);
        }
    }  else if (isFilterEnabled && (iCol === 6 || iCol === 7) ) { // expires in N days or termination review within N days
        MyApp.contractFilters[0].eDocsIsEnabled = true;
        $("#searchFilterCheckbox1").attr('checked',true);
    }

    oTable.fnDraw();
    return false;
}


function createDateString(daysFromToday) {
    var d = new Date();

    d.setDate(d.getDate()+daysFromToday);
    return sprintf("%04d-%02d-%02d", d.getFullYear(), d.getMonth()+1,d.getDate());
}

function filterRow( oSettings, aData, iDataIndex ) {
    var option;
    var value = true;
    var status = aData[6];
    var cFiltersEnabled = 0;
    var storageRequest;
    $.each(MyApp.contractFilters, function(){ ++cFiltersEnabled;});
    // If no filters enabled, do no filtering.
    if (!cFiltersEnabled) {
        return true;
    }
    if (status === "Active") {
        if (MyApp.contractFilters[0].eDocsIsEnabled < 1) {
            value = false;
        }
    } else if (status == "Expired") {
        if (MyApp.contractFilters[1].eDocsIsEnabled < 1) {
            value = false;
        }
    } else if (status == "Terminated") {
        if (MyApp.contractFilters[2].eDocsIsEnabled < 1) {
            value = false;
        }
    }

    if (value && MyApp.contractFilters[3].eDocsIsEnabled) {
        // bookmarked.
        var contractId = parseInt(aData[1],10);
        if (contractId === undefined || (MyApp.simpleBookmarks[contractId] === undefined && MyApp.postedBookmarks[contractId] === undefined)) {
            value = false;
        }

    }
    if (value && MyApp.contractFilters[4].eDocsIsEnabled) {
        var contract = MyApp.contracts.find(aData[1]);
        if (contract === undefined) {
            value = false;
        } else {
            value = false;
            storageRequest = MyApp.storageRequests[contract.documentIdentifier];

            if (storageRequest && !storageRequest.isOnHold) {
                value = true;
            } else {
//                console.log("check for storage request for related documents for " + contract.documentIdentifier);
                $.each(contract.relatedDocuments, function(indexInArray, item){
                    //console.log("storage request item ", item);

                    storageRequest = MyApp.storageRequests[item.documentIdentifier];
                    //console.log("related document " + item.documentIdentifier + " storageRequest " + storageRequest);
                    if (storageRequest && !storageRequest.isOnHold) {
                      //  console.log("found storage request for related document for " + item.documentIdentifier);
                        value = true;
                    }
                });
            }
        }
    }
    if (value && MyApp.contractFilters[5].eDocsIsEnabled) {
        var contract = MyApp.contracts.find(aData[1]);
        if (contract === undefined) {
            value = false;
        } else {
            value = false;
            storageRequest = MyApp.storageRequests[contract.documentIdentifier];
            if (storageRequest) {
                value = true;
            } else {
                $.each(contract.relatedDocuments, function(item, index){
                    storageRequest = MyApp.storageRequests[item.documentIdentifier];
                    if (storageRequest) {
                        value = true;
                    }
                });
            }
        }
    }
    option = MyApp.contractFilters[6];
    if (value && option.eDocsIsEnabled) {
        // for collection/return

        if (option.eDocsMinDate === undefined || isNaN(option.eDocsMinDate)) {
            option.eDocsMinDate = createDateString(option.eDocsMinValue);
            option.eDocsMaxDate = createDateString(option.eDocsMaxValue);
        }


        var expiryDate = aData[19];//expiry date
        if (expiryDate.match(/^\d\d\d\d-\d\d-\d\d$/)) {
            if ( expiryDate >  '1900-00-00' && expiryDate >= option.eDocsMinDate && expiryDate <= option.eDocsMaxDate) {
                value = true;
            } else {
                value = false;
            }
        }
    }
    option = MyApp.contractFilters[7];
    if (value && option.eDocsIsEnabled) {
        if (option.eDocsMinDate === undefined || isNaN(option.eDocsMinDate)) {
            option.eDocsMinDate = createDateString(option.eDocsMinValue);
            option.eDocsMaxDate = createDateString(option.eDocsMaxValue);
        }
        // for collection/return
        var termReviewDate = aData[9]; // termination review date
        if (termReviewDate.match(/^\d\d\d\d-\d\d-\d\d$/)) {
            if ( termReviewDate >  '1900-00-00' && termReviewDate >= option.eDocsMinDate && termReviewDate <= option.eDocsMaxDate) {
                value = true;
            } else {
                value = false;
            }
        } else {
            // No review date specified.
            value = false;
        }
    }
    if (value && MyApp.contractFilters[8].eDocsIsEnabled) {
        value =  (aData[23] > 0);//isUsesSubcontractors
    }

    return value;
}
function addContractsTableCommonFilters() {
    var d = new Date();

    $.fn.dataTableExt.afnFiltering.push( filterRow );
}

function isSearchColumnVisible(item) {
    var isVisible = false;
    if (MyApp.visibleContractColumns[item.eDocsName] != undefined) {
        isVisible = MyApp.visibleContractColumns[item.eDocsName] * 1;
    } else {
        isVisible = item.eDocsIsVisible;
    }
    return isVisible;
}




function showContractsTableCheckboxes() {
    $.each(MyApp.contractColumns,function(index,item) {
        var myDiv = $("#contract-table-column-toggles-" + (1+(index%3)));
        if (index >= 1) {
            myDiv.append('<label class="checkbox">'
                + sprintf('<input type="checkbox" id="optionsCheckbox%d" value="option%d" onclick="setContractsTableColumnVisibility(%d,'+'%s);"%s>%s</label>',
                index+1, index+1,
                index, "'toggle'",
                isSearchColumnVisible(item)? ' checked="1"' : '',
                item.sTitle));
        }
    });
}


function getContractsTableCommonFilterMin(index) {
    return MyApp.contractFilters[index].eDocsMinValue;
}


function getContractsTableCommonFilterMax(index) {
    return MyApp.contractFilters[index].eDocsMaxValue;
}


function showContractsTableCommonOptionsCheckboxes() {

    $.each(MyApp.contractFilters,function(index,value) {
        var myDiv = $("#contract-table-common-options-toggles-" + (1+(index%3)));

        if (value.eDocsIsRange) {

            myDiv.append(sprintf('<div id="searchFilterRange%d" class="control-group"><form class="form-inline"><label class="checkbox">'
                + '<input type="checkbox" id="searchFilterCheckbox%d" value="option%d" onclick="setContractsTableCommonFilter(%d, this.checked'+');"%s>%s</label>'
                + ' <input type="text" class="input-tiny" id="searchFilterMin%d" value="'+getContractsTableCommonFilterMin(index)+'" onblur="setContractsTableCommonFilterRange(%d);" /> <label class="control-label" for="searchFilterMin%d"> to </label> '
                + ' <input type="text" class="input-tiny" id="searchFilterMax%d" value="'+getContractsTableCommonFilterMax(index)+'" onblur="setContractsTableCommonFilterRange(%d);" /> <label class="control-label" for="searchFilterMax%d"> days </label></form></div>',
                index,
                index+1, index+1,
                index,
                value.eDocsIsEnabled ? ' checked="1"' : '',
                value.sTitle,
                index,index,index,
                index,index,index));

        } else {
            myDiv.append('<label class="checkbox">'
                + sprintf('<input type="checkbox" id="searchFilterCheckbox%d" value="option%d" onclick="setContractsTableCommonFilter(%d, this.checked'+');"%s>%s</label>',
                index+1, index+1,
                index,
                value.eDocsIsEnabled ? ' checked="1"' : '',
                value.sTitle));
        }

    });
}





function toggleExtraSearchOptions() {
    $("#contract-search-accordion").toggle();
}




function setCurrentContract(contractId) {
    MyApp.isAddAttachment = false;
    MyApp.isUpdateAttachment = false;
    MyApp.isUploadFile = false;

    if (MyApp.currentContractId != contractId) {
        MyApp.bookMarkSelectedUsers = {};
        MyApp.bookMarkSelectedUsersCount = 0;

        if (MyApp.currentContractId) {
            var tr = $("#tr_contract_id_" + MyApp.currentContractId);
            var nTr, oTable;
            if (tr) { nTr = tr[0];}
            oTable = $(MyApp.hashContractTableId).dataTable();
            if (nTr && oTable) {
                oTable.fnClose(nTr);
                $("#options_contract_id_" + MyApp.currentContractId).removeClass('icon-minus-sign').addClass('icon-plus-sign');
            }
        }
    }

    MyApp.currentContractId = contractId;

    var contract = undefined;
    if (contractId !== undefined) {
        contract = MyApp.contracts.find(contractId);
    }

    if (contract === undefined) {
        // TODO: what to do if contract does not exist!
        // Disable view summary and view documents tabs
        $("#mainTab-tab-li-view-documents").addClass('disabled');
        $("#mainTab-tab-li-view-summary").addClass('disabled');
        $("#mainTab-tab-li-view-summary").off('click');

        //$("#mainTab-tab-li-add-attachment").addClass('disabled');
       // $("#mainTab-tab-li-add-attachment").off('click');
        //$("#view-documents-tab-icon").attr('data-toggle', 'dropdown');
        return;
    } else {
        $("#mainTab-tab-li-view-documents").removeClass('disabled');
        $("#mainTab-tab-li-view-summary").removeClass('disabled');

        //$("#mainTab-tab-li-add-attachment").removeClass('disabled');
        $("#view-documents-tab-icon").attr('data-toggle', 'dropdown');

        $("#mainTab-tab-li-view-summary-a").attr('data-toggle', 'tab');
        $("#mainTab-tab-li-view-summary-a").off('click');
        $("#mainTab-tab-li-view-summary-a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        //$("#mainTab-tab-li-options-a").attr('data-toggle', 'tab');
       // $("#mainTab-tab-li-options-a").off('click');
       // $("#mainTab-tab-li-options-a").click(function(e) {
       //     e.preventDefault();
       //     $(this).tab('show');
       // });


        $('#mainTab a[href="#contract-details-tab"]').on('shown', function () {
            // e.target // activated tab
            //e.relatedTarget // previous tab
            adjustContractDetailsHeight('viewcontract');
            //alert("shown tab callback");
        });

        $('#mainTab a[href="#contract-details-tab"]').click(function(e){
            showContractDetails(e, { 'mode':'view', 'contractId': MyApp.currentContractId });
        });
        //$("#mainTab-tab-li-view-summary").tab();
    }
    $('#mainTab a[href="#options-tab"]').on('shown', function () {
        adjustContractDetailsHeight('editoptions');
    });

    var docIcon = getDocIcon(contract);
    $("a#view-documents-tab-icon i").removeClass(
        (function (index, css) {
            return (css.match (/\bicon-\S+/g) || []).join(' ');
        })).addClass(docIcon);
    MyApp.currentContractFiles = [
        {
            'description': contract.fmtDocumentIdentifier  + " : "
                +  contract.contractTitle
                + ", " +  contract.contractType,
            'documentIdentifier': contract.documentIdentifier,
            'obj' : contract
        }
    ];
    $.each(contract.relatedDocuments, function(index, item){
        MyApp.currentContractFiles.push(
            {
                'description': item.fmtDocumentIdentifier
                    + ": " + item.relatedDocumentType,
                'documentIdentifier': item.documentIdentifier,
                'obj' : item
            }
        );
    });
    //showContractDetails(contractId);
    showContractFiles(MyApp.currentContractFiles);
}


function showItemViewed(item, documentIdentifier) {

    var html = item.innerHTML;
    if (!html.match(/i class="icon\-ok"/)) {
        html += ' <i class="icon-ok">';
        item.innerHTML = html;
        var filePath = '/downloads/get?document_identifier=' + documentIdentifier;
        $('#pdf-file').attr('data', filePath + "#toolbar=1&navpanes=0&scrollbar=1&page=1&view=FitH");
    }

    $('#mainTab a[href="#contract-files-tab"]').tab('show');
    return false;
}


function showContractFiles(files){
    $('#view-documents-dropdown-menu').empty();
    var c=0;
    $.each(files, function(index){
        var possiblyMarkedViewed = (++c === 100000)?' <i class="icon-ok">' :'';

        $('#view-documents-dropdown-menu').append(
            $('<li>').append(
                $('<a>').attr('href','#contract-files-tab').attr('onclick', 'showItemViewed(this, '+files[index].documentIdentifier+');').append(
                    $('<span>').attr('class', 'tab').append(files[index].description + possiblyMarkedViewed)
                )));
        if (c === 1 && files.length > 1) {
            $("#view-documents-dropdown-menu").append('<li class="divider"></li>');
        }

    });

}

function showViewDocuments(e, contractId) {

    var myDiv = $("#view-documents-tab-icon-content");
    myDiv.empty();

    var contract = undefined;
    if (contractId !== undefined) {
        contract = MyApp.contracts.find(contractId);
    }
    if (contract === undefined) {
        myDiv.append('<h3>No contract selected</h3><p>Please select a contract from the search results and then click on this tab again.</p>');
        e.preventDefault();
        return;
    }
}

function showContractsTable() {
//    alert("showContractsTable.start");
    //#contract-search-ta
    $('#mainTab a[href="#contract-search-tab"]').click(function(e){
        MyApp.isSearchTabActive = true;
    });
    $('#mainTab a[href="#contract-details-tab"]').click(function(e){
        MyApp.isSearchTabActive = false;
        showContractDetails(e, { 'mode': 'view', 'contractId': MyApp.currentContractId });
    });

    if (isThisUserPermitted( { permission:'isAddDocument' } )) {
        $("#mainTab-tab-li-add-contract").attr('data-toggle', 'tab');
        $("#mainTab-tab-li-add-contract").removeClass('disabled');
        $('#mainTab a[href="#contract-add-tab"]').click(function(e){
            try {
            MyApp.isSearchTabActive = false;
            showContractDetails(null, { 'mode': 'new', 'divId': "add-contract-form-elements"});
            }catch(e) { alert("exception drawing add contract " + e.stack);}
        });
        $('#mainTab a[href="#contract-add-tab"]').on('shown', function () {
            // e.target // activated tab
            //e.relatedTarget // previous tab
            adjustContractDetailsHeight('newcontract');
            //alert("shown tab callback");
        });
    }
    $('#mainTab a[href="#options-tab"]').click(function(e){
        MyApp.isSearchTabActive = false;
        showOptions(null, { 'mode': 'new', 'divId': "options-form-elements"});
    });
    $('#mainTab a[href="#options-tab"]').on('shown', function () {
        adjustContractDetailsHeight('editoptions');
    });


    $('#mainTab a[href="#view-documents-tab-icon"]').click(function(e){
        showViewDocuments(e, MyApp.currentContractId);
    });

    var table;
    var rows = [];
    var id="contract-search-columns-visibility-toggle";
    initViewData();
    var myDiv = $('#' + id);

    showContractsTableCheckboxes();
    showContractsTableCommonOptionsCheckboxes();
    addContractsTableCommonFilters();
    //login('johnbrown','pass');
    showContractsTable2();
}


function showContractsTable2() {
    var table;
    var rows = [];
    var id="contract-search-columns-visibility-toggle";
    var myDiv = $('#' + id);
    var colsNotVisible = [];
    rows = prepareContractsTableData();

    $.each(MyApp.contractColumns,function(index, item){
        if(!isSearchColumnVisible(item)) {
            colsNotVisible.push(index);
        }
    });

    if (MyApp.isContractsTableCreated) {
        // Destroy it
        $(MyApp.hashContractTableId).dataTable().fnDestroy();
        MyApp.isContractsTableCreated = false;
    }

    $(MyApp.hashContractTableId).dataTable({
            "sDom": "<'row'<'span8'l<\"search-toolbar\">><'span4'f>r>t<'row'<'span6'i<\"table-buttons\">><'span6'>p>",
            "bDeferRender": false,
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ rows per page</div>"
            },
            "bAutoWidth" : false,
            //"bStateSave": true,
            "aoColumns": MyApp.contractColumns,
            //     "aoColumns" : initDataTableColumnMapping(),
            // If using an array of objects
            // aoColumns: [ { "mDataProp": "cell1" },{ "mDataProp": "cell2" },{ "mDataProp": "cell3" } ],
            "bUseRendered" : true,
            "aaData" : rows,
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                var contractId = parseInt(aData.id,10);
                $(nRow).attr('id', 'tr_contract_id_' + contractId);
                $('td:eq(0)', nRow).attr('id', 'td_contract_id_' + contractId);
            },
            "aoColumnDefs": [
                {
                    "fnRender": renderFilterResultsCell, "aTargets": [ 0, 10, 12, 23, 29 ]
                },
                {
                    "bSortable": false, "aTargets": [ 0 ] /* should be false * */
                },
                /*
                 {
                 "sType": "string", "aTargets": [ 0,1,2,3,4,5,6,7,8,9,
                 10,11,12,13,14,15,16,17,18,19,
                 20,21,22,23,24,25,26,27,28,29, 30,31]
                 },
                 */
                {
                    "bVisible": false, "aTargets": colsNotVisible
                }
            ],
            "fnDrawCallback": function( oSettings ) {
                //$(".table-popover").popover();
                updatePopovers();
            }
        }
    );
    $("#loginModal").modal('hide');
    /*
     '<ul class="nav nav-pills smaller-margin" style="margin-bottom:2px;padding-bottom:2px;">'
     + '<li style="font-size:110%"><a href="#" onclick="createAddAttachmentForm();"><i class="icon-paper-clip"></i> Add attachment</a></li> '
     */

    $("div.search-toolbar").append(
        '<ul class="nav nav-pills smaller-margin" zstyle="margin-bottom:2px;padding-bottom:20px;">'
            //   + '<li>Show tools <label class="checkbox"><input type="checkbox" id="search-toolbar-checkbox" onclick="toggleExtraSearchOptions();" /></label></li>'
            +'<li><input type="checkbox" id="search-toolbar-checkbox" onclick="toggleExtraSearchOptions();"><i class="icon-cogs icon-large"></i></label></li>'
            //+'<li><a href="#" class="btn btn-mini" onclick="refreshSearchTable();">Refresh</a></li>'
            //+ '<li><a class="btn" href="#" onclick="refreshSearchTable();">Refresh</a></li>'
            + '</ul>'
    );

    $("div.table-buttons").append(' <span id="testDownloadify" style="padding-left:20px"></span>');
    initDownloadOption();
    //$("div.table-buttons").append('<a class="btn" href="#" style="margin-left:20px;margin-top:5px" onclick="downloadSearchResults();"><i class="icon-download-alt"></i> Save search results</a><p id="testDownloadify"></p>');
    /*
     '<ul class="nav nav-pills smaller-margin" zstyle="margin-bottom:2px;padding-bottom:20px;">'
     + '<li>Show tools <label class="checkbox"><input type="checkbox" id="search-toolbar-checkbox" onclick="toggleExtraSearchOptions();" /></label></li>'
     + '<li style="margin-left:20px"><label class="checkbox"><a href="#" onclick="downloadContractTable();"><i class="icon-download-alt"></i> Save search results</a></label></li> '
     + '</ul>'
     );
     */
    //$(".table-popover").popover();

    MyApp.isContractsTableCreated = true;

    table = $(MyApp.hashContractTableId).dataTable();

    $(MyApp.hashContractTableId + " tbody").click(function(event) {
        // If the event is for a expanded row, do nothing here.

        var node = event.target;
        var isTr;
        var isExpandedTr;
        var isTrOptions;
        var contractId;
        var matches;
        for(node = event.target; node && !isTr && !isExpandedTr; node = node.parentNode) {
            if (node.nodeName === 'TD') {
                for(var i=0; i < node.classList.length; ++i) {
                    matches = (node.classList[i]).match(/^tr_sub_contract_id_(\d+)$/);
                    if (matches && ((contractId = parseInt(matches[1],10)) > 0)) {
                        isExpandedTr = true;
                        break;
                    }
                }
            } else if (node.nodeName === 'TR') {
                matches = ((node.id || '').match(/^tr_contract_id_(\d+)$/));
                if (matches && ((contractId = parseInt(matches[1],10)) > 0)) {
                    isTr = true;
                }
            } else if (node.nodeName === 'I') {
                matches = ((node.id || '').match(/^options_contract_id_(\d+)$/));
                if (matches && ((contractId = parseInt(matches[1],10)) > 0)) {
                    isTrOptions = true;
                }
            }
        }

        if (isTr || isExpandedTr || isTrOptions) {

            // Un-highlight all rows. MyApp.currentContractId has already been updated so cannot check its value here.
            $(table.fnSettings().aoData).each(function (){
                $(this.nTr).removeClass('row_selected');
            });
            setCurrentContract(contractId);

            // Highlight the current row (if any).
            if (contractId) {
                $("#tr_contract_id_" + contractId).addClass('row_selected');
            }
        }
    });
    MyTimer = window.setInterval(function() {
        //console.log("interval timer isLoggedIn=" + MyApp.isLoggedIn + " searchTabActive=" + MyApp.isSearchTabActive);
        if (MyApp.isLoggedIn && MyApp.isSearchTabActive) {
            var lastRefresh = MyApp.lastRefreshTime;
            var now = new Date().getTime() / 1000;
            console.log("interval timer last refresh=" + (now - lastRefresh) + " secs ago");
            if (now - lastRefresh > 50) {
                MyApp.lastRefreshTime = now;
                refreshSearchTable();
            }
        }

    }, 60000);
}


function updatePopovers() {
    $('.table-popover').each(function() {
        $(this).popover({
            html: true,
            trigger: 'manual'
        }).mouseover(function(e) {
                $(this).popover('show');
                MyApp.isVisible = true;
                //e.preventDefault();
            }).mouseout(function(e) {
                $(this).popover('hide');
                MyApp.isVisible = false;
                //e.preventDefault();
                /*
                 }).click(function(e) {
                 $(this).popover('hide');
                 MyApp.isVisible = false;
                 //e.preventDefault();
                 */
            })
    });
    $(document).click(function(e) {
        if (MyApp.isVisible & MyApp.clickedAway) {
            $('.table-popover').each(function() {
                $(this).popover('hide');
            })  ;
        } else {
            MyApp.clickedAway = false;
        }
    });
}

function noOp() {
    // does nothing.
}

function toggleBookmark(item, contractId) {
    if (MyApp.simpleBookmarks[contractId]) {
        sendServerRequest({ url:'simple_bookmarks/remove', sync: true, data: { "userId" : MyApp.currentUserId, "contractId": contractId}, onSuccess: "noOp" });
        delete MyApp.simpleBookmarks[contractId];
    } else {
        sendServerRequest({ url:'simple_bookmarks/add', sync: true, data: { "userId" : MyApp.currentUserId, "contractId": contractId}, onSuccess: "noOp" });
        MyApp.simpleBookmarks[contractId] = 1;
    }
    $(item).trigger('mouseout');
    $('#td_contract_id_' + contractId).html(renderIconCell(undefined,contractId));
    updatePopovers();
    return false;
}


function toggleContractOptions(item, contractId) {
    var nTd = item.parentNode.parentNode.parentNode.parentNode;
    var nTr = nTd.parentNode;


    var oTable = $(MyApp.hashContractTableId).dataTable();
    var aPosTr = oTable.fnGetPosition(nTr);
    var aPosTd = oTable.fnGetPosition(nTd);
    var oSettings = oTable.fnSettings();

    var aData = oTable.fnGetData( aPosTd[0] );
    var isOpen=0;
    var isHideRow = 0;

    if (item.innerHTML.match(/icon\-minus/)) {
        // Currently open, close it.
        item.innerHTML = '<i class="icon-plus-sign"></i>';

        isHideRow = !filterRow( undefined, aData, undefined );
        oTable.fnClose( nTr );
        if (isHideRow) {
            // unset current contract;
            setCurrentContract(undefined);
        }
    } else {
        // Currently closed, open it.
        item.innerHTML = '<i class="icon-minus-sign"></i>';
        ++isOpen;
        setCurrentContract(contractId);
        // Note: 3rd param to fnOpen is class to apply (not
        var v2= oTable.fnOpen( nTr, fnFormatDetails(contractId), 'tr_sub_contract_id_' + contractId );
        resetErPostBookmarkForm();
        adjustCurrentContractOptionsHeight();
    }
    nTd.innerHTML = renderIconCell(isOpen,contractId);
    //oTable.fnUpdate('xxx',this.parentNode,0,1,1);
    // May have affected filter and so require redraw.
    if (isHideRow) {
        var oSettings = oTable.fnSettings();
        var iStart = oSettings._iDisplayStart;
        oTable.fnDraw();
        oSettings = oTable.fnSettings();
        oSettings._iDisplayStart = iStart;
        oSettings.oApi._fnCalculateEnd( oSettings );
        oSettings.oApi._fnDraw( oSettings );

    }
    // fnUpdate(obj|arr|str, node|int, isRedrawTable, isDoPredraw);

}

function printStackTraceORIG() {
    var callstack = [];
    var isCallstackPopulated = false;
    try {
        i.dont.exist+=0; //doesn't exist- that's the point
    } catch(e) {
        if (e.stack) { //Firefox
            var lines = e.stack.split('\n');
            for (var i=0, len=lines.length; i<len; i++) {
                if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
                    callstack.push(lines[i]);
                }
            }
            //Remove call to printStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        }
        else if (window.opera && e.message) { //Opera
            var lines = e.message.split('\n');
            for (var i=0, len=lines.length; i<len; i++) {
                if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
                    var entry = lines[i];
                    //Append next line also since it has the file info
                    if (lines[i+1]) {
                        entry += ' at ' + lines[i+1];
                        i++;
                    }
                    callstack.push(entry);
                }
            }
            //Remove call to printStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        }
    }
    if (!isCallstackPopulated) { //IE and Safari
        var currentFunction = arguments.callee.caller;
        while (currentFunction) {
            var fn = currentFunction.toString();
            var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf('')) || 'anonymous';
            callstack.push(fname);
            currentFunction = currentFunction.caller;
        }
    }
    output(callstack);
}

function printStackTrace(e) {
    var callstack = [];
    var isCallstackPopulated = false;

    if (e.stack) { //Firefox
        var lines = e.stack.split('\n');
        for (var i=0, len=lines.length; i<len; i++) {
            if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\s*\(/)) {
                callstack.push(lines[i]);
            }
        }
        //Remove call to printStackTrace()
        callstack.shift();
        isCallstackPopulated = true;
    }
    else if (window.opera && e.message) { //Opera
        var lines = e.message.split('\n');
        for (var i=0, len=lines.length; i<len; i++) {
            if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\s*\(/)) {
                var entry = lines[i];
                //Append next line also since it has the file info
                if (lines[i+1]) {
                    entry += ' at ' + lines[i+1];
                    i++;
                }
                callstack.push(entry);
            }
        }
        //Remove call to printStackTrace()
        callstack.shift();
        isCallstackPopulated = true;
    }

    if (!isCallstackPopulated) { //IE and Safari
        var currentFunction = arguments.callee.caller;
        while (currentFunction) {
            var fn = currentFunction.toString();
            var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf('')) || 'anonymous';
            callstack.push(fname);
            currentFunction = currentFunction.caller;
        }
    }
    return callstack;
}

function output(arr) {
    //Optput however you want
    alert(arr.join('\n\n'));
}

function getStackTrace(e) {
    if (e.stack) {
        return e.stack;
    }
    if (window.opera && e.message) {
        return e.message;
    }
    var currentFunction = arguments.callee.caller;
    var callstack = [];
    while (currentFunction) {
        var fn = currentFunction.toString();
        var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf('')) || 'anonymous';
        callstack.push(fname);
        currentFunction = currentFunction.caller;
    }
    return callstack.join("\n");
}


function remoteLog(options) {
    if (options.hasOwnProperty("e")) {
        var event = getStackTrace(options.e);
        options.event = event;
    }
    try {
        sendServerRequestNoWrapper({ url:'events/notify', sync: true, data: options, onComplete: function (o, textStatus) {
    }});
    } catch(e) {
console.log(" sendServerRequestNoWrapper failed with exception " + e.message);
    }
}

function getSearchTableRowIndex(options) {
    var contractId = options.contractId;


    var nodes = options.table.fnGetNodes();
    var index;
    for(index = nodes.length-1;index > -1 ; --index) {
        var id = nodes[index].id.substr(15);
        if (id == contractId) {
            break;
        }
    }
    return(index);
}


function refreshSearchTable() {
    var data = { 'timestamp' : MyApp.lastDataUpdateTimestamp};
    try {
    sendServerRequest({ url:'app_caches', sync: true, data: data, onComplete: function (o, textStatus) {

        //alert("JSON response contracts/update: " + textStatus);
        if (textStatus == "success")
            var data = $.parseJSON(o.responseText);
            var contractsAffected = {};
            loadAppCache(data, contractsAffected);
            var table = $(MyApp.hashContractTableId).dataTable();
        var cnt = 0;

            $.each(contractsAffected, function(key,value){
                var contract = MyApp.contracts.find(key);
                if (contract) {
                    var pos = getSearchTableRowIndex({table: table, contractId: key });
                    var isRedrawTable = false;

                    if (pos > -1) {
                        table.fnUpdate(contract,pos,0, isRedrawTable);
                    } else {
                        table.dataTable().fnAddData(contract, isRedrawTable);
                    }
                    ++cnt;
                }

            });
        if(cnt) {
            redrawSearchTable(table);
        }

        }
    } );
    } catch(e) {
        alert("refreshSearchTable error: " + e);
    }
}


function redrawSearchTable(oTable) {

    if (!oTable) {
        oTable = $(MyApp.hashContractTableId).dataTable();
    }
    var oSettings = oTable.fnSettings();
    var iStart = oSettings._iDisplayStart;
    oTable.fnDraw();
    oSettings = oTable.fnSettings();
    oSettings._iDisplayStart = iStart;
    oSettings.oApi._fnCalculateEnd( oSettings );
    oSettings.oApi._fnDraw( oSettings );

}



function deleteBookmark(contractId) {

    if (MyApp.postedBookmarks[contractId] !== undefined && MyApp.postedBookmarks[contractId] !== undefined) {
        sendServerRequest({ url:'posted_bookmarks/remove', async: true, data: { "userId" : MyApp.currentUserId, "contractId": contractId}, onSuccess: "noOp" });
        delete MyApp.postedBookmarks[contractId];
        $('#td_contract_id_' + contractId).html(renderIconCell(undefined,contractId));
    }
    return false;
}


function userBookmarkDialogue(contractId) {
    if (MyApp.postedBookmarks[contractId] === undefined) {
        return false;
    }
    var title = 'Posted bookmark';
    var content = '';

    $.each(MyApp.postedBookmarks[contractId], function(key, info){
        var user = MyApp.users.find(info.sender);

        var sender = (user === undefined) ? 'User #' + info.sender : user.name;

        content += info.timestamp.replace(/:\d{2}$/, '') + ' <em>' + sender.escapeHTML() + '</em>';

        if (info.message !== undefined) {
            content += '<br>' + info.message.escapeHTML();
        }

        content += '<br>';

    } );



    var html =
        '<div class="modal-header">'
            +    '<button type="button" class="close" data-dismiss="modal">×</button>'
            +   '<h3>'+title+'</h3>'
            +'</div>'
            +'<div class="modal-body">'
            +   content
            +'</div>'
            +'<div class="modal-footer">'
            +  '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>'
            +   '<a href="#" class="btn btn-danger" data-dismiss="modal" onclick="deleteBookmark('+contractId+');"><i class="icon-trash"></i> Delete bookmark</a>'
            +'</div>';
    $("#bookmarkModal").html(html);
    $("#bookmarkModal").modal();
}


function renderIconCell(isOpen, contractId) {
    var contract = MyApp.contracts.find(contractId);
    if (contract == undefined) {
        return '';
    }

    if (isOpen == undefined) {
        var t1 =  $("#options_contract_id_" + contractId);
        var tContent = t1[0].outerHTML;
        var isOpen = tContent.match(/icon\-minus/);
    }
    var value = '<div class="inline-list" contractid="'+ contractId +'"><p></p><ul>';


    value += '<li>'
        + '<a href="#" onclick="toggleContractOptions(this, ' + contractId + ');">'
        + (isOpen?'<i id="options_contract_id_'+contractId+'" class="icon-minus-sign"></i>' : '<i id="options_contract_id_'+contractId+'" class="icon-plus-sign"></i>')
        +'</a></li>';

    var bookmarkIconClass = 'icon-bookmark-empty';
    var bookmarkHelp = 'Not bookmarked. Click to add bookmark.';
    if (MyApp.simpleBookmarks[contractId] > 0) {
        bookmarkIconClass = 'icon-bookmark';
        bookmarkHelp = 'Bookmarked. Click to remove bookmark.';
    }
    value +=  '<li><a href="#" id="quick_bookmark_contract_id_"'+contractId+' rel="popover" class="table-popover" title="Quick bookmark" data-content="'+bookmarkHelp+'." onclick="toggleBookmark(this, ' + contractId + ');">'
        +'<i id="bookmark_contract_id_'+contractId+'" class="'+bookmarkIconClass+'"></i></a></li>';

    if (MyApp.postedBookmarks[contractId] !== undefined) {
        var title = 'Posted bookmark';
        var content = '';
        $.each(MyApp.postedBookmarks[contractId], function(key, info){
            var user = MyApp.users.find(info.sender);

            var sender = (user === undefined) ? 'User #' + info.sender : user.name;

            content += info.timestamp.replace(/:\d{2}$/, '') + ' <em>' + sender.escapeHTML() + '</em>';

            if (info.message !== undefined) {
                content += '<br>' + info.message.escapeHTML();
            }

            content += '<br>';

        } );

        // TODO: escape title and content
        //content = '<p>bo</p>';
        content = content.replace(/./g, function(m) { return sprintf("&#%02d;", m.charCodeAt(0)); })
        value += '<a href="#"  rel="popover" class="table-popover" onclick="userBookmarkDialogue(' + contractId + ',this);"  title="'+title.escapeHTML()+'" data-content="'+content.escapeHTML()+'">'
            + '<li><i id="user_bookmark_contract_id_'+contractId+'" class="icon-comment-alt"></i></a></li>';
    }
    //value += '<a href="#" class="btn btn-mini table-popover" rel="popover" title="A Title" data-content="And here is some amazing content" >hover for popover</a>';

    var n = contract.relatedDocuments.length;
    var storageRequests = [];
    var i;
    var s;
    var maxTitleLen = 27;
    var contractStorageRequest = MyApp.storageRequests[contract.documentIdentifier];

    if (contractStorageRequest) {
        s = contract.contractTitle.length <= maxTitleLen ? contract.contractTitle : (contract.contractTitle.substring(0,maxTitleLen-3) + '...');
        storageRequests.push("<i class='"+getStorageRequestIcon(contractStorageRequest)+"'></i> " +  s.escapeHTML() + '');
    }

    for(i=0; i < n; ++i) {
        var attachment = contract.relatedDocuments[i];
        var attachmentStorageRequest = MyApp.storageRequests[attachment.documentIdentifier];
        if (attachmentStorageRequest) {
            s = attachment.relatedDocumentType.length <= maxTitleLen ? attachment.relatedDocumentType : (attachment.relatedDocumentType.substring(0,maxTitleLen-3) + '...');
            storageRequests.push("<i class='"+getStorageRequestIcon(attachmentStorageRequest)+"'></i> " +  s.escapeHTML() + '');
        }
    }
    if (n > 0) {


        if (contract.isHasAddendum) {
            s = (n == 1)? 'Contract has an addendum.' : sprintf("Contract has an addendum and %d other related document%s.", (n-1), (n == 2?'':'s'));
            value += '<li><a href="#"  rel="popover" class="table-popover" title="Addendum" data-content="'+s+'"><i class="icon-paper-clip"></i>A</a></li>';
        } else {
            s = (n == 1) ? 'Contract has 1 related document.' : 'Contract has '+n+' related documents.';
            value += '<li><a href="#"  rel="popover" class="table-popover" title="Related documents" data-content="'+s+'"><i class="icon-paper-clip"></i></a></li>';
        }
    }
    /*
     var s = (n == 1) ? 'Has a related document ' : 'Has '+n+' related documents';
     //value += '<li><a href="#"  rel="popover" class="table-popover" title="Related documents" data-content="'+s+'. <br />Does not have an addendum."><i class="icon-copy"></a></li>';
     value += '<li><a href="#"  rel="popover" class="table-popover" title="Related documents" data-content="'+s+'. <br />Does not have an addendum."><i class="icon-paper-clip"></a></li>';

     }
     */
    if (storageRequests.length > 0) {
        var s = storageRequests.join('<br>');
        //s = '"name"';
        //s = s.replace(/"/g, '\\"');
        //s = "my <i class='icon-truck'></i>";
        value += '<li><a href="#"  rel="popover" class="table-popover" title="Storage request(s)" data-content="'+s+'"><i id="storage_contract_id_'+contractId+'" class="icon-truck"></i></a></li>';
    }

    value += '</ul><p></p></div>';
    return value;

}
function getStorageRequestIcon(storageRequest, isReturnBlankIcon) {
    var value = isReturnBlankIcon ? 'icon-check-empty':'';
    if (storageRequest != undefined) {
        var isOnHold = storageRequest.isOnHold;

        if (storageRequest.hasOwnProperty("isSendToStorage")) {
            if (storageRequest.isSendToStorage) {
                value = isOnHold ? 'icon-step-forward' : 'icon-signin';
            } else {
                value = isOnHold ? 'icon-step-backward' : 'icon-signout';
            }
        }
    }

    return value;
}


function renderFilterResultsCell(o, value) {
    if (value == undefined) {
        value = '';
    }

    switch (o.iDataColumn) {
        case 0:
            var id = o.aData.id;
            value = renderIconCell(0, id);
            break;

        case 10: // is convenience termination date
        case 12: // is auto renewal
        case 23: // Uses subcontractors
            if (value != '') {
                if (value === "1" || value === 1) {
                    value = 'Yes';
                }  else if (value === "0"  || value === 0) {
                    value = "No";
                }
            }
            break;
        case 29: // related documents
            var contractId = o.aData.id;
            var contract = MyApp.contracts.find(contractId);
            if (!contract) {
                contract = {};
                console.log("Invalid contractId=" + contractId + " in renderFilterResults");
            }

            var relatedDocuments = contract.relatedDocuments;
            var docIds = [];
            $.each(relatedDocuments, function(index, item) {
                docIds.push(item.fmtDocumentIdentifier);
            });
            if (docIds.length > 0) {
                value = docIds.join(":");
                //console.log("renderFilterResultsCell contract.id=" + contract.id +
                //    " documentIdentifier= " + contract.fmtDocumentIdentifier + "  Qty related docs=" + docIds.length +
                //    " related docs: " + value);
            }
            break;

        default:
            alert("renderFilterResultsCell() called for unsupported column " + o.iDataColumn);
            break;
    }
    return value;
}





function downloadSearchResults(saveDataId) {
    //getTableData();
    downloadFile('contract_search.csv', '#contract-search-column-visibility');
}


