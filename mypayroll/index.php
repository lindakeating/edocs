<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");

$SortTableFlag = true;

$sql = "SELECT * FROM payroll WHERE id = 1";
$result = mysql_query($sql);
if(!$result) {
    die("Database query failed: " . mysql_error());
}
$row = mysql_fetch_array($result);

$Client = $row["Client"];
$Entity = $row["Entity"];
$Stage1 = $row["Stage1"];
$Stage2 = $row["Stage2"];
$Stage3 = $row["Stage3"];
$Stage4 = $row["Stage4"];
$Stage5 = $row["Stage5"];
$Stage6 = $row["Stage6"];

switch ($Stage1)
{
	case 1: // Not started and not overdue
		$Stage1Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 2: // Not started and overdue
		$Stage1Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 3: // In progress and not overdue
		$Stage1Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 4: // In progress and overdue
		$Stage1Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 5: // Complete and not overdue
		$Stage1Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 6: // Complete and overdue
		$Stage1Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 7: // Halted and not overdue
		$Stage1Colour = "<span class=\"label label-sm label-sucess\"><i class=\"fa fa-warning\"></i></span>";
		break;
	case 8: // Halted and overdue
		$Stage1Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-warning\"></i></span>";
		break;
	default:
		$Stage1Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
}

switch ($Stage2)
{
	case 1: // Not started and not overdue
		$Stage2Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 2: // Not started and overdue
		$Stage2Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 3: // In progress and not overdue
		$Stage2Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 4: // In progress and overdue
		$Stage2Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 5: // Complete and not overdue
		$Stage2Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 6: // Complete and overdue
		$Stage2Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 7: // Halted and not overdue
		$Stage2Colour = "<span class=\"label label-sm label-sucess\"><i class=\"fa fa-warning\"></i></span>";
		break;
	case 8: // Halted and overdue
		$Stage2Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-warning\"></i></span>";
		break;
	default:
		$Stage2Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
}

switch ($Stage3)
{
	case 1: // Not started and not overdue
		$Stage3Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 2: // Not started and overdue
		$Stage3Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 3: // In progress and not overdue
		$Stage3Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 4: // In progress and overdue
		$Stage3Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 5: // Complete and not overdue
		$Stage3Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 6: // Complete and overdue
		$Stage3Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 7: // Halted and not overdue
		$Stage3Colour = "<span class=\"label label-sm label-sucess\"><i class=\"fa fa-warning\"></i></span>";
		break;
	case 8: // Halted and overdue
		$Stage3Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-warning\"></i></span>";
		break;
	default:
		$Stage3Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
}

switch ($Stage4)
{
	case 1: // Not started and not overdue
		$Stage4Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 2: // Not started and overdue
		$Stage4Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 3: // In progress and not overdue
		$Stage4Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 4: // In progress and overdue
		$Stage4Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 5: // Complete and not overdue
		$Stage4Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 6: // Complete and overdue
		$Stage4Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 7: // Halted and not overdue
		$Stage4Colour = "<span class=\"label label-sm label-sucess\"><i class=\"fa fa-warning\"></i></span>";
		break;
	case 8: // Halted and overdue
		$Stage4Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-warning\"></i></span>";
		break;
	default:
		$Stage4Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
}

switch ($Stage5)
{
	case 1: // Not started and not overdue
		$Stage5Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 2: // Not started and overdue
		$Stage5Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 3: // In progress and not overdue
		$Stage5Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 4: // In progress and overdue
		$Stage5Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 5: // Complete and not overdue
		$Stage5Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 6: // Complete and overdue
		$Stage5Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 7: // Halted and not overdue
		$Stage5Colour = "<span class=\"label label-sm label-sucess\"><i class=\"fa fa-warning\"></i></span>";
		break;
	case 8: // Halted and overdue
		$Stage5Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-warning\"></i></span>";
		break;
	default:
		$Stage5Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
}

switch ($Stage6)
{
	case 1: // Not started and not overdue
		$Stage6Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 2: // Not started and overdue
		$Stage6Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-times\"></i></span>";
		break;
	case 3: // In progress and not overdue
		$Stage6Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 4: // In progress and overdue
		$Stage6Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-cogs\"></i></span>";
		break;
	case 5: // Complete and not overdue
		$Stage6Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 6: // Complete and overdue
		$Stage6Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-check\"></i></span>";
		break;
	case 7: // Halted and not overdue
		$Stage6Colour = "<span class=\"label label-sm label-sucess\"><i class=\"fa fa-warning\"></i></span>";
		break;
	case 8: // Halted and overdue
		$Stage6Colour = "<span class=\"label label-sm label-danger\"><i class=\"fa fa-warning\"></i></span>";
		break;
	default:
		$Stage6Colour = "<span class=\"label label-sm label-success\"><i class=\"fa fa-check\"></i></span>";
		break;
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Payroll Processing
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-hover" id="SortTable">
					<thead>
					<tr>
						<th>
							 Client
						</th>
						<th>
							 Client Entity
						</th>
						<th>
							 Status
						</th>
						<th>
							 Frequency
						</th>
                        <th>
                            Country
                        </th>
						<th>
							 Avg Employees
						</th>
                        <th>
                           <small> Input</small>
                        </th>
                        <th>
                            <small> Processing</small>
                        </th>
						<th>
							 <small>Draft Approval</small>
						</th>
						<th>
							 <small>Finalise</small>
						</th>
						<th>
							 <small>Approval</small>
						</th>
						<th>
							 <small>Closed</small>
						</th>
						<th>
							 <small>Global Reports</small>
						</th>
						<th>
							 <small>e-Payslips Available</small>
						</th>
					</tr>
					</thead>
					<tbody>
					<tr class="odd gradeX">
						<td>
							<a href="/mypayroll/processing/?ce=Ice%20Limited">Frozen</a>
						</td>
						<td>
							 <a href="/mypayroll/processing/?ce=Ice%20Limited">Ice Ltd </a>
						</td>
						<td>
							Open
						</td>
                        <td>
                           Monthly
                        </td>
                        <td>
                            Ireland
                        </td>
                        <td>
                            50
                        </td>
						<td>
							<?php echo $Stage1Colour; ?>
						</td>
						<td>
							<?php echo $Stage2Colour; ?>
						</td>
						<td>
							<?php echo $Stage3Colour; ?>
						</td>
						<td>
							<?php echo $Stage4Colour; ?>
						</td>
						<td>
							<?php echo $Stage5Colour; ?>
						</td>
						<td>
							<?php echo $Stage6Colour; ?>
						</td>
						<td>
							<?php echo $Stage6Colour; ?>
						</td>
						<td>
							<?php echo $Stage6Colour; ?>
						</td>
					</tr>
                    <tr class="odd gradeX">
                        <td>
                            <a href="/mypayroll/processing/?ce=Ice%20Limited">Frozen</a>
                        </td>
                        <td>
                            <a href="/mypayroll/processing/?ce=Ice%20Limited">Ice Ltd</a>
                        </td>
                        <td>
                            Open
                        </td>
                        <td>
                            Monthly
                        </td>
                        <td>
                            Ireland
                        </td>
                        <td>
                            50
                        </td>
                        <td>
                            <?php echo $Stage1Colour; ?>
                        </td>
                        <td>
                            <?php echo $Stage2Colour; ?>
                        </td>
                        <td>
                            <?php echo $Stage3Colour; ?>
                        </td>
                        <td>
                            <?php echo $Stage4Colour; ?>
                        </td>
                        <td>
                            <?php echo $Stage5Colour; ?>
                        </td>
                        <td>
                            <?php echo $Stage6Colour; ?>
                        </td>
                        <td>
                            <?php echo $Stage6Colour; ?>
                        </td>
                        <td>
                            <?php echo $Stage6Colour; ?>
                        </td>
                    </tr>
					<tr class="odd gradeX">
						<td>
							<a href="/mypayroll/processing/?ce=Warm%20Limited">Frozen</a>
						</td>
						<td>
							 <a href="/mypayroll/processing/?ce=Warm%20Limited">Warm Ltd</a>
						</td>
                        <td>
                            Open
                        </td>
                        <td>
                            Monthly
                        </td>
                        <td>
                            Ireland
                        </td>
						<td>
							20
						</td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
					</tr>
					<tr class="odd gradeX">
						<td>
							<a href="/mypayroll/processing/?ce=Copper%20Limited">Frozen</a>
						</td>
						<td>
							 <a href="/mypayroll/processing/?ce=Copper%20Limited">Copper Ltd
						</td>
                        <td>
                            Open
                        </td>
                        <td>
                            Fortnightly
                        </td>
						<td>
							Ireland
						</td>
                        <td>
                            45
                        </td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
						<td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
						</td>
					</tr>
                    <tr class="odd gradeX">
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Frozen</a>
                        </td>
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Gold Ltd
                        </td>
                        <td>
                            Open
                        </td>
                        <td>
                            Monthly
                        </td>
                        <td>
                            Ireland
                        </td>
                        <td>
                            99
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Frozen</a>
                        </td>
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Hold Ltd
                        </td>
                        <td>
                            Open
                        </td>
                        <td>
                            Fortnightly
                        </td>
                        <td>
                            Ireland
                        </td>
                        <td>
                            200
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Frozen</a>
                        </td>
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Nice Ltd
                        </td>
                        <td>
                            Open
                        </td>
                        <td>
                            Weekly
                        </td>
                        <td>
                            Ireland
                        </td>
                        <td>
                            60
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-warning">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Frozen</a>
                        </td>
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Action Ltd
                        </td>
                        <td>
                            Open
                        </td>
                        <td>
                            Fortnightly
                        </td>
                        <td>
                            Ireland
                        </td>
                        <td>
                            45
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-warning">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-warning">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Frozen</a>
                        </td>
                        <td>
                            <a href="/mypayroll/processing/?ce=Copper%20Limited">Bold Ltd
                        </td>
                        <td>
                            Open
                        </td>
                        <td>
                            Monthly
                        </td>
                        <td>
                            Ireland
                        </td>
                        <td>
                            80
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-success">
								 <i class="fa fa-check"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-warning">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-warning">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                        <td>
							 <span class="label label-sm label-danger">
								 <i class="fa fa-warning"></i>
							</span>
                        </td>
                    </tr>
					</tbody>
				</table>		
			</div>
		</div>
		<!-- END PAGE CONTENT-->
		<a href="reset.php" class="btn green btn-xs">(reset)</a>
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>