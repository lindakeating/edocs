<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
include($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/client/1/findFiles.php');

$UploadFlag = true;
$timeLineFlag = true;


$sql = "SELECT * FROM payroll WHERE id = 1";
$result = mysql_query($sql);
if(!$result) {
    die("Database query failed: " . mysql_error());
}
$row = mysql_fetch_array($result);

$Client = $row["Client"];
$Entity = $row["Entity"];
$Stage1 = $row["Stage1"];
$Stage2 = $row["Stage2"];
$Stage3 = $row["Stage3"];
$Stage4 = $row["Stage4"];
$Stage5 = $row["Stage5"];
$Stage6 = $row["Stage6"];

$Stage1Owner = $row["Stage1Owner"];
$Stage2Owner = $row["Stage2Owner"];
$Stage3Owner = $row["Stage3Owner"];
$Stage4Owner = $row["Stage4Owner"];
$Stage5Owner = $row["Stage5Owner"];
$Stage6Owner = $row["Stage6Owner"];

include($_SERVER['DOCUMENT_ROOT'] . "/includes/stagecolours.php");

if($Stage4 >= 5)
{
	header("location:../5/");
	exit();
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Payroll Processing | <?php echo $Entity; ?> | IE
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<ul class="timeline">
						<li class="timeline-<?php echo $Stage1Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime = new DateTime();
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cloud-upload iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading" >Inputting | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<h1> Input Complete </h1>
									<table class="table uploadFiles">
										<tr>
											<th> Inputted Files</th>
											<th> File Type </th>
											<th> Time Submitted </th>
											<th> Downloaded By </th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `files`");
											while($row = mysql_fetch_array($result)){
												$endtime = strtotime( $row['date_time']) + 600;
												echo "<tr>
														<td><a href=\"http://localhost/mypayroll/processing/client/1/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td>
														<td>".$row['description']."</td>
														<td>".$row['date_time']."</td>
														<td>Mary Moore</td>
														<td>".date('Y-m-d H:i:s',$endtime )."</td>
													</tr>";
											}									
										?>
									</table>
									<?php echo $row["Stage1Comments"]; ?>
								</div>
							</div>
						</li>
						<!-- /.modal -->
							<div class="modal fade" id="fullapproval" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Full Approval</h4>
										</div>
										<div class="modal-body">
											 <p>You have approved the draft payroll file with no exceptions, are you sure you happy for this file to be finalised?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<a class="btn green" href="../5/">Yes</a>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							<!-- /.modal -->
							<div class="modal fade" id="qualifiedapproval" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Qualified Approval</h4>
										</div>
										<div class="modal-body">
											 <p>You have approved with qualification.  Your qualification comment will be processed  and final payroll output will be returned to you.</p>
											 <p>Are you happy to issue qualified approval?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<a class="btn yellow" href="../5/">Yes</a>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							<!-- /.modal -->
							<div class="modal fade" id="reject" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Reject</h4>
										</div>
										<div class="modal-body">
											 <p>You have rejected the draft payroll and a new input file is expected before further action can be taken to process the payroll.  <strong>Please note it is <span class="text-danger">5 Days to Pay Day</span>.</strong>
												 
											 </p> <p>Do you wish to proceed?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<a class="btn red" href="/mypayroll/">Yes</a>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
						<li class="timeline-<?php echo $Stage2Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cogs iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Processing | By TMF</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<h1>Processing Complete</h1>
									
									<table class="table uploadFiles ">
										<tr>
											<th> TMF Draft Payroll Files</th>
											<th> Status </th>
											<th> Submitted By </th>
											<th> Downloaded By </th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `tmf_2_files`");
										while($row = mysql_fetch_array($result)){
											$endtime = strtotime( $row['date_time']) + 600;
											echo "<tr>
													<td><a href=\"http://localhost/mypayroll/processing/tmf/2/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td>
													<td>".$row['description']."</td>
													<td>".$row['date_time']."</td>
													<td>Mary Moore</td>
													<td>".date('Y-m-d H:i:s',$endtime )."</td>
												</tr>";																						
											}
											?>
									</table>
									<?php echo $row["Stage2Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage3Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P2D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-eye iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Approval | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>	
								<div class="timeline-content hiddenTimeline">
									<table class="table ">
										<tr>
											<th> Type </th>
											<th> Rejection Reason</th>
											<th> Employees Affected</th>
											<th> Comments</th>
											<th> Files </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `reject_client_3`");
										while($row = mysql_fetch_array($result)){
											echo "<tr>
													<td>".$row['type']."</td>
													<td>".$row['reason']."</td>
													<td>".$row['no_employees']."</td>
													<td>".$row['comments']."</td>
													<td><a href=\"#\" class=\"btn default\"><i class=\"fa fa-file-o\"></i></a></td>
												</tr>";																						
											}
											?>
									</table>
									<?php echo $row["Stage3Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage4Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-check iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Finalising | By TMF</h2>
								<?php if($Stage4Owner == 1) { ?>
								<div class="timeline-content">
									<div class="form-body">
										<form class="form-horizontal" role="form">
										<div class="form-group">
											<label class="col-md-3 control-label">Gross to Net</label>
											<div class="col-md-9">
												<a class="btn default" data-toggle="modal" href="#download"><i class="fa fa-cloud-download"></i> Download</a>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">GL Jornal</label>
											<div class="col-md-9">
												<a class="btn default" data-toggle="modal" href="#download"><i class="fa fa-cloud-download"></i> Download</a>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Payslip PDFs</label>
											<div class="col-md-9">
												<a class="btn default" data-toggle="modal" href="#download"><i class="fa fa-cloud-download"></i> Download</a>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Global Report Payout</label>
											<div class="col-md-9">
												<a class="btn default" data-toggle="modal" href="#download"><i class="fa fa-cloud-download"></i> Download</a>
											</div>
										</div>
										</form>
									</div>
									<div class="form-body">
										<form class="form-horizontal" role="form">
										<div class="form-group">
											<label class="col-md-9 control-label">Total No. of Employees Processed in Period</label>
											<label class="col-md-1 control-label">76</label>
										</div>
										<div class="form-group">
											<label class="col-md-9 control-label">No of Starters in the period </label>
											<label class="col-md-1 control-label">4</label>
										</div>
										<div class="form-group">
											<label class="col-md-9 control-label">No of Leavers in the Period</label>
											<label class="col-md-1 control-label">2</label>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label"></label>
											<div class="col-md-9">
												<a class="btn green" href="../5/"><i class="fa fa-thumbs-o-up"></i> Submit Final Payroll for Client Approval</a>
											</div>
										</div>
										</form>
									</div>
								</div>
								<?php } else { ?>
								<div class="timeline-content">
									<h1>Waiting for TMF</h1>
								</div>
								<?php } ?>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage5Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-thumbs-o-up iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Final Output Acceptance | By Client</h2>
								<div class="timeline-content">
									<?php echo $row["Stage5Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage6Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-lock iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Period Closed</h2>
								<div class="timeline-content">
									<?php echo $row["Stage6Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-bar-chart-o iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Reporting Available</h2>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-money iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Payslips Available</h2>
							</div>
						</li>
					</ul>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>