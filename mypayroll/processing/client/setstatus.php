<?php
// Create connection
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");

// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$stage = $_GET["sg"];
$state = $_GET["st"];
$owner = $_GET["so"];
$nextstage = $_GET["nsg"];
$nextstate = $_GET["nst"];
$nextowner = $_GET["nso"];

$pstage = (int)$_POST["sg"];
$pstate = (int)$_POST["st"];
$powner = (int)$_POST["so"];
$reason = $_POST["reason"];
if(isset($_POST['rCode'])){
	$rcode = $_POST['rCode'];
}

$comments = $_POST["comments"];
if(isset($stage))
{
	$sql = "UPDATE payroll SET Stage" . $stage . " = " . $state . ", Stage" . $stage . "Owner = " . $owner . "";
	$result = mysql_query($sql);

	
	
	if(isset($nextstage))
	{
		$sql = "UPDATE payroll SET Stage" . $nextstage . " = " . $nextstate . ", Stage" . $nextstage . "Owner = " . $nextowner . ";";
		$result = mysql_query($sql);
	}
}
if($pstage == 5 && $pstate == 1)
	{
	
	$sql = "UPDATE payroll SET Stage4 = 3 , Stage5 = 2 , Stage4Owner = 2, Stage5Comments = '". $comments. "' WHERE id = 1 ;";
	$result = mysql_query($sql) or die(mysql_error()."update failed");
		
	header("location:./4/");
	exit();
	
	}
if($pstage == 3 && $pstate == 7)
{	

	
	if($reason == 1)
	{
		$message = "<blockquote><p>Rejected: Reason, New Input Required</p></blockquote>";
	}
	else
	{
		$message = "<blockquote><p>Rejected: Reason, Input interpretation</p></blockquote>";
	}
	$message .= "<blockquote><p>$comments</p></blockquote>";
	
	$sql = "UPDATE payroll SET Stage1 = 1, Stage3 = 4, Stage3Comments = '" . $message . "' WHERE id = 1";
	$result = mysql_query($sql);
	
	if($reason == 1)
	{
		// get rid of all the files in the client/1/ directory so they don't show when client is redirected
		$files = glob($_SERVER['DOCUMENT_ROOT'].'/mypayroll/processing/client/1/files/*');

			foreach($files as $file){ // iterate files
				$leavers = substr($file, strrpos($file, '/'), strlen($file));
				
				if(is_file($file) && $leavers != "/1401463427.2591SS-Changes.xlsx")
				unlink($file); // delete file
			}
			
		// get rid of all the files in the client/1/ directory so they don't show when client is redirected
		$files = glob($_SERVER['DOCUMENT_ROOT'].'/mypayroll/processing/tmf/2/files/*');

			foreach($files as $file){ // iterate files
				$leavers = substr($file, strrpos($file, '/'), strlen($file));
				
				if(is_file($file) && $leavers != "/1401463427.2591SS-Changes.xlsx")
				unlink($file); // delete file
			}
			
		// update the status of each stage
		$sql = "UPDATE payroll SET Stage1 = 1, Stage1Owner = 1, Stage2 = 1, Stage3 = 7, Stage4 = 1, Stage5 = 1, Stage6 = 1";
		$result = mysql_query($sql);
		
		// insert into the database the reason for rejection
		$sql = "INSERT into reject_client_3 (reason, no_employees, comments, type) VALUES ('Inputting Error', '".$_POST['employeesImpacted']."' , '".$_POST['comments']."' , '<span class=\"label label-sm label-danger\">Full Rejection</span>' );";
		$result = mysql_query($sql) or die(mysql_error()."failed");
		
		header("location:./1/");
		exit();
	}
	else
	{
		// get rid of all the files in the client/1/ directory so they don't show when client is redirected
		$files = glob($_SERVER['DOCUMENT_ROOT'].'/mypayroll/processing/client/1/files/*');

			foreach($files as $file){ // iterate files
				$leavers = substr($file, strrpos($file, '/'), strlen($file));
				
				if(is_file($file) && $leavers != "/1401463427.2591SS-Changes.xlsx")
				unlink($file); // delete file
			}
	
		// get rid of all the files in the client/1/ directory so they don't show when client is redirected
		$files = glob($_SERVER['DOCUMENT_ROOT'].'/mypayroll/processing/tmf/2/files/*');

			foreach($files as $file){ // iterate files
				$leavers = substr($file, strrpos($file, '/'), strlen($file));
				
				if(is_file($file) && $leavers != "/1401463427.2591SS-Changes.xlsx")
				unlink($file); // delete file
			}
			
		$sql = "UPDATE payroll SET Stage2 = 3, Stage2Owner = 2, Stage3 = 7";
		$result = mysql_query($sql);
		
		$sql = "INSERT into reject_client_3 (reason, no_employees, comments, type) VALUES ('Processing Error', '".$_POST['employeesImpacted']."' , '".$_POST['comments']."' , '<span class=\"label label-sm label-danger\">Full Rejection</span>');";
		$result = mysql_query($sql) or die(mysql_error()."failed");
		
		header("location:./2/");
		exit();
	}
}

if($pstage == 3 && $pstate == 6)
{	

	$sql = "INSERT into reject_client_3 (reason, no_employees, comments, type) VALUES ('".$_POST['rCode']."', '".$_POST['employeesImpacted']."' , '".$_POST['comments']."' , '<span class=\"label label-sm label-warning\">Qualified Approval</span>');";
	$result = mysql_query($sql) or die(mysql_error()."failed");
	
	$message = "<h3>Qualified Approval</h3>";
	$message .= "<blockquote><p>$rcode</p></blockquote>";
	$message .= "<blockquote><p>$comments</p></blockquote>";
	
	$sql = "UPDATE payroll SET Stage3 = 6, Stage4 = 3, Stage4Owner = 2, Stage3Comments = '" . $message . "'";
	$result = mysql_query($sql);
		
	header("location:./4/");
	exit();
}

if($pstage == 5 && $pstate == 7)
{	
	$message = "<h3>Rejected:</h3>";
	$message .= "<p>$comments</p>";
	
	$sql = "UPDATE payroll SET Stage5 = 7, Stage6 = 7, Stage5Comments = '" . $message . "' WHERE id = 1";
	$result = mysql_query($sql);
		
	header("location:./6/");
	exit();
}

if(isset($nextstage))
{
	header("location:./" . $nextstage . "/");
}
else
{
	header("location:./" . $stage . "/");
}
?>