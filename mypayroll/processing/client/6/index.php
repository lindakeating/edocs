<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
include($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/client/1/findFiles.php');

$timeLineFlag = true;
$UploadFlag = true;

$sql = "SELECT * FROM payroll WHERE id = 1";
$result = mysql_query($sql);
if(!$result) {
    die("Database query failed: " . mysql_error());
}
$row = mysql_fetch_array($result);

$Client = $row["Client"];
$Entity = $row["Entity"];
$Stage1 = $row["Stage1"];
$Stage2 = $row["Stage2"];
$Stage3 = $row["Stage3"];
$Stage4 = $row["Stage4"];
$Stage5 = $row["Stage5"];
$Stage6 = $row["Stage6"];

$Stage1Owner = $row["Stage1Owner"];
$Stage2Owner = $row["Stage2Owner"];
$Stage3Owner = $row["Stage3Owner"];
$Stage4Owner = $row["Stage4Owner"];
$Stage5Owner = $row["Stage5Owner"];
$Stage6Owner = $row["Stage6Owner"];

include($_SERVER['DOCUMENT_ROOT'] . "/includes/stagecolours.php");
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Payroll Processing | <?php echo $Entity; ?> | IE
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<ul class="timeline">
						<li class="timeline-<?php echo $Stage1Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime = new DateTime();
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cloud-upload iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Inputting | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>	
								<div class="timeline-content hiddenTimeline">
									<h1> Input Complete </h1>
									<table class="table uploadFiles">
										<tr>
											<th> Inputted Files</th>
											<th> Upload Time </th>
											<th> Submitted</th>
											<th> Downloaded</th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$array=drawArray(new DirectoryIterator($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/client/1/files'));
										//var_dump($array);
										foreach($array as $f=>$f_value){
											echo "<tr><td>".substr($f_value, 15, strlen($f_value))."</td><td>".substr(gmdate('r' ,substr($f_value, 0, 10)), 0, -5)."</td><td>Mary Moore</td><td>Frank Henry</td><td>".substr(gmdate('r' ,substr($f_value, 0, 10)), 0, -5)."</td></tr>";									
										}
										?>
									</table
									<?php echo $row["Stage1Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage2Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cogs iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Processing | By TMF</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<h1>Processing Complete</h1>
									<table class="table ">
										<tr>
											<th> Inputted Files</th>
											<th> Upload Time </th>
											<th> Forwarded </th>
											<th> Approved</th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$array=drawArray(new DirectoryIterator($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/tmf/2/files'));
										//var_dump($array);
										foreach($array as $f=>$f_value){
											echo "<tr><td>".substr($f_value, 15, strlen($f_value))."</td><td>".substr(gmdate('r' ,substr($f_value, 0, 10)), 0, -5)."</td><td>Frank Henry</td><td>Sarah Parsons</td><td>".substr(gmdate('r' ,substr($f_value, 0, 10)), 0, -5)."</td></tr>";									
										}
										?>
									</table>
									<?php echo $row["Stage2Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage3Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P2D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-eye iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Approval | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<?php echo $row["Stage3Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage4Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-check iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Finalising | By TMF</h2>
								<i class="fa fa-chevron-down time-line-control"></i>					
								<div class="timeline-content hiddenTimeline">
									<h1>Finalising Complete</h1>
									<table class="table ">
										<tr>
											<th> Inputted Files</th>
											<th> Upload Time </th>
											<th> Forwarded </th>
											<th> Approved</th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$array=drawArray(new DirectoryIterator($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/tmf/4/files'));
										//var_dump($array);
										foreach($array as $f=>$f_value){
											echo "<tr><td>".substr($f_value, 15, strlen($f_value))."</td><td>".substr(gmdate('r' ,substr($f_value, 0, 10)), 0, -5)."</td><td>Frank Henry</td><td>Sarah Parsons</td><td>".substr(gmdate('r' ,substr($f_value, 0, 10)), 0, -5)."</td></tr>";									
										}
										?>
									</table>
									<?php echo $row["Stage4Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage5Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-thumbs-o-up iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Final Output Acceptance | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<?php echo $row["Stage5Comments"]; 
									if($Stage5 == 7){
										echo "<a href=\"#\" class=\"btn btn-lg default pull-right\">
													<i class=\"fa fa-ticket\"></i> View Ticket
												</a>";								
									}
									?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage6Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-lock iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Period Closed</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content ">
								<?php if($Stage5 == 7) { ?>	
									<p>The payroll is closed but was rejected at final stage and is being resolved through ticketing system.</p>
									<?php echo $row["Stage5Comments"]; ?>								
									
								<?php } else { ?>
									<p>There is no further activity status of Payroll Processing is now closed.</p>
								</div>
								<?php } ?>
							</div>
						</li>
						<li class="timeline-yellow">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-bar-chart-o iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Reporting Available</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<?php if($Stage5 != 7){ ?>
										<p>Global reporting for this period will be available on </p>
									 <?php
									echo $datetime->format("F d");
									 } else { ?>
									 <p> global reporting is not available because the final outputs have not been accepted  </p>
									<?php } ?>
								</div>
							</div>
						</li>
						<li class="timeline-yellow">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-money iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Payslips Available</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline ">								
									<?php if($Stage5 == 7) { ?>	
										<p>Payslips are not available for download as a ticket was raised at final output acceptance</p>									
										
									<?php } else { ?>
											<p>Payslips will be available for viewing on <?php echo $datetime->format("F d"); ?></p>							
									<?php } ?>
								</div>
							</div>
						</li>
					</ul>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>