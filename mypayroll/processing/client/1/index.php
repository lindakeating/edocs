<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
include('findFiles.php');

$UploadFlag = true;
$timeLineFlag = true;


$_SESSION['uploadDir'] = getcwd();

$sql = "SELECT * FROM payroll";
$result = mysql_query($sql);
if(!$result) {
    die("Database query failed: " . mysql_error());
}
$row = mysql_fetch_array($result);

$Client = $row["Client"];
$Entity = $row["Entity"];
$Stage1 = $row["Stage1"];
$Stage2 = $row["Stage2"];
$Stage3 = $row["Stage3"];
$Stage4 = $row["Stage4"];
$Stage5 = $row["Stage5"];
$Stage6 = $row["Stage6"];

$Stage1Owner = $row["Stage1Owner"];
$Stage2Owner = $row["Stage2Owner"];
$Stage3Owner = $row["Stage3Owner"];
$Stage4Owner = $row["Stage4Owner"];
$Stage5Owner = $row["Stage5Owner"];
$Stage6Owner = $row["Stage6Owner"];

include($_SERVER['DOCUMENT_ROOT'] . "/includes/stagecolours.php");

if($Stage1 >= 5)
{
	header("location:../2/");
	exit();
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Payroll Processing | <?php echo $Entity; ?> | IE
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="modal fade" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"></h4>
				</div>
				<form>
					<div class="modal-body">
					</div>
					<div class="modal-footer">
						<!--<button type="button" class="btn btn-default" data-dismiss="modal">No. Go back.</button>
						<button type="button" class="btn btn-primary" ><a href="../setstatus.php?sg=1&st=3&so=2">Confirm</a></button> -->
					</div>
				</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<ul class="timeline">
						<li class="timeline-<?php echo $Stage1Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime = new DateTime();
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cloud-upload iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Inputting | By Client</h2>
								<?php if($Stage1Owner == 1) { ?>
								<div class="timeline-content">
									<blockquote>
										<p style="font-size:16px">
											 Please upload your payroll files
										</p>
									</blockquote>
									<br>
									<form id="fileupload" action="/assets/plugins/jquery-file-upload/server/php/UploadHandler.php" method="POST" enctype="multipart/form-data">
										<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
										<div class="row fileupload-buttonbar">
											<div class="col-lg-7">
												<!-- The fileinput-button span is used to style the file input field as button -->
												<span class="btn green fileinput-button">
													<i class="fa fa-plus"></i>
													<span>
														 Add files...
													</span>
													<input type="file" name="files[]" multiple="">
												</span>
												<!-- The global file processing state -->
												<span class="fileupload-process">
												</span>
											</div>
											<!-- The global progress information -->
											<div class="col-lg-5 fileupload-progress fade">
												<!-- The global progress bar -->
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
													<div class="progress-bar progress-bar-success" style="width:0%;">
													</div>
												</div>
												<!-- The extended global progress information -->
												<div class="progress-extended">
													 &nbsp;
												</div>
											</div>
										</div>
										<!-- The table listing the files available for upload/download -->
										<div class="well">
											<table role="presentation" class="table clearfix" >
												<tbody class="files">
												</tbody>
											</table>
										</div>
									</form>
									<a id="submitFiles" class="btn default"  ><i class="fa fa-cloud-upload"></i> Submit files for processing</a>
								</div>
								<?php } else { 
								
								?>
								<div class="timeline-content">
									<h1>Input Complete</h1>
										<table class="table uploadFiles ">
											<tr>
												<th> Inputted Files</th>
												<th> File Type </th>
												<th> Upload Time </th>
												<th> Uploaded By </th>
											</tr>
											<tr><td>SS-Changes</td><td>SS-changes</td><td>2014-06-03 12:47:37</td><td>Self Service Portal</td></tr>
											<?php 
											$result = mysql_query("SELECT * FROM `files`");
											while($row = mysql_fetch_array($result)){
												echo "<tr><td><a href=\"http://localhost/mypayroll/processing/client/1/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td><td>".$row['description']."</td><td>".$row['date_time']."</td><td>Mary Moore</td></tr>";																						
											}
											?>
										</table>
								</div>
								<?php } ?>
							</div>
						</li>
						<!-- /.modal -->
							<div class="modal fade" id="warning" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Warning</h4>
										</div>
										<div class="modal-body">
											 <p>You are submitting this file without  any leavers or change requests.</p>
											 <p>Are you sure you want to proceed?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<a class="btn blue" href="../2/">Yes</a>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->

						<li class="timeline-<?php echo $Stage2Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cogs iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Processing | By TMF</h2>
								<div class="timeline-content">
								<?php echo $row["Stage2Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage3Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P2D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-eye iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Approval | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>	
								<div class="timeline-content hiddenTimeline">
									<table class="table uploadFiles">
										<tr>
											<th> Type </th>
											<th> Rejection Reason</th>
											<th> Employees Affected</th>
											<th> Comments</th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `reject_client_3`");
										while($row = mysql_fetch_array($result)){
											echo "<tr>
													<td>".$row['type']."</td>
													<td>".$row['reason']."</td>
													<td>".$row['no_employees']."</td>
													<td>".$row['comments']."</td>
												</tr>";																						
											}
											?>
									</table>
								<?php echo $row["Stage3Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage4Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-check iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Finalising | By TMF</h2>
								<div class="timeline-content">
								<?php echo $row["Stage4Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage5Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-thumbs-o-up iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Final Output Acceptance | By Client</h2>
								<div class="timeline-content">
								<?php echo $row["Stage5Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage6Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-lock iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Period Closed</h2>
								<div class="timeline-content">
								<?php echo $row["Stage6Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-bar-chart-o iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Reporting Available</h2>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-money iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Payslips Available</h2>
							</div>
						</li>
					</ul>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->

<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php");  ?>