<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
include($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/client/1/findFiles.php');

$UploadFlag = true;
$timeLineFlag = true;


$sql = "SELECT * FROM payroll WHERE id = 1";
$result = mysql_query($sql);
if(!$result) {
    die("Database query failed: " . mysql_error());
}
$row = mysql_fetch_array($result);

$Client = $row["Client"];
$Entity = $row["Entity"];
$Stage1 = $row["Stage1"];
$Stage2 = $row["Stage2"];
$Stage3 = $row["Stage3"];
$Stage4 = $row["Stage4"];
$Stage5 = $row["Stage5"];
$Stage6 = $row["Stage6"];

$Stage1Owner = $row["Stage1Owner"];
$Stage2Owner = $row["Stage2Owner"];
$Stage3Owner = $row["Stage3Owner"];
$Stage4Owner = $row["Stage4Owner"];
$Stage5Owner = $row["Stage5Owner"];
$Stage6Owner = $row["Stage6Owner"];

include($_SERVER['DOCUMENT_ROOT'] . "/includes/stagecolours.php");

if($Stage3 >= 5)
{
	header("location:../4/");
	exit();
}
?>

<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Payroll Processing | <?php echo $Entity; ?> | IE
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<ul class="timeline">
						<li class="timeline-<?php echo $Stage1Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime = new DateTime();
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cloud-upload iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Inputting | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<h1>Input Complete</h1>
									<table class="table uploadFiles">
										<tr>
											<th> Inputted Files</th>
											<th> File Type </th>
											<th> Submitted By </th>
											<th> Time Submitted </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `files`");
											while($row = mysql_fetch_array($result)){
												$endtime = strtotime( $row['date_time']) + 600;
												echo "<tr>
														<td><a href=\"http://localhost/mypayroll/processing/client/1/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td>
														<td>".$row['description']."</td>
														<td>Mary Moore</td>
														<td>".date('Y-m-d H:i:s',$endtime )."</td>
													</tr>";
											}									
										?>
									</table>
									<?php echo $row["Stage3Comments"]; ?>
								</div>
							</div>
						</li>
						<!-- /.modal -->
							<div class="modal fade" id="fullapproval" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Full Approval</h4>
										</div>
										<div class="modal-body">
											 <p>You have approved the draft payroll file with no exceptions, are you sure you happy for this file to be finalised?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<a class="btn green" href="../setstatus.php?sg=3&st=5&so=1&nsg=4&nst=3&nso=2">Yes</a>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							<!-- /.modal -->
							<div class="modal fade" id="qualifiedapproval" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Qualified Approval</h4>
										</div>
										<form class="form-horizontal" role="form" action="../setstatus.php" method="post">
										<div class="modal-body">
											 <p>You have approved with qualification.  Your qualification comment will be processed  and final payroll output will be returned to you.</p>
											 <div class="form-body">
												
												<input type="hidden" name="sg" value="3"/>
												<input type="hidden" name="st" value="6"/>
												<input type="hidden" name="so" value="1"/>
												<div class="form-group">
													<label class="col-md-3 control-label">Reason Code</label>
													<div class="col-md-9">
														<select class="form-control" name="rCode">
															<option>Select Reason</option>
															<option value="Additional Input Data">Inputting Error</option>
															<option value="Input Interpretation">Processing Error</option>
														</select>
													</div>
												</div>
												<div class="form-group">

													<label class="col-md-3 control-label" for="qualified_employees_affected">Employees Affected</label>
													<div class="col-md-9">													
														<select id="qualified_employees_affected" class="form-control" name="employeesImpacted" placeholder="Employees Impacted">
																<option value="">Select No. Employees impacted</option>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
															</select>
													</div>
												</div>												
												<div class="form-group">													
													<label class="col-md-3 control-label">Comments</label>
													<div class="col-md-9">
														<textarea name="comments" class="form-control"></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Attach File</label>
													<div class="col-md-9">
														<input type="file" class="form-control btn" name="fileupload"/>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											Are you happy to issue qualified approval?
											<button type="button" class="btn default" data-dismiss="modal">No</button>
											<input type="submit" class="btn btn-warning" value="Yes"/>
										</div>
										</form>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							<!-- /.modal -->
							<div class="modal fade" id="reject" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Reject</h4>
										</div>
										<div class="modal-body">
											 <p>You have rejected the draft payroll and a new input file is expected before further action can be taken to process the payroll.  <strong>Please note it is <span class="text-danger">5 Days to Pay Day</span>.</strong></p>
											
											<div class="form-body">
												<form class="form-horizontal" role="form" action="../setstatus.php" method="post">
												<input type="hidden" name="sg" value="3"/>
												<input type="hidden" name="st" value="7"/>
												<input type="hidden" name="so" value="1"/>
												<div class="form-group">
													<div class="form-group">
														<label class="col-md-4 control-label">Reason for Rejection</label>
														<div class="col-md-8">
															<select name="reason" class="form-control">
																<option value="1">Inputting Error</option>
																<option value="2">Processing Error</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label" for="employeesImpacted">Employees Impacted</label>
														<div class="col-md-8">
															<select id="employeesImpacted" class="form-control" name="employeesImpacted" placeholder="Employees Impacted">
																<option value="">Select No. Employees impacted</option>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
															</select>
														<!--	<input type="text" class="form-control" id="employeesImpacted" name="employeesImpacted" placeholder="Employees Impacted">-->
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label" for="comments">Comments </label>
														<div class="col-md-8">
															<textarea name="comments" class="form-control" placeholder="comments"></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											Do you wish to proceed with rejection?
											<button type="button" class="btn default" data-dismiss="modal">No</button>
											<input type="submit" class="btn btn-danger" value="Yes">
										</div>									
										</form>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
						<li class="timeline-<?php echo $Stage2Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cogs iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Processing | By TMF</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<h1>Processing Complete</h1>
									<table class="table uploadFiles">
										<tr>
											<th> TMF Draft Payroll Files</th>
											<th> Status </th>
											<th> Approved By </th>
											<th> Reason Code </th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `tmf_2_files`");
										while($row = mysql_fetch_array($result)){
											$endtime = strtotime( $row['date_time']) + 600;
											echo "<tr>
													<td><a href=\"http://localhost/mypayroll/processing/tmf/2/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td>
													<td>".$row['description']."</td>
													<td> Frank Furlong </td>
													<td>".$row['reason_code']."</td>
													<td>".date('Y-m-d H:i:s',$endtime )."</td>
												</tr>";																						
											}
											?>
									</table>
									<?php echo $row["Stage2Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage3Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P2D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-eye iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Approval | By Client</h2>
								<div class="timeline-content">
									<div class="form-body">
										<form class="form-horizontal" role="form">
										<div id="hideDownload">
											<div class="form-group">
												<label class="col-md-3 control-label">Draft Payroll</label>
												<div class="col-md-9">
													<a class="btn default" data-toggle="modal" href="#" onclick="showProceed()" ><i class="fa fa-cloud-download"></i> Download</a>
												</div>
											</div>
										</div>
										<div id="hidden" class="hidden uploadFiles">
											<table class="table ">
												<tr>
													<th> TMF Approved Files</th>
													<th> Status </th>
													<th> Approved By </th>
													<th> Uploaded Time </th>
												</tr>
												<?php 
												$result = mysql_query("SELECT * FROM `tmf_2_files` WHERE description = '<span class=\"label label-sm label-success\">Approved</span>'");
												while($row = mysql_fetch_array($result)){
													$endtime = strtotime( $row['date_time']) + 60000;
													echo "<tr>
															<td><a href=\"http://localhost/mypayroll/processing/tmf/2/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td>
															<td>".$row['description']."</td>
															<td>Frank Furlong</td>															
															<td>".date('Y-m-d H:i:s',$endtime )."</td>
														</tr>";																						
													}
													?>
											</table>
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-9">
													<a class="btn green" data-toggle="modal" href="#fullapproval"><i class="fa fa-thumbs-o-up"></i> Full Approval</a>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-9">
													<a class="btn yellow" data-toggle="modal" href="#qualifiedapproval"><i class="fa fa-thumbs-o-up"></i> Qualified Approval</a>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label"></label>
												<div class="col-md-9">
													<a class="btn red" data-toggle="modal" href="#reject"><i class="fa fa-thumbs-o-down"></i> Reject</a>
												</div>
											</div>
										</div>
										</form>
									</div>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage4Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-check iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Finalising | By TMF</h2>
								<div class="timeline-content">
								<?php echo $row["Stage4Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage5Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-thumbs-o-up iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Final Output Acceptance | By Client</h2>
								<div class="timeline-content">
								<?php echo $row["Stage5Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage6Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-lock iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Period Closed</h2>
								<div class="timeline-content">
								<?php echo $row["Stage6Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-bar-chart-o iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Reporting Available</h2>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-money iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Payslips Available</h2>
							</div>
						</li>
					</ul>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
	<script>
	 function showProceed(){
	 $('#hidden').removeClass('hidden');
	 $('#hideDownload').addClass('hidden');
	 }
	</script>
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>