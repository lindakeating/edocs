<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
include($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/client/1/findFiles.php');
include("rejectFiles.php");

$UploadFlag = true;
$timeLineFlag = true;

$sql = "SELECT * FROM payroll WHERE id = 1";
$result = mysql_query($sql);
if(!$result) {
    die("Database query failed: " . mysql_error());
}
$row = mysql_fetch_array($result);

$Client = $row["Client"];
$Entity = $row["Entity"];
$Stage1 = $row["Stage1"];
$Stage2 = $row["Stage2"];
$Stage3 = $row["Stage3"];
$Stage4 = $row["Stage4"];
$Stage5 = $row["Stage5"];
$Stage6 = $row["Stage6"];

$Stage1Owner = $row["Stage1Owner"];
$Stage2Owner = $row["Stage2Owner"];
$Stage3Owner = $row["Stage3Owner"];
$Stage4Owner = $row["Stage4Owner"];
$Stage5Owner = $row["Stage5Owner"];
$Stage6Owner = $row["Stage6Owner"];

include($_SERVER['DOCUMENT_ROOT'] . "/includes/stagecolours.php");

if($Stage2 >= 5)
{
	header("location:../3/");
	exit();
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Payroll Processing | <?php echo $Entity; ?> | IE
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<ul class="timeline">
						<li class="timeline-<?php echo $Stage1Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime = new DateTime();
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cloud-upload iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Inputting | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>	
								<div class="timeline-content hiddenTimeline">
									<h1>Input Complete</h1>
										<table class="table uploadFiles">
											<tr>
												<th> Inputted Files</th>
												<th> File Type </th>
												<th> Time Submitted </th>
												<th> Submitted By </th>
											</tr>
											<tr><td>SS Changes</td><td>SS Changes</td><td>2014-06-01 09:13:09</td><td>Self Service Portal</td><td><td></tr>
											<?php 
											$result = mysql_query("SELECT * FROM `files`");
												while($row = mysql_fetch_array($result)){
													$endtime = strtotime( $row['date_time']) + 600;
													echo "<tr><td>".$row['name']."</td><td>".$row['description']."</td><td>".$row['date_time']."</td><td>Mary Moore</td></tr>";																						
													}									
											?>
										</table>
									<div class="timeline-content">
										<?php echo $row["Stage1Comments"]; ?>
									</div>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage2Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cogs iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Processing | By TMF</h2>
								<?php

								if($Stage2Owner == 2) { ?>
								<div class="timeline-content">
									<blockquote>
										<p style="font-size:16px">
											 Please upload draft payroll for client approval.
										</p>
									</blockquote>
									<br>
									<form id="fileupload" action="/assets/plugins/jquery-file-upload/server/php/UploadHandler.php" method="POST" enctype="multipart/form-data">
										<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
										<input type="hidden" name="pending" value="<span class=&quot;label label-sm label-warning&quot;>Pending approval</span>" />
										<div class="row fileupload-buttonbar">
											<div class="col-lg-7">
												<!-- The fileinput-button span is used to style the file input field as button -->
												<span class="btn green fileinput-button">
													<i class="fa fa-plus"></i>
													<span>
														 Add files...
													</span>
													<input type="file" name="files[]" multiple="">
												</span>
												<button type="submit" class="btn blue start">
												<i class="fa fa-upload"></i>
												<span>
													 Start uploads
												</span>
												</button>
												<!-- The global file processing state -->
												<span class="fileupload-process">
												</span>
											</div>
											<!-- The global progress information -->
											<div class="col-lg-5 fileupload-progress fade">
												<!-- The global progress bar -->
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
													<div class="progress-bar progress-bar-success" style="width:0%;">
													</div>
												</div>
												<!-- The extended global progress information -->
												<div class="progress-extended">
													 &nbsp;
												</div>
											</div>
										</div>
										<!-- The table listing the files available for upload/download -->
										<div class="well">
											<table role="presentation" class="table clearfix ">
												<tbody class="files">
												</tbody>
											</table>
										</div>
									</form>
									<?php
									$result = mysql_query("SELECT * FROM `tmf_2_files`");
									$num = mysql_num_rows($result);
									if($num > 0)
									{
											echo
											"<table class=\"table uploadFiles\">
															<tr>
																<th> TMF Draft Payroll Files</th>
																<th> Status</th>
																<th> Comments </th>
																<th> Approved By </th>
																<th> Reason Code</th>
																<th> Time Approved </th>
															</tr>";
															while($row = mysql_fetch_array($result)){
																$endtime = strtotime( $row['date_time']) + 3000;
																echo "<tr>
																		<td>".$row['name']."</td>
																		<td>".$row['description']."</td>
																		<td>".$row['title']."</td>
																		<td>Frank Furlong</td>
																		<td>".$row['reason_code']."</td>
																		
																		<td>".date('Y-m-d H:i:s',$endtime )."</td>
																	</tr>";																						
																}
																echo "</table>;


										<a class=\"btn default\" data-toggle=\"modal\" href=\"./b/\"><i class=\"fa fa-cloud-upload\"></i>Re-submit Draft Payroll for internal checking</a>";
									}
									else
									{
									?>
									<a class="btn default" data-toggle="modal" href="./b/"><i class="fa fa-cloud-upload"></i> Submit Draft Payroll for internal checking</a>
									<?php
									}
									?>
								</div>
								<?php } else { ?>
								<div class="timeline-content">
									<h1>Processing Complete</h1>
								</div>
								<?php } ?>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage3Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P2D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-eye iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Approval | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">								
									<table class="table uploadFiles">
										<tr>
											<th> Type </th>
											<th> Rejection Reason</th>
											<th> Employees Affected</th>
											<th> Comments</th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `reject_client_3`");
										while($row = mysql_fetch_array($result)){
											echo "<tr>
													<td>".$row['type']."</td>
													<td>".$row['reason']."</td>
													<td>".$row['no_employees']."</td>
													<td>".$row['comments']."</td>
												</tr>";																						
											}
											?>
									</table>
									<?php echo $row["Stage3Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage4Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-check iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Finalising | By TMF</h2>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage5Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-thumbs-o-up iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Final Output Acceptance | By Client</h2>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage6Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-lock iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Period Closed</h2>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-bar-chart-o iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Reporting Available</h2>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-money iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Payslips Available</h2>
							</div>
						</li>
					</ul>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>