<?php
// change the name of all the rejected files
function rejectFiles(DirectoryIterator $directory)
	{
	$result=array();
		foreach($directory as $object)
			{
			 if($object->isFile())
				{
				$f = $object->getFilename();
				$extension_pos = strrpos($f, '.');
				$reject = substr($f, 0, $extension_pos).'_rejected_'.substr($f, $extension_pos); 
				rename('files/'.$f, 'files/'.$reject);
				$result[]=$reject;
				}
			}
	}
?>