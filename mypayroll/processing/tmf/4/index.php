<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
include($_SERVER['DOCUMENT_ROOT'] .'/mypayroll/processing/client/1/findFiles.php');

$UploadFlag = true;
$timeLineFlag = true;
$updatePayslips = true;

$sql = "SELECT * FROM payroll WHERE id = 1";
$result = mysql_query($sql);
if(!$result) {
    die("Database query failed: " . mysql_error());
}
$row = mysql_fetch_array($result);

$Client = $row["Client"];
$Entity = $row["Entity"];
$Stage1 = $row["Stage1"];
$Stage2 = $row["Stage2"];
$Stage3 = $row["Stage3"];
$Stage4 = $row["Stage4"];
$Stage5 = $row["Stage5"];
$Stage6 = $row["Stage6"];

$Stage1Owner = $row["Stage1Owner"];
$Stage2Owner = $row["Stage2Owner"];
$Stage3Owner = $row["Stage3Owner"];
$Stage4Owner = $row["Stage4Owner"];
$Stage5Owner = $row["Stage5Owner"];
$Stage6Owner = $row["Stage6Owner"];

include($_SERVER['DOCUMENT_ROOT'] . "/includes/stagecolours.php");

if($Stage4 >= 5)
{
	header("location:../5/");
	exit();
}

if($Stage3 == 7 && $Stage2Owner == 2){
	header("location:../1/");
	exit();

}

// redirect to stage 2 if stage 3 has been rejected by client
if($Stage3 == 7)
{
	header("location:../2/");
	exit();
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Payroll Processing | <?php echo $Entity; ?> | IE
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<ul class="timeline">
						<li class="timeline-<?php echo $Stage1Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime = new DateTime();
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cloud-upload iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Inputting | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<h1>Input Complete</h1>
									<table class="table ">
										<tr>
											<th> Inputted Files</th>
											<th> File Type </th>
											<th> Time Submitted </th>
											<th> Downloaded By </th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `files`");
											while($row = mysql_fetch_array($result)){
												$endtime = strtotime( $row['date_time']) + 600;
												echo "<tr>
														<td><a href=\"http://localhost/mypayroll/processing/client/1/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td>
														<td>".$row['description']."</td>
														<td>".$row['date_time']."</td>
														<td>Mary Moore</td>
														<td>".date('Y-m-d H:i:s',$endtime )."</td>
													</tr>";
											}									
										?>
									</table>
								</div>
							</div>
						</li>
						<div class="modal fade" id="verifyFiles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="myModalLabel">Final Payroll</h4>
									</div>
									<div class="modal-body">
        ...
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary">Save changes</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.modal -->
							<div class="modal fade"  tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Verify Final Payroll</h4>
										</div>
										<div class="modal-body">
											 <div class="table-responsive">
												<table class="table table-hover">
													<thead>
											 		<tr>
											 		<td>Check</td>
											 		<td>Payroll</td>
											 		<td>Inputed</td>
											 		<td>Result</td>
											 		</tr>
											 	</th>
											 	</thead>
											 	<tbody>
											 	<tr class="green">
											 		<td>Total Number of Employees Processed in Period</td>
											 		<td id="noPayroll"></td>
											 		<td id="noInputted"></td>
											 		<td><span class="label label-sm label-success"><i class="fa fa-check "></i></span></td>
											 	</tr>
											 	</tbody>
											 </table>
											 </div>
										</div>
										<div class="modal-footer">Submit for final client acceptance?
											<button type="button" class="btn default" data-dismiss="modal">No</button>
											<!--<a id="verifyYes" class="btn green" href="../setstatus.php?sg=4&st=5&so=2&nsg=5&nst=3&nso=1">Yes</a>-->
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->

						<li class="timeline-<?php echo $Stage2Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-cogs iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Processing | By TMF</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
								<h1> Processing Complete </h1>
								<table class="table uploadFiles">
										<tr>
											<th> TMF Draft Payroll Files</th>
											<th> Status </th>
											<th> Submitted By </th>
											<th> Downloaded By </th>
											<th> Time Downloaded </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `tmf_2_files`");
										while($row = mysql_fetch_array($result)){
											$endtime = strtotime( $row['date_time']) + 600;
											echo "<tr>
													<td><a href=\"http://localhost/mypayroll/processing/tmf/2/files/".$row['name']."\" download = \"".$row['name']."\">".$row['name']."</a></td>
													<td>".$row['description']."</td>
													<td>".$row['date_time']."</td>
													<td>Mary Moore</td>
													<td>".date('Y-m-d H:i:s',$endtime )."</td>
												</tr>";																						
											}
											?>
									</table>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage3Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P2D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-eye iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2 class="timelineHeading">Approval | By Client</h2>
								<i class="fa fa-chevron-down time-line-control"></i>
								<div class="timeline-content hiddenTimeline">
									<table class="table uploadFiles">
										<tr>
											<th> Type </th>
											<th> Rejection Reason</th>
											<th> Employees Affected</th>
											<th> Comments</th>
											<th> Files </th>
										</tr>
										<?php 
										$result = mysql_query("SELECT * FROM `reject_client_3`");
										while($row = mysql_fetch_array($result)){
											echo "<tr>
													<td>".$row['type']."</td>
													<td>".$row['reason']."</td>
													<td>".$row['no_employees']."</td>
													<td>".$row['comments']."</td>
													<td><a href=\"#\" class=\"btn default\"><i class=\"fa fa-file-o\"></i></a></td>
												</tr>";																						
											}
											?>
									</table>
									<?php echo $row["Stage3Comments"]; ?>
								</div>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage4Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-check iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Finalising | By TMF</h2>
								<?php if($Stage4Owner == 2) { ?>
								<div class="timeline-content">
									<blockquote>
										<p style="font-size:16px">
											 Upload final payroll files.
										</p>
									</blockquote>
									<br>
									<form id="fileupload" action="/assets/plugins/jquery-file-upload/server/php/UploadHandler.php" method="POST" enctype="multipart/form-data">
										<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
										<div class="row fileupload-buttonbar">
											<div class="col-lg-7">
												<!-- The fileinput-button span is used to style the file input field as button -->
												<span class="btn green fileinput-button">
													<i class="fa fa-plus"></i>
													<span>
														 Add files...
													</span>
													<input type="file" name="files[]" multiple="">
												</span>
												<button type="submit" class="btn blue start">
												<i class="fa fa-upload"></i>
												<span>
													 Start uploads
												</span>
												</button>
												<!-- The global file processing state -->
												<span class="fileupload-process">
												</span>
											</div>
											<!-- The global progress information -->
											<div class="col-lg-5 fileupload-progress fade">
												<!-- The global progress bar -->
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
													<div class="progress-bar progress-bar-success" style="width:0%;">
													</div>
												</div>
												<!-- The extended global progress information -->
												<div class="progress-extended">
													 &nbsp;
												</div>
											</div>
										</div>
										<!-- The table listing the files available for upload/download -->
										<div class="well">
											<table role="presentation" class="table clearfix" style="color:#000;">
												<tbody class="files">
												</tbody>
											</table>
										</div>
									</form>
									
									<div class="form-body">
										<form class="form-horizontal" role="form">
										<div class="form-group">
											<label class="col-md-6 control-label">Total Number of Employees Processed in Period</label>
											<div class="col-md-2">
												<input type="text" class="form-control " name="employees" id="employees" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-6 control-label">Number of Starters in the period</label>
											<div class="col-md-2">
												<input type="text" class="form-control " name="starters" id="starters"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-6 control-label">Number of Leavers in the period</label>
											<div class="col-md-2">
												<input type="text" class="form-control " name="leavers" id="leavers"/>
											</div>
										</div>
										</form>
									</div>
									<a class="btn default" id="#verify" onclick="checkFiles()"><i class="fa fa-check-square"></i> Submit Final Payroll Files</a>
								</div>
								<?php } else { ?>
								<div class="timeline-content">
									<h1>Finalising Complete</h1>
								</div>
								<?php } ?>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage5Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-thumbs-o-up iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Final Output Acceptance | By Client</h2>
							</div>
						</li>
						<li class="timeline-<?php echo $Stage6Colour; ?>">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-lock iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Period Closed</h2>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-bar-chart-o iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Reporting Available</h2>
							</div>
						</li>
						<li class="timeline-blue">
							<div class="timeline-time">
								<span class="time">
								 <?php
									$datetime->add(new DateInterval("P1D"));
									echo $datetime->format("F d");
                                 ?>
								</span>
							</div>
							<div class="timeline-icon">
								<i class="fa fa-money iconheight"></i>
							</div>
							<div class="timeline-body">
								<h2>Payslips Available</h2>
							</div>
						</li>
					</ul>
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<script>
function checkFiles(){
alert(this);
}
</script>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>