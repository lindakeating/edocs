<?php
$PageTitle = "TMF | My Tickets";

include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				My Tickets
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-hover" id="SortTable">
							<thead>
							<tr>
								<th>
									 Client
								</th>
								<th>
									 Client Entity
								</th>
								<th>
									 Country
								</th>
								<th>
									Date Raised
								</th>
								<th>
									Last Updated
								</th>
								<th>
									Action To
								</th>
								<th>
									Ticket Reason
								</th>
								<th>
									Status
								</th>
							</tr>
							</thead>
							<tbody>
							<tr class="odd gradeX" onclick="location.href = './ticket/';">
								<td>
									<a href="ticket/?c=FL&ce=IL">Frozen Limited</a>
								</td>
								<td>
									 <a href="ticket/?c=FL&ce=IL">Ice Limited</a>
								</td>
								<td>
									Ireland
								</td>
								<td>
									01/01/2014
								</td>
								<td>
									01/01/2014
								</td>
								<td>
									TMF
								</td>
								<td>
									Final Rejected Payroll
								</td>
								<td>
									Closed
								</td>
							</tr>
							<tr class="even gradeX" onclick="location.href = './ticket/';">
								<td>
									<a href="ticket/?c=FL&ce=FL">Frozen Limited</a>
								</td>
								<td>
									 <a href="ticket/?c=FL&ce=FL">Frost Limited</a>
								</td>
								<td>
									Ireland
								</td>
								<td>
									12/03/2014
								</td>
								<td>
									02/04/2014
								</td>
								<td>
									Client
								</td>
								<td>
									Final Output Query
								</td>
								<td>
									Open
								</td>
							</tr>
							<tr class="odd gradeX" onclick="location.href = './ticket/';">
								<td>
									<a href="ticket/?c=FL&ce=FL">Frozen Limited</a>
								</td>
								<td>
									 <a href="ticket/?c=FL&ce=FL">Frost Limited</a>
								</td>
								<td>
									UK
								</td>
								<td>
									01/04/2014
								</td>
								<td>
									03/04/2014
								</td>
								<td>
									TMF
								</td>
								<td>
									Input Query
								</td>
								<td>
									Open
								</td>
							</tr>
							</tbody>
							</table>
				
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>