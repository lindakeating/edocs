<?php 
$host = 'localhost';
$username = 'tmf';
$password = 'tmf';
$database = 'tmf';
	
$con = mysql_connect($host, $username, $password); 
$db_selected = mysql_select_db($database, $con);

if(isset($_POST['action']) && !empty($_POST['action'])){
	$action = $_POST['action'];
	switch($action){
		case 'getChildTable': getChildTable(); break;
		case 'getParentTable':getParentTable(); break;
		case 'getWhereTable' :getWhereTable(); break;
	}
}

function do_mysql_query($sql) {

    $sql = preg_replace('/[\n\r\t]+/', ' ',$sql);
    error_log("SQL : " . $sql);
    $result = mysql_query($sql);
    $status = $result? 'ok' : mysql_error();
    error_log("SQL status: " . $status);
    return $result;

}

	function getChildTable(){
		$comp_id = $_POST['comp_id'];
		$column = $_POST['column'];
		$col_data = $_POST['col_data'];
        $period1 = $_POST['period1'];
        $period2 = $_POST['period2'];
        //$start = $_POST['start'];
        //$end = $_POST['end'];
		$sql1 = "SELECT `Period`,
				    `Country`,
					`Company Name`,
					`Department`, 
					`Cost Center`,
					`Employee Name`,
					ROUND(`Commission`, 0) AS `Commission`,
					ROUND(`Total Taxable Salary`,0) AS `Total Taxable Salary`,
					ROUND(`EE Pension`,0) AS `EE Pension`,
					ROUND(`ER Social Security/ Insurances`,0) AS `ER Social Security/ Insurances`,
					ROUND(`ER Other Deductions(taxable)`,0) AS `ER Other Deductions(taxable)`,
					ROUND(`Benefits in Kind`,0) AS `Benefits in Kind`,
					ROUND(`Overtime`,0) AS `Overtime`,
					ROUND(`EE Other Gross Additions`,0) AS `EE Other Gross Additions`,
					ROUND(`EE Withholding Tax`,0) AS `EE Withholding Tax`,
					ROUND(`EE Other Net Deductions`,0) AS `EE Other Net Deductions`,
					ROUND(`ER Pension`,0) AS `ER Pension`,
					ROUND(`ER Other Additions (non-taxable)`,0) AS `ER Other Additions (non-taxable)`,
					ROUND(`Basic Salary`,0) AS `Basic Salary`,
					ROUND(`Bonus`,0) AS `Bonus`,
					ROUND(`EE Other Gross Deductions`,0) AS `EE Other Gross Deductions`,
					ROUND(`EE Social Security/ Insurances`,0) AS `EE Social Security/ Insurances`,
					ROUND(`EE Other Net Additions`,0) AS `EE Other Net Additions`,
					ROUND(`ER Other Additions (taxable)`,0) AS `ER Other Additions (taxable)`,
					ROUND(`ER Other Deductions (non-taxable)`,0) AS `ER Other Deductions (non-taxable)`,
					ROUND(`Total Taxable Salary`,0) AS `Total Taxable Salary`,					
					ROUND(`Net Salary`,0) AS `Net Salary`,
					ROUND(`Total Gross Salary`,0) AS `Total Gross Salary`,					
					ROUND(`Total ER Cost`,0) AS `Total ER Cost`,
					`comp_id`
					FROM gross_to_net
					WHERE `".$column."` = '".$col_data."' AND `Period` ='" . $period1 . "'";
        //WHERE `".$column."` = '".$col_data."' AND Period BETWEEN '".$start."' AND '".$end."'";

        $sql2 = "SELECT `Period`,
				    `Country`,
					`Company Name`,
					`Department`,
					`Cost Center`,
					`Employee Name`,
					ROUND(`Commission`, 0) AS `Commission`,
					ROUND(`Total Taxable Salary`,0) AS `Total Taxable Salary`,
					ROUND(`EE Pension`,0) AS `EE Pension`,
					ROUND(`ER Social Security/ Insurances`,0) AS `ER Social Security/ Insurances`,
					ROUND(`ER Other Deductions(taxable)`,0) AS `ER Other Deductions(taxable)`,
					ROUND(`Benefits in Kind`,0) AS `Benefits in Kind`,
					ROUND(`Overtime`,0) AS `Overtime`,
					ROUND(`EE Other Gross Additions`,0) AS `EE Other Gross Additions`,
					ROUND(`EE Withholding Tax`,0) AS `EE Withholding Tax`,
					ROUND(`EE Other Net Deductions`,0) AS `EE Other Net Deductions`,
					ROUND(`ER Pension`,0) AS `ER Pension`,
					ROUND(`ER Other Additions (non-taxable)`,0) AS `ER Other Additions (non-taxable)`,
					ROUND(`Basic Salary`,0) AS `Basic Salary`,
					ROUND(`Bonus`,0) AS `Bonus`,
					ROUND(`EE Other Gross Deductions`,0) AS `EE Other Gross Deductions`,
					ROUND(`EE Social Security/ Insurances`,0) AS `EE Social Security/ Insurances`,
					ROUND(`EE Other Net Additions`,0) AS `EE Other Net Additions`,
					ROUND(`ER Other Additions (taxable)`,0) AS `ER Other Additions (taxable)`,
					ROUND(`ER Other Deductions (non-taxable)`,0) AS `ER Other Deductions (non-taxable)`,
					ROUND(`Total Taxable Salary`,0) AS `Total Taxable Salary`,
					ROUND(`Net Salary`,0) AS `Net Salary`,
					ROUND(`Total Gross Salary`,0) AS `Total Gross Salary`,
					ROUND(`Total ER Cost`,0) AS `Total ER Cost`,
					`comp_id`
					FROM gross_to_net
					WHERE `".$column."` = '".$col_data."' AND `Period` ='" . $period2 . "'";
					//WHERE `".$column."` = '".$col_data."' AND Period BETWEEN '".$start."' AND '".$end."'";


        $showColumns = [];

        $result1 = do_mysql_query($sql1);
        $result2 = do_mysql_query($sql2);
        $returnedData1 = createJsonResponse($result1, $showColumns);
        $returnedData2 = createJsonResponse($result2, $showColumns);
        $rows1 =     $returnedData1['aaData'];
        $rows2 =     $returnedData2['aaData'];
        $n = count($rows1);

        for ($i = 0; $i < $n; $i += 1) {
            foreach ($rows1[$i] as $key => $value1) {
                $value2 = $rows2[$i]->$key;
                $rows1[$i]->$key = [ $value1, $value2, $key, $i ];
            }
        }
        $returnedData1['aaData'] = $rows1;
        echo json_encode($returnedData1);
	}

	function getParentTable(){
        $period1 = $_POST['period1'];
        $period2 = $_POST['period2'];
	$sql1 = "SELECT `Period`,
					`Region`,
					`Group`,
				    `Country`,
					`Department`, 
					`Cost Center`,
					ROUND(SUM(`Commission`), 0) AS `Commission`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,
					ROUND(SUM(`EE Pension`),0) AS `EE Pension`,
					ROUND(SUM(`ER Social Security/ Insurances`),0) AS `ER Social Security/ Insurances`,
					ROUND(SUM(`ER Other Deductions(taxable)`),0) AS `ER Other Deductions(taxable)`,
					ROUND(SUM(`Benefits in Kind`),0) AS `Benefits in Kind`,
					ROUND(SUM(`Overtime`),0) AS `Overtime`,
					ROUND(SUM(`EE Other Gross Additions`),0) AS `EE Other Gross Additions`,
					ROUND(SUM(`EE Withholding Tax`),0) AS `EE Withholding Tax`,
					ROUND(SUM(`EE Other Net Deductions`),0) AS `EE Other Net Deductions`,
					ROUND(SUM(`ER Pension`),0) AS `ER Pension`,
					ROUND(SUM(`ER Other Additions (non-taxable)`),0) AS `ER Other Additions (non-taxable)`,
					ROUND(SUM(`Basic Salary`),0) AS `Basic Salary`,
					ROUND(SUM(`Bonus`),0) AS `Bonus`,
					ROUND(SUM(`EE Other Gross Deductions`),0) AS `EE Other Gross Deductions`,
					ROUND(SUM(`EE Social Security/ Insurances`),0) AS `EE Social Security/ Insurances`,
					ROUND(SUM(`EE Other Net Additions`),0) AS `EE Other Net Additions`,
					ROUND(SUM(`ER Other Additions (taxable)`),0) AS `ER Other Additions (taxable)`,
					ROUND(SUM(`ER Other Deductions (non-taxable)`),0) AS `ER Other Deductions (non-taxable)`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,					
					ROUND(SUM(`Net Salary`),0) AS `Net Salary`,
					ROUND(SUM(`Total Gross Salary`),0) AS `Total Gross Salary`,					
					ROUND(SUM(`Total ER Cost`),0) AS `Total ER Cost`,
					`comp_id`
					FROM gross_to_net WHERE period = '$period1' GROUP by `Region`";

        $sql2 = "SELECT `Period`,
					`Region`,
					`Group`,
				    `Country`,
					`Department`,
					`Cost Center`,
					ROUND(SUM(`Commission`), 0) AS `Commission`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,
					ROUND(SUM(`EE Pension`),0) AS `EE Pension`,
					ROUND(SUM(`ER Social Security/ Insurances`),0) AS `ER Social Security/ Insurances`,
					ROUND(SUM(`ER Other Deductions(taxable)`),0) AS `ER Other Deductions(taxable)`,
					ROUND(SUM(`Benefits in Kind`),0) AS `Benefits in Kind`,
					ROUND(SUM(`Overtime`),0) AS `Overtime`,
					ROUND(SUM(`EE Other Gross Additions`),0) AS `EE Other Gross Additions`,
					ROUND(SUM(`EE Withholding Tax`),0) AS `EE Withholding Tax`,
					ROUND(SUM(`EE Other Net Deductions`),0) AS `EE Other Net Deductions`,
					ROUND(SUM(`ER Pension`),0) AS `ER Pension`,
					ROUND(SUM(`ER Other Additions (non-taxable)`),0) AS `ER Other Additions (non-taxable)`,
					ROUND(SUM(`Basic Salary`),0) AS `Basic Salary`,
					ROUND(SUM(`Bonus`),0) AS `Bonus`,
					ROUND(SUM(`EE Other Gross Deductions`),0) AS `EE Other Gross Deductions`,
					ROUND(SUM(`EE Social Security/ Insurances`),0) AS `EE Social Security/ Insurances`,
					ROUND(SUM(`EE Other Net Additions`),0) AS `EE Other Net Additions`,
					ROUND(SUM(`ER Other Additions (taxable)`),0) AS `ER Other Additions (taxable)`,
					ROUND(SUM(`ER Other Deductions (non-taxable)`),0) AS `ER Other Deductions (non-taxable)`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,
					ROUND(SUM(`Net Salary`),0) AS `Net Salary`,
					ROUND(SUM(`Total Gross Salary`),0) AS `Total Gross Salary`,
					ROUND(SUM(`Total ER Cost`),0) AS `Total ER Cost`,
					`comp_id`
                    FROM gross_to_net WHERE period = '$period2' GROUP by `Region`";
        //FROM gross_to_net GROUP by `Country`";


	$showColumns = [];
    $result1 = do_mysql_query($sql1);
    $result2 = do_mysql_query($sql2);
	$returnedData1 = createJsonResponse($result1, $showColumns);
    $returnedData2 = createJsonResponse($result2, $showColumns);
    $rows1 =     $returnedData1['aaData'];
    $rows2 =     $returnedData2['aaData'];
    $n = count($rows1);

    for ($i = 0; $i < $n; $i += 1) {
        foreach ($rows1[$i] as $key => $value1) {
            $value2 = $rows2[$i]->$key;
            $rows1[$i]->$key = [ $value1, $value2, $key, $i ];
        }
    }
    $returnedData1['aaData'] = $rows1;
	echo json_encode($returnedData1);
	}
	
	function getWhereTable(){
        $period1 = $_POST['period1'];
        $period2 = $_POST['period2'];
	$region = $_POST['region'];
	$country = $_POST['country'];
	$costCenter = $_POST['costCenter'];
	$group = $_POST['group'];
    if(isset($_POST['start'])){
        $start = $_POST['start'];
        }
     else{
         $start = new DateTime();
     }

    if(isset($_POST['end'])){
        $end = $_POST['end'];
        }
    else{
       $date = new DateTime();
       $interval = new DateInterval('P1M');
       $end = $date->sub($interval);
    }
    if(isset($_POST['start2'])){
        $start2 = $_POST['start2'];
    }
    else{
        $start2 = new DateTime();
    }

    if(isset($_POST['end2'])){
        $end2 = $_POST['end2'];
    }
    else{
        $date = new DateTime();
        $interval = new DateInterval('P1M');
        $end2 = $date->sub($interval);
    }
	
	$regionString = "";
	$countryString = "";
	$groupString = "";
	$costCenterString = "";


	if(isset($region)){
		$regionString .= " Region IN ( ";
		foreach($region as $r){
			$regionString .= "'".$r."' ,";		
		};
		$regionString = rtrim($regionString , ", ");
		$regionString .= " )";
	}
	
	if(isset($country)){
		$countryString .= " Country IN ( ";
		foreach($country as $c){
			$countryString .= "'".$c."' ,";		
		};
		$countryString = rtrim($countryString , ", ");
		$countryString .= " )";
	}
	
	if(isset($costCenter)){
		$costCenterString .= " `Cost Center` IN ( ";
		foreach($costCenter as $cc){
			$costCenterString .= "'".$cc."' ,";	
		}
		$costCenterString = rtrim($costCenterString , ", ");
		$costCenterString .= " )";
	}
	
	if(isset($group)){
		$groupString .= " 'Group' IN ( ";
		foreach($group as $g){
			$groupString .= "'".$g."' ,";		
		}
		$groupString = rtrim($groupString , ", ");
		$groupString .= " )";
	}
	$sql = "SELECT `Period`,
                    `Region`,
                    `Group`,
				    `Country`,
					`Department`, 
					`Cost Center`,
					ROUND(SUM(`Commission`), 0) AS `Commission`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,
					ROUND(SUM(`EE Pension`),0) AS `EE Pension`,
					ROUND(SUM(`ER Social Security/ Insurances`),0) AS `ER Social Security/ Insurances`,
					ROUND(SUM(`ER Other Deductions(taxable)`),0) AS `ER Other Deductions(taxable)`,
					ROUND(SUM(`Benefits in Kind`),0) AS `Benefits in Kind`,
					ROUND(SUM(`Overtime`),0) AS `Overtime`,
					ROUND(SUM(`EE Other Gross Additions`),0) AS `EE Other Gross Additions`,
					ROUND(SUM(`EE Withholding Tax`),0) AS `EE Withholding Tax`,
					ROUND(SUM(`EE Other Net Deductions`),0) AS `EE Other Net Deductions`,
					ROUND(SUM(`ER Pension`),0) AS `ER Pension`,
					ROUND(SUM(`ER Other Additions (non-taxable)`),0) AS `ER Other Additions (non-taxable)`,
					ROUND(SUM(`Basic Salary`),0) AS `Basic Salary`,
					ROUND(SUM(`Bonus`),0) AS `Bonus`,
					ROUND(SUM(`EE Other Gross Deductions`),0) AS `EE Other Gross Deductions`,
					ROUND(SUM(`EE Social Security/ Insurances`),0) AS `EE Social Security/ Insurances`,
					ROUND(SUM(`EE Other Net Additions`),0) AS `EE Other Net Additions`,
					ROUND(SUM(`ER Other Additions (taxable)`),0) AS `ER Other Additions (taxable)`,
					ROUND(SUM(`ER Other Deductions (non-taxable)`),0) AS `ER Other Deductions (non-taxable)`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,					
					ROUND(SUM(`Net Salary`),0) AS `Net Salary`,
					ROUND(SUM(`Total Gross Salary`),0) AS `Total Gross Salary`,					
					ROUND(SUM(`Total ER Cost`),0) AS `Total ER Cost`,
					`comp_id`
					FROM gross_to_net WHERE ";
					
	$showColumns = [];				
	if($regionString != ""){
		$sql = $sql .= $regionString . " AND ";
		array_push($showColumns, "Region");	
	}
	if($countryString != ""){
		$sql = $sql .= $countryString . " AND ";
		array_push($showColumns, "Country");
	}
	if($groupString != ""){
		$sql = $sql .= $groupString . " AND ";
	    array_push($showColumns , "Group");
	}
	if($costCenterString != ""){
		$sql = $sql .= $costCenterString . " AND ";
		array_push($showColumns, "Cost Center");
	
	}
    $sql2 = "" . $sql;
    //$sql = $sql .= " Period BETWEEN '".$start."' AND '".$end."' ";
        $sql .= " Period = '$period1'";
	$sql = $sql .= " GROUP BY ";
	
	if($regionString != "") {
		$sql = $sql .= " Region, ";	
	}
	
	if($groupString != "") {
		$sql = $sql .= " 'Group', ";	
	}
	
	if($countryString != ""){
		$sql = $sql .= " Country, ";
	
	}
	if($costCenterString != ""){
		$sql = $sql .= " `Cost Center`, ";	
	}
    $sql = $sql.= " `Period`";
	$sql = rtrim($sql , ", ");

        //$sql2 = $sql2 .= " Period BETWEEN '".$start2."' AND '".$end2."' ";
        $sql2 .= " Period = '$period2'";
        $sql2 = $sql2 .= " GROUP BY ";

        if($regionString != "") {
            $sql2 = $sql2 .= " Region, ";
        }

        if($groupString != "") {
            $sql2 = $sql2 .= " 'Group', ";
        }

        if($countryString != ""){
            $sql2 = $sql2 .= " Country, ";

        }
        if($costCenterString != ""){
            $sql2 = $sql2 .= " `Cost Center`, ";
        }
        $sql2 = $sql2.= " `Period`";
        $sql2 = rtrim($sql2 , ", ");


        $showColumns = [];
        $result1 = do_mysql_query($sql);
        $result2 = do_mysql_query($sql2);
        $returnedData1 = createJsonResponse($result1, $showColumns);
        $returnedData2 = createJsonResponse($result2, $showColumns);
        $rows1 =     $returnedData1['aaData'];
        $rows2 =     $returnedData2['aaData'];
        $n = count($rows1);

        for ($i = 0; $i < $n; $i += 1) {
            foreach ($rows1[$i] as $key => $value1) {
                $value2 = $rows2[$i]->$key;
                $rows1[$i]->$key = [ $value1, $value2, $key, $i ];
            }
        }
    $returnedData1['aaData'] = $rows1;
    echo json_encode($returnedData1);
	}
	
	function createJsonResponse($res, $showColumns){
		$data = array();
		$returnedData = array();
	
		$row = array();
	
		while($r = mysql_fetch_array($res, MYSQL_ASSOC)){
		$columns = array();
		$keys = array_keys($r);
		$dataObject = new StdClass;
	
		foreach($keys as $val){
			$object = new StdClass;
			$object->mDataProp = $val;
			$object->sTitle = $val;
			if(count($showColumns)>0){
				if(!in_array($val, $showColumns)){
					$object->show = true;
				}
				else{
					$object->show = false;
				}
			}
			array_push($columns, $object);			
			$dataObject->$val = $r[$val];
			
		}
		array_push($data, $dataObject);
	}
		$returnedData['aaData'] = $data;
		$returnedData['aoColumns'] = $columns;
		return $returnedData;
	}
	
	
?>