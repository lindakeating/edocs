<?php 
$host = 'localhost';
$username = 'tmf';
$password = 'tmf';
$database = 'tmf';
	
$con = mysql_connect($host, $username, $password); 
$db_selected = mysql_select_db($database, $con);

if(isset($_POST['action']) && !empty($_POST['action'])){
	$action = $_POST['action'];
	switch($action){
		case 'getChildTable': getChildTable(); break;
		case 'getParentTable':getParentTable(); break;
		case 'getWhereTable' :getWhereTable(); break;
	}
}

	function getChildTable(){
		$comp_id = $_POST['comp_id'];
		$column = $_POST['column'];
		$col_data = $_POST['col_data'];
        $period = $_POST['period'];
		$sql = "SELECT `Period`,
		            `Region`,
		            `Group`,
				    `Country`,
					`Company Name`,
					`Department`, 
					`Cost Center`,
					`Employee Name`,
					ROUND(`Commission`, 0) AS `Commission`,
					ROUND(`Total Taxable Salary`,0) AS `Total Taxable Salary`,
					ROUND(`EE Pension`,0) AS `EE Pension`,
					ROUND(`ER Social Security/ Insurances`,0) AS `ER Social Security/ Insurances`,
					ROUND(`ER Other Deductions(taxable)`,0) AS `ER Other Deductions(taxable)`,
					ROUND(`Benefits in Kind`,0) AS `Benefits in Kind`,
					ROUND(`Overtime`,0) AS `Overtime`,
					ROUND(`EE Other Gross Additions`,0) AS `EE Other Gross Additions`,
					ROUND(`EE Withholding Tax`,0) AS `EE Withholding Tax`,
					ROUND(`EE Other Net Deductions`,0) AS `EE Other Net Deductions`,
					ROUND(`ER Pension`,0) AS `ER Pension`,
					ROUND(`ER Other Additions (non-taxable)`,0) AS `ER Other Additions (non-taxable)`,
					ROUND(`Basic Salary`,0) AS `Basic Salary`,
					ROUND(`Bonus`,0) AS `Bonus`,
					ROUND(`EE Other Gross Deductions`,0) AS `EE Other Gross Deductions`,
					ROUND(`EE Social Security/ Insurances`,0) AS `EE Social Security/ Insurances`,
					ROUND(`EE Other Net Additions`,0) AS `EE Other Net Additions`,
					ROUND(`ER Other Additions (taxable)`,0) AS `ER Other Additions (taxable)`,
					ROUND(`ER Other Deductions (non-taxable)`,0) AS `ER Other Deductions (non-taxable)`,
					ROUND(`Total Taxable Salary`,0) AS `Total Taxable Salary`,					
					ROUND(`Net Salary`,0) AS `Net Salary`,
					ROUND(`Total Gross Salary`,0) AS `Total Gross Salary`,					
					ROUND(`Total ER Cost`,0) AS `Total ER Cost`,
					`comp_id`
					FROM gross_to_net
					WHERE `".$column."` = '".$col_data."' AND Period = '".$period."' GROUP BY `Period`, `Employee Name`";
		$result = mysql_query($sql);
		$showColumns = [];
		$returnedData = createJsonResponse($result, $showColumns);
		echo json_encode($returnedData);
	}

	function getParentTable(){
     $start = $_POST['start'];
     $end = $_POST['end'];
	$sql = "SELECT `Period`, 
					`Region`,
					`Group`,
				    `Country`,
					`Department`, 
					`Cost Center`,
					ROUND(SUM(`Commission`), 0) AS `Commission`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,
					ROUND(SUM(`EE Pension`),0) AS `EE Pension`,
					ROUND(SUM(`ER Social Security/ Insurances`),0) AS `ER Social Security/ Insurances`,
					ROUND(SUM(`ER Other Deductions(taxable)`),0) AS `ER Other Deductions(taxable)`,
					ROUND(SUM(`Benefits in Kind`),0) AS `Benefits in Kind`,
					ROUND(SUM(`Overtime`),0) AS `Overtime`,
					ROUND(SUM(`EE Other Gross Additions`),0) AS `EE Other Gross Additions`,
					ROUND(SUM(`EE Withholding Tax`),0) AS `EE Withholding Tax`,
					ROUND(SUM(`EE Other Net Deductions`),0) AS `EE Other Net Deductions`,
					ROUND(SUM(`ER Pension`),0) AS `ER Pension`,
					ROUND(SUM(`ER Other Additions (non-taxable)`),0) AS `ER Other Additions (non-taxable)`,
					ROUND(SUM(`Basic Salary`),0) AS `Basic Salary`,
					ROUND(SUM(`Bonus`),0) AS `Bonus`,
					ROUND(SUM(`EE Other Gross Deductions`),0) AS `EE Other Gross Deductions`,
					ROUND(SUM(`EE Social Security/ Insurances`),0) AS `EE Social Security/ Insurances`,
					ROUND(SUM(`EE Other Net Additions`),0) AS `EE Other Net Additions`,
					ROUND(SUM(`ER Other Additions (taxable)`),0) AS `ER Other Additions (taxable)`,
					ROUND(SUM(`ER Other Deductions (non-taxable)`),0) AS `ER Other Deductions (non-taxable)`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,					
					ROUND(SUM(`Net Salary`),0) AS `Net Salary`,
					ROUND(SUM(`Total Gross Salary`),0) AS `Total Gross Salary`,					
					ROUND(SUM(`Total ER Cost`),0) AS `Total ER Cost`,
					`comp_id`
					FROM gross_to_net WHERE Period BETWEEN '".$start."' AND '".$end."' GROUP by `Region`, `Period`";
	$result = mysql_query($sql);
	$showColumns = [];
	$returnedData = createJsonResponse($result, $showColumns);
	echo json_encode($returnedData);
	}
	
	function getWhereTable(){
	$region = $_POST['region'];
	$country = $_POST['country'];
	$costCenter = $_POST['costCenter'];
	$group = $_POST['group'];
    if(isset($_POST['start'])){
        $start = $_POST['start'];
        }
     else{
         $start = new DateTime();
     }

    if(isset($_POST['end'])){
        $end = $_POST['end'];
        }
    else{
       $date = new DateTime();
       $interval = new DateInterval('P1M');
        $end = $date->sub($interval);
        }
	
	$regionString = "";
	$countryString = "";
	$groupString = "";
	$costCenterString = "";


	if(isset($region)){
		$regionString .= " Region IN ( ";
		foreach($region as $r){
			$regionString .= "'".$r."' ,";		
		};
		$regionString = rtrim($regionString , ", ");
		$regionString .= " )";
	}
	
	if(isset($country)){
		$countryString .= " Country IN ( ";
		foreach($country as $c){
			$countryString .= "'".$c."' ,";		
		};
		$countryString = rtrim($countryString , ", ");
		$countryString .= " )";
	}
	
	if(isset($costCenter)){
		$costCenterString .= " `Cost Center` IN ( ";
		foreach($costCenter as $cc){
			$costCenterString .= "'".$cc."' ,";	
		}
		$costCenterString = rtrim($costCenterString , ", ");
		$costCenterString .= " )";
	}
	
	if(isset($group)){
		$groupString .= " 'Group' IN ( ";
		foreach($group as $g){
			$groupString .= "'".$g."' ,";		
		}
		$groupString = rtrim($groupString , ", ");
		$groupString .= " )";
	}
	$sql = "SELECT `Period`,
                    `Region`,
                    `Group`,
				    `Country`,
					`Department`, 
					`Cost Center`,
					ROUND(SUM(`Commission`), 0) AS `Commission`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,
					ROUND(SUM(`EE Pension`),0) AS `EE Pension`,
					ROUND(SUM(`ER Social Security/ Insurances`),0) AS `ER Social Security/ Insurances`,
					ROUND(SUM(`ER Other Deductions(taxable)`),0) AS `ER Other Deductions(taxable)`,
					ROUND(SUM(`Benefits in Kind`),0) AS `Benefits in Kind`,
					ROUND(SUM(`Overtime`),0) AS `Overtime`,
					ROUND(SUM(`EE Other Gross Additions`),0) AS `EE Other Gross Additions`,
					ROUND(SUM(`EE Withholding Tax`),0) AS `EE Withholding Tax`,
					ROUND(SUM(`EE Other Net Deductions`),0) AS `EE Other Net Deductions`,
					ROUND(SUM(`ER Pension`),0) AS `ER Pension`,
					ROUND(SUM(`ER Other Additions (non-taxable)`),0) AS `ER Other Additions (non-taxable)`,
					ROUND(SUM(`Basic Salary`),0) AS `Basic Salary`,
					ROUND(SUM(`Bonus`),0) AS `Bonus`,
					ROUND(SUM(`EE Other Gross Deductions`),0) AS `EE Other Gross Deductions`,
					ROUND(SUM(`EE Social Security/ Insurances`),0) AS `EE Social Security/ Insurances`,
					ROUND(SUM(`EE Other Net Additions`),0) AS `EE Other Net Additions`,
					ROUND(SUM(`ER Other Additions (taxable)`),0) AS `ER Other Additions (taxable)`,
					ROUND(SUM(`ER Other Deductions (non-taxable)`),0) AS `ER Other Deductions (non-taxable)`,
					ROUND(SUM(`Total Taxable Salary`),0) AS `Total Taxable Salary`,					
					ROUND(SUM(`Net Salary`),0) AS `Net Salary`,
					ROUND(SUM(`Total Gross Salary`),0) AS `Total Gross Salary`,					
					ROUND(SUM(`Total ER Cost`),0) AS `Total ER Cost`,
					`comp_id`
					FROM gross_to_net WHERE ";
					
	$showColumns = ['Period', 'Net Salary', 'Total Gross Salary', 'Total ER Cost'];
	if($regionString != ""){
		$sql = $sql .= $regionString . " AND ";
		array_push($showColumns, "Region");	
	}
	if($countryString != ""){
		$sql = $sql .= $countryString . " AND ";
		array_push($showColumns, "Country");
	}
	if($groupString != ""){
		$sql = $sql .= $groupString . " AND ";
	    array_push($showColumns , "Group");
	}
	if($costCenterString != ""){
		$sql = $sql .= $costCenterString . " AND ";
		array_push($showColumns, "Cost Center");
	}
    $sql = $sql .= " Period BETWEEN '".$start."' AND '".$end."' ";
	$sql = $sql .= " GROUP BY ";
	
	if($regionString != "") {
		$sql = $sql .= " Region, ";	
	}
	
	if($groupString != "") {
		$sql = $sql .= " 'Group', ";	
	}
	
	if($countryString != ""){
		$sql = $sql .= " Country, ";
	
	}
	if($costCenterString != ""){
		$sql = $sql .= " `Cost Center`, ";	
	}
    $sql = $sql.= " `Period`";
	$sql = rtrim($sql , ", ");
	
	$result = mysql_query($sql);
	$returnedData = createJsonResponse($result, $showColumns);
	echo json_encode($returnedData);
	}
	
	function createJsonResponse($res, $showColumns){
		$data = array();
		$returnedData = array();
	
		$row = array();
	
		while($r = mysql_fetch_array($res, MYSQL_ASSOC)){
		$columns = array();
		$keys = array_keys($r);
		$dataObject = new StdClass;
	
		foreach($keys as $val){
			$object = new StdClass;
			$object->mDataProp = $val;
			$object->sTitle = $val;
			if(count($showColumns)>0){
				if(in_array($val, $showColumns)){
					$object->show = true;
				}
				else{
					$object->show = false;
				}
			}
			array_push($columns, $object);			
			$dataObject->$val = $r[$val];
			
		}
		array_push($data, $dataObject);
	}
		$returnedData['aaData'] = $data;
		$returnedData['aoColumns'] = $columns;
		return $returnedData;
	}

?>