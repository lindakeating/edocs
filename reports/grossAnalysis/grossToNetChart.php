<?php
/**
 * Created by PhpStorm.
 * User: Linda
 * Date: 20/06/14
 * Time: 09:28
 */

$host = 'localhost';
$username = 'tmf';
$password = 'tmf';
$database = 'tmf';

$con = mysql_connect($host, $username, $password);
$db_selected = mysql_select_db($database, $con);

$sql = "SELECT Region AS Category , ";
$groupByString = " GROUP By Region, ";

if(isset($_POST['group']) && count($_POST['group'])> 0){
    $sql = "SELECT `Group` AS Category , ";
    $groupByString = $groupByString.="`Group`, ";
}

if(isset($_POST['country']) && count($_POST['country'])> 0){
    $sql = "SELECT Country AS Category , ";
    $groupByString = $groupByString.=" Country,";
}

if(isset($_POST['costCenter']) && count($_POST['costCenter'])> 0){
    $sql = "SELECT `Cost Center` AS Category , ";
    $groupByString = $groupByString.=" `Cost Center`, ";
}

$sql =  $sql .=" SUM(`Net Salary`) as 'Total Net Salary',
            SUM(`Total Gross Salary`) as 'Total Gross Salary',
            SUM(`Total ER Cost`)as 'Total ER Cost'
            FROM gross_to_net ";
$sql = rtrim($sql.=$groupByString,", ");


$result = mysql_query($sql);
$obj = new stdClass();
$categories = [];
$series = [];


while($r = mysql_fetch_assoc($result)){
    array_push($categories, $r['Category']);
    $seriesObj = new stdClass();
    $seriesObj->type = 'column';
    $seriesObj->name= $r['Category'];
    $seriesObj->data = [intval($r['Total Net Salary']), intval($r['Total Gross Salary']), intval($r['Total ER Cost'])];
    array_push($series, $seriesObj);

}
// Query for spline

$sql = "SELECT
            AVG (TotalNetSalary) as `Avg Net Salary`,
		    AVG (Totalgrosssalary) as `Avg Gross Salary`,
		    AVG (totalercost) as `Avg Gross Cost`
          FROM
          (SELECT `Region`,
                SUM(`Net Salary`) as 'TotalNetSalary',
				SUM(`Total Gross Salary`) as 'totalgrosssalary',
				SUM(`Total ER Cost`) as 'totalercost'
                FROM gross_to_net gtn";

$groupByString = " Group BY Region) inner_query";

if(isset($_POST['group']) && count($_POST['group'])> 0){
    $groupByString = " Group BY `Group`) inner_query ";
}

if(isset($_POST['country']) && count($_POST['country'])> 0){

    $groupByString = " Group BY `Country`) inner_query ";
}

if(isset($_POST['costCenter']) && count($_POST['costCenter'])> 0){
    $groupByString = " Group BY `Cost Center`) inner_query ";
}

$sql = $sql.= $groupByString;


$result = mysql_query($sql);

$avg = new stdClass();
$avgArray = [];
$avg->type = 'spline';
$avg->name = 'average';

while($r = mysql_fetch_assoc($result)){
    array_push($avgArray, intval($r['Avg Net Salary']));
    array_push($avgArray, intval($r['Avg Gross Salary']));
    array_push($avgArray, intval($r['Avg Gross Cost']));
}

$avg->data = $avgArray;

$markerObj = new stdClass();
$markerObj->lineWidth = 2;
$markerObj->lineColor = 'red';
$markerObj->fillColor = 'white';

$avg->marker = $markerObj;

array_push($series, $avg);

// Query for the pie Chart

$sql = "SELECT  Region , ROUND(SUM(`Total ER Cost`),0) as 'Total Er Cost' FROM gross_to_net GROUP BY Region";
$result = mysql_query($sql);

$pieObject = new stdClass();

$colors = ['Teal', 'Red','LightSteelBlue', 'Orange' ];
$i = 0;
$pieData = [];

while($r = mysql_fetch_assoc($result)){
    $pieDataObj = new stdClass();
    $pieDataObj->name = $r['Region'];
    $pieDataObj->y = intval($r['Total Er Cost']);
    $pieDataObj->color = $colors[$i];
    array_push($pieData, $pieDataObj);
    $i++;
}
    $pieObject->type = "pie";
    $pieObject->name= "Total ER Costs";
    $pieObject->data = $pieData;
    $pieObject->size = 80;
    $pieObject->center = [80,50];
    $pieObject->showInLegend = false;
    $dataLabelsObj = new stdClass();
    $dataLabelsObj->enabled = false;
    $pieObject->dataLabels = $dataLabelsObj;

    array_push($series, $pieObject);
    $obj->categories = $categories;
    $obj->series = $series;

    echo json_encode($obj);
