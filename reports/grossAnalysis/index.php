<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$dataTables = true;
$datePicker = true;
$grossAnalysis = true;
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php");
 ?>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Gross Analysis
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="timeline-body">
			<h2 class="timelineHeading"></h2>
			<i class="fa fa-chevron-up time-line-control"></i>
			<div class="timeline-content col-md-12">
				<div id="grossToNetChart" class=" col-md-12"></div>
			</div>
		</div>
		
		<div class="table-responsive">
			<div id="drilldown"></div>
			<table id="grossToNet" class="table display table-condensed table-striped table-bordered table-responsive">
				 <tfoot>
					<tr>
					</tr>
				</tfoot>
			</table>
			<table id="childTable" class="table display table-condensed table-striped table-bordered table-responsive">
				<tfoot>
					<tr></tr>
				</tfoot>
			</table>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>