<?php
/**
 * Created by PhpStorm.
 * User: Linda
 * Date: 21/06/14
 * Time: 13:35
 */

$host = 'localhost';
$username = 'tmf';
$password = 'tmf';
$database = 'tmf';

$con = mysql_connect($host, $username, $password);
$db_selected = mysql_select_db($database, $con);

$sql = "SELECT
        `Period`,
		Region,
		Country,
	   `Company Name`,
		Department,
		`Cost Center`,
		STR_TO_DATE(`Start Date`,'%m/%d/%Y') as `Start Date`,
		STR_TO_DATE(`Leave Date`, '%m/%d/%Y') as `Leave Date`,
		ROUND(SUM(FTE),0)/COUNT(DISTINCT(`Period`)) as `Total FTE`,
		COUNT(NULLIF(`Start Date`, '')) as `Joiners`,
		COUNT(NULLIF(`Leave Date`, '')) as `Leavers`
        FROM h_union_entities";

if(isset($_POST['action']) && !empty($_POST['action'])){
    $action = $_POST['action'];
    $selected = [];
    if(isset($_POST['parameters'])){
        $selected = $_POST['parameters'];
    }
    if(isset($_POST['start'])){
        $start = $_POST['start'];
    }
    if(isset($_POST['end'])){
        $end = $_POST['end'];
    }
    switch($action){
        case 'refineSearch': refineSearch($selected, $sql, $start, $end); break;
        case 'initTable' :initTable($sql); break;
        case 'drawCountry' : drawCountry(); break;
        case 'initStats' : initHeadCountStats(); break;
    }
}

function initHeadCountStats(){
    if(isset($_POST['region'])){
        $region = $_POST['region'];
    }
    else{
        $region = "BENELUX";
    }
    $sql = "SELECT Period,
                  Region,
                  Country,
                  `Company Name`,
                  Position ,
                  COUNT(Position) as p FROM tmf.h_union_entities
                  WHERE Region = '".$region."'
                  GROUP BY Position";
    $result = mysql_query($sql);
    $returnedData = createJsonResponse($result);
    echo json_encode($returnedData);
}

function drawCountry(){
    $sql = "SELECT DISTINCT COUNT(`EE ID`) ,`Company Name` ,`Country`  FROM h_union_entities WHERE `Leave Date` = '' GROUP BY  `Company Name`  ";
    $result = mysql_query($sql);

    $coords = [];
    while($r = mysql_fetch_array($result)){
       $countryCoords = getCountryCoords($r['Country']);
    }
}

function initTable($sql){
    $sql = $sql.= " GROUP BY `Region`";
    $result = mysql_query($sql);
    $returnedData = createJsonResponse($result);
    echo json_encode($returnedData);
}


function createJsonResponse($res){
    $showColumns = [];
    $data = array();
    $returnedData = array();

    $row = array();

    while($r = mysql_fetch_array($res, MYSQL_ASSOC)){
        $columns = array();
        $keys = array_keys($r);
        $dataObject = new StdClass;

        foreach($keys as $val){
            $object = new StdClass;
            $object->mDataProp = $val;
            $object->sTitle = $val;
            if(count($showColumns)>0){
                if(!in_array($val, $showColumns)){
                    $object->show = true;
                }
                else{
                    $object->show = false;
                }
            }
            array_push($columns, $object);
            $dataObject->$val = $r[$val];
        }
        array_push($data, $dataObject);
    }
    $returnedData['aaData'] = $data;
    $returnedData['aoColumns'] = $columns;
    return $returnedData;
}

function refineSearch($selected, $sql, $start, $end){
    $groupBy = " GROUP BY ";
    $where = " WHERE DATE(STR_TO_DATE(`Start Date`, '%m/%d/%Y')) >= '".$start."' AND DATE(STR_TO_DATE(`Leave Date`, '%m/%d/%Y')) <=  '".$end."'";
   foreach($selected as $val){
       $groupBy = $groupBy.= "`".$val."`, ";
   }
    $groupBy = rtrim($groupBy, ", ");
    $sql = $sql.=$where.$groupBy;
    $result = mysql_query($sql);
    $returnedData = createJsonResponse($result);
    echo json_encode($returnedData);

}