<?php
$host = 'localhost';
$username = 'tmf';
$password = 'tmf';
$database = 'tmf';

$con = mysql_connect($host, $username, $password);
$db_selected = mysql_select_db($database, $con);

	$column = $_POST['column'];
	$selected = $_POST['selected'];
	
	$country = [];
	$region = [];
	$costCenter = [];
	$group = [];
	
	$response = new stdClass();
	
	if($column != "Country"){
		$sql = "SELECT DISTINCT Country from gross_to_net WHERE ";
		$whereStatement = "";
		foreach($selected as $s){
			$whereStatement .= " `".$column."` = '".$s[0]."' ||";
		}
		$w = substr($whereStatement , 0, -2);
		$result = mysql_query($sql.$w);
		$data = [];

		while($r = mysql_fetch_array($result, MYSQL_BOTH)){
			$obj = new stdClass();
			$obj->label = $r[0];
			$obj->value = $r[0];
			array_push($data, $obj);
		}
		$response->country = $data;
	}
	
	if($column != "Region"){
		$sql = "SELECT DISTINCT Region from gross_to_net WHERE ";
		$whereStatement = "";
		foreach($selected as $s){
			$whereStatement .= " `".$column."` = '".$s[0]."' ||";
		}
		$w = substr($whereStatement , 0, -2);
		$result = mysql_query($sql.$w);
		$data = [];
		while($r = mysql_fetch_array($result, MYSQL_BOTH)){
			$obj = new stdClass();
			$obj->label = $r[0];
			$obj->value = $r[0];
			array_push($data, $obj);
		}
		$response->region = $data;
	}
	
	if($column != "Cost Center"){
		$sql = "SELECT DISTINCT `Cost Center` from gross_to_net WHERE ";
		$whereStatement = "";
		foreach($selected as $s){
			$whereStatement .= " `".$column."` = '".$s[0]."' ||";
		}
		$w = substr($whereStatement , 0, -2);
		$result = mysql_query($sql.$w);
		$data = [];

		while($r = mysql_fetch_array($result, MYSQL_BOTH)){
			$obj = new stdClass();
			$obj->label = $r[0];
			$obj->value = $r[0];
			array_push($data, $obj);			
		}
		$response->costCenter = $data;
	}
	
	if($column != "Group"){
		$sql = "SELECT DISTINCT `Group` from gross_to_net WHERE ";
		$whereStatement = "";
		foreach($selected as $s){
			$whereStatement .= " `".$column."` = '".$s[0]."' ||";
		}
		$w = substr($whereStatement , 0, -2);
		$result = mysql_query($sql.$w);
		$data = [];

		while($r = mysql_fetch_array($result, MYSQL_BOTH)){
			$obj = new stdClass();
			$obj->label = $r[0];
			$obj->value = $r[0];
			array_push($data, $obj);			
		}
		$response->group = $data;
	}
	
	echo json_encode($response);
	
?>