<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
$headcountStats = true;
$datePicker = true;
$dataTables = true;
?>
    <!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php");
?>
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Headcount Statistics
                    </h3>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>

            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="tabbable-custom nav-justified">
             <ul class="nav nav-tabs nav-justified">
                 <li class="active">
                     <a id="tab1" data-target="#tab_1_1_1" data-toggle="tab">
                         Gender </a>
                 </li>
                 <li>
                     <a id="tab2" data-target="#tab_1_1_2" data-toggle="tab">
                         Position </a>
                 </li>
                 <li>
                     <a id="tab3" data-target="#tab_1_1_3" data-toggle="tab">
                         Longevity</a>
                 </li>
             </ul>
             <div class="tab-content">
                 <div class="tab-pane active" id="tab_1_1_1">
                     <img src="/assets/img/headcountGender.PNG">
                 </div>
                 <div class="tab-pane" id="tab_1_1_2">
                     <div class="row col-md-12">
                         <form >
                             <div class="col-md-3 input-group pull-left">
                                 <span class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                </span>
                                <select id="selectPosition" class="form-control">
                                    <option disabled>Select Region</option>
                                    <option>EMEA</option>
                                    <option>AMERICAS</option>
                                    <option>BENELUX</option>
                                    <option>APAC</option>
                                </select>
                             </div>
                         </form>
                     </div>
                    <div id="positionChart">

                    </div>
                 </div>
                 <div class="tab-pane" id="tab_1_1_3">
                     <div class="row col-md-12">
                         <form >
                             <div class="col-md-3 input-group pull-left">
                                 <span class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                </span>
                                 <select id="selectLongevity" class="form-control">
                                     <option disabled selected>Select</option>
                                     <option value="countryLongevity">Longevity By Country</option>
                                     <option value="positionLongevity">Longevity By Position</option>
                                     <option value="regionLongevity">Longevity By Region</option>
                                 </select>
                             </div>
                         </form>
                     </div>
                     <div id="longevityChart">

                     </div>
                </div>
             </div>
        </div>
        </div>


    <!-- END PAGE CONTENT-->
        </div>
        <!-- BEGIN CONTENT -->
    </div>
    <!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>