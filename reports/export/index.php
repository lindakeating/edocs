<?php
$PageTitle = "Local Office Management";

$datePicker = true;
$export = true;

include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_head.php");
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_header.php"); ?>
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_sidebar.php"); ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				TMF Offices
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="form-group">
                                <label>Select Payroll Entity</label>
                                <div class="input-group input-lg margin-top-10">
                                    <span class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </span>
                                    <select class="form-control">
                                        <option disabled selected>Select Entity</option>
                                        <option>ie_ice_ltd</option>
                                        <option>cn_warm_ltd</option>
                                        <option>dk_ice-ltd</option>
                                        <option>gb_sunny_ltd</option>
                                        <option>nl_gold_bv</option>
                                        <option>us_frost_ltd</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Select Date Range</label>
                                <div class="input-group input-lg margin-top-10">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" name="exportRange" id="exportRange" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Select File Format</label>
                                <div class="input-group input-lg margin-top-10">
                                    <span class="input-group-addon">
                                        <i class="fa fa-download"></i>
                                    </span>
                                    <select id="fileFormat" class="form-control">
                                        <option disabled selected>Select File Format</option>
                                        <option>XLS</option>
                                        <option>Text</option>
                                        <option>XML</option>
                                        <option>All Formats</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions right">
                            <button  type="button" class="btn green">
                                <a href="dataout/ie_ice_ltd.xlsx" download="ie_ice_ltd_xls" target="_blank">Download</a></button>
                        </div>
                    </form>
                </div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- BEGIN CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include($_SERVER['DOCUMENT_ROOT'] . "/includes/page_footer.php"); ?>